
(************************************************************************)
(* Copyright (c) 2021, Gergei Bana                                      *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(* Special thanks to Qianli and Ajay K. Eeralla                         *)
(************************************************************************)

Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Setoid.
Import ListNotations.
Require Export FunctionalExtensionality.
Require Import Coq.Bool.Bool.
Require Export Background.




(* For now we set up our logic for shallow embedding *)


(**************************************************************************************)
(**************************************************************************************)
(********************* CCSA SYNTAX WITH SHALLOW EMBEDDING ****************************)
(**************************************************************************************)
(**************************************************************************************)





(**************************************************************************************)
(*************************** FUNCTION SYMBOLS IN GENERAL **************************)
(**************************************************************************************)





(* Our set of function symbols is "Symbols" this set is divided into 4 disjoint groups,
"Deterministic", "Honest", "Adversarial" and "General" symbols, which are further divied into
infinite number of subsets by "Arity". For each "n" natural number we have an arity "narg n"
where "narg" stands for number of arguments. Furthermore, we have a division "arbarg", which stands
for arbitrary arguments, meaning that we do no pre-specify the arity. *)


(* Each time a user wants to add a new function symbol, that should be done here specifying which
type it has. See below the additions of "defaults", "EQs" etc.  *)


Inductive ComputationType (* set *) : Set :=
      Honest | Deterministic | Adversarial | Mixed | General.
(* Maybe replace General with Mixed *)


Inductive arity : Set  :=
|narg : nat -> arity
|arbarg.


Inductive Symbols (hag : ComputationType) (arg : arity) :  Set :=
| symbols  : list  nat -> Symbols hag arg.   



(**************************************************************************************)
(*************************** PREDICATE SYMBOLS IN GENERAL **************************)
(**************************************************************************************)

(* Since we only have the indistinguishability predicate and we do not allow at this point
to add further predicate symbols, we do not need types for predicates similar to what we did for
function symbols. If that were to change, it should be done here. *)



(**************************************************************************************)
(*************************** SPECIFIC FUNCTION SYMBOLS **************************)
(**************************************************************************************)


(* Some specific function symbols *)

(* They could also be just parameters, at least so far we have not used that they are fixed  *)



(* Here the "s" at the end stands for symbol *)       

(* constants *)


Definition defaults : Symbols Deterministic (narg 0) :=
  symbols Deterministic (narg 0) [0].

Definition TRues : Symbols Deterministic (narg 0) :=
  symbols Deterministic (narg 0) [0 ; 1].   

Definition FAlses : Symbols Deterministic (narg 0) :=
  symbols Deterministic (narg 0) [0 ; 0].   


  (* Non-zero arity function symbols for equality and tests*)

  

Definition EQs : Symbols Deterministic (narg 2) :=
  symbols Deterministic (narg 2) [0].

Definition ITEs : Symbols Deterministic (narg 3) :=
  symbols Deterministic (narg 3) [0].





(*Functions symbols for the adversary. These symbols have no pre-specified number of
arguments*)


Definition  advs (n: nat) : Symbols Adversarial arbarg :=
  symbols Adversarial arbarg [n].




(*************************************** RANDOM BLOCKS ***************************************)


(* nonces stand for disjoint random blocks of the honest tape, each with equal length *)

Definition nonces (n: nat) : Symbols Honest (narg 0) :=
  symbols Honest (narg 0) [n].



