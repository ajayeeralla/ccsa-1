(************************************************************************)
(* Copyright (c) 2021, Gergei Bana, Qianli Zhang                        *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

(*   How to type unicode?
 *   1. in osx, choose Unicode hex input method, then hold option, and type the four digits code.
 *   2. in emacs, type C-x 8 ret, then type the four digits unicode and ret.
 *
 *   Unicode used in this file:
 *   1.'❴' :      U+2774
 *   2.'❵' :      U+2775
 *   3.'＾':      U+FF3E
 *   4.'π' :      U+03C0
 *   5.'＜' :     U+FF1C
 *   6.'＞' :     U+FF1E
 *   7.'≟' :      U+225F*)

Require Import Coq.Lists.List.
Import ListNotations.
Require Import AuxiliaryTheorem.

(********************************************
 *******        Example 12.2          *******
 ********************************************)

Module Example122.

  Notation "'cca_2'" := (CCA2 Len Enc Dec Pkey Skey Rand Error 1 1).
  Notation "'adv1' c1" := (adv (1) [c1]) (at level 101, left associativity).
  Notation n   := (nonce 0).        (* n  *)
  Notation n'  := (nonce 1).        (* n' *)
  Notation pk1 := (Pkey  [nonce 2]). (* pk1, sk1 *)
  Notation sk1 := (Skey  [nonce 2]).
  Notation Dec_sk1 c := (Dec [c; Skey [nonce 2]]).
  Notation pk2 := (Pkey  [nonce 3]). (* pk2, sk2 *)
  Notation sk2 := (Skey [nonce 3]).
  Notation Dec_sk2 c := (Dec [c; Skey [nonce 3]]).
  Notation r1  := (Rand [nonce 4]). (* r1 *)
  Notation r2  := (Rand [nonce 5]). (* r2 *)
  Notation n5  := (nonce 6).        (* n5 *)
  Notation n6  := (nonce 7).        (* n6 *)
  Notation n7  := (nonce 8).        (* n7 *)




(***************************************************************
 * (2): t[..sk1..] ~ t[..n6..] and (3): u[..sk1..] ~ u[..n6..] *
 ***************************************************************)

(* Gergei simply copied the formulas from the proof of ex12_3: *)
Proposition ex12_1_3: cca_2 -> forall n'', n'' <> 3 -> n'' <> 4 ->
  [❴＜sk1     , n5＞❵_pk2 ＾ r1;   ❴＜π2 Dec_sk2 (adv1(❴＜sk1 ,     n5＞❵_pk2 ＾ r1)), n＞❵_pk1 ＾ r2;   (nonce n'')]
 ~
  [❴＜Skey[n6], n5＞❵_pk2 ＾ r1;   ❴＜π2 Dec_sk2 (adv1(❴＜Skey[n6], n5＞❵_pk2 ＾ r1)), n＞❵_pk1 ＾ r2;   (nonce n'')].
Proof.
  intros cca2 n'' frpk frr.
  assert (forall n8 , [❴＜Skey [n8], n5＞❵_pk2 ＾ r1;   ❴＜π2 Dec_sk2 (adv1 (❴＜Skey [n8], n5＞❵_pk2 ＾ r1)), n＞❵_pk1 ＾ r2;   (nonce n'')]
                ##
         ((fun x => [x; ❴＜π2 (If adv1 x ≟ x Then ＜sk1, n5＞ Else (If adv1 x ≟ x Then Error Else Dec_sk2 (adv1 x))), n＞❵_pk1 ＾ r2; (nonce n'')])
                      (If |＜sk1, n5＞| ≟ |＜Skey [n6], n5＞| Then ❴＜Skey [n8], n5＞❵_pk2 ＾ r1 Else Error))).
  intros n8.
  repeat rewrite EqLen in *; repeat rewrite If_true in *; rewrite If_else. (*We can create a normalization tactic here*)
  rewrite <- (@If_same ((adv1 ❴＜Skey [n8], n5＞❵_pk2 ＾ r1) ≟ (❴＜ Skey [n8], n5＞❵_pk2 ＾r1)) (adv1 ❴＜Skey [n8], n5＞❵_pk2 ＾r1)) at 1.
  rewrite Example7_3.
  rewrite (@If_morph_list (fun x => Dec x)).
  rewrite decenc.
  repeat rewrite (@If_morph_list (fun x => Proj2 x)); repeat rewrite proj2pair in *. (*Here we have to use proj2 not π2, but how to fix it?*)
  reflexivity.
  all : ProveContext.
  apply intID. rewrite (H (nonce 2)). rewrite H. apply rewID.
  apply (cca2 [nonce 3] [nonce 4]  (fun x => [x; ❴＜π2 (If adv1 x ≟ x Then ＜sk1, n5＞ Else (If adv1 x ≟ x Then Error Else Dec_sk2 (adv1 x))), n＞❵_pk1 ＾r2; (nonce n'')]) (＜sk1, n5＞) (＜Skey [n6], n5＞)).
  clear cca2.
  all: ProveCCA2. Qed.

(***********************************************************
 * (2): t[..n..] ~ t[..n7..] and (3): u[..n..] ~ u[..n7..] *
 ***********************************************************)

Proposition ex12_many: cca_2 -> forall n'' ,  n'' <> 2 -> n'' <> 5 ->
  [❴＜Skey [n6], n5＞❵_pk2 ＾ r1;   ❴＜π2 Dec_sk2 (adv1 ❴＜Skey [n6], n5＞❵_pk2 ＾ r1), n＞ ❵_pk1 ＾ r2;   (nonce n'')]
  ~
  [❴＜Skey [n6], n5＞❵_pk2 ＾ r1;   ❴＜π2 Dec_sk2 (adv1 ❴＜Skey [n6], n5＞❵_pk2 ＾ r1), n7＞❵_pk1 ＾ r2;   (nonce n'')].
Proof.
  intros cca2 n'' frpk frr.
  assert (forall n8 , [❴＜Skey [n6], n5＞❵_pk2 ＾r1;   ❴＜π2 Dec_sk2 (adv1 ❴＜Skey [n6], n5＞❵_pk2 ＾ r1), n8＞ ❵_pk1 ＾r2;   (nonce n'')]
                ##
         ((fun x => [❴＜Skey [n6], n5＞❵_pk2 ＾r1;   x;   (nonce n'')])
            (If |＜π2 Dec_sk2 (adv1 ❴＜Skey [n6], n5＞❵_pk2 ＾r1), n ＞| ≟ |＜π2 Dec_sk2 (adv1 ❴＜Skey [n6], n5＞❵_pk2 ＾r1), n7＞|
                Then ❴＜π2 Dec_sk2 (adv1 ❴＜Skey [n6], n5＞❵_pk2 ＾r1), n8＞ ❵_pk1 ＾r2 Else Error))).
  intros n8.
  repeat rewrite EqLen in *; repeat rewrite If_true in *; reflexivity.
  apply intID. rewrite H. rewrite H. apply rewID.
  apply (cca2 [nonce 2] [nonce 5]  (fun x => [❴＜Skey [n6], n5＞❵_pk2 ＾r1;   x;   nonce n'' ]) (＜π2 Dec_sk2 (adv1 ❴＜Skey [n6], n5＞❵_pk2 ＾r1), n ＞)  (＜π2 Dec_sk2 (adv1 ❴＜Skey [n6], n5＞❵_pk2 ＾r1), n7＞)).
  all: ProveCCA2. Qed.

(*************************
 **  Final Proposition  **
 *************************)

Proposition ex12 : cca_2 ->
  [❴＜sk1, n5＞❵_pk2 ＾r1;   ❴＜π2 Dec_sk2 (adv1 ❴＜sk1, n5＞❵_pk2 ＾r1), n＞ ❵_pk1 ＾r2;   n]
  ~
  [❴＜sk1, n5＞❵_pk2 ＾r1;   ❴＜π2 Dec_sk2 (adv1 ❴＜sk1, n5＞❵_pk2 ＾r1), n＞ ❵_pk1 ＾r2;   n'].
Proof.
  intros.
  assert(
    [❴＜Skey [n6], n5＞❵_pk2 ＾ r1;   ❴＜π2 Dec_sk2 (adv1 ❴＜Skey [n6], n5＞❵_pk2 ＾ r1), n7＞ ❵_pk1 ＾ r2]
   ~
    [❴＜Skey [n6], n5＞❵_pk2 ＾ r1;   ❴＜π2 Dec_sk2 (adv1 ❴＜Skey [n6], n5＞❵_pk2 ＾ r1), n7＞ ❵_pk1 ＾ r2]) as ex12_middle.
  apply cind_ref.
  apply (FreshInd n n') in ex12_middle.
  apply (@cind_funcapp (fun lc =>((skipn 1 lc) ++ (firstn 1 lc)))) in ex12_middle; simpl in ex12_middle.
  rewrite ex12_1_3; auto.
  rewrite ex12_many; auto.
  rewrite ex12_middle; auto.
  rewrite <- ex12_many; auto.
  rewrite <- ex12_1_3; auto.
  reflexivity.
  all : try ProveContext.
  all : try ProveFresh.
Qed.

End Example122.
