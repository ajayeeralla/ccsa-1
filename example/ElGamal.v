
(************************************************************************)
(* Copyright (c) 2020, Gergei Bana                                      *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Setoid.
Require Import Coq.micromega.Lia.
Import ListNotations.


Require Export Pairs.
(*Require Import CCSARandomizedPublicKeyEncryptions.*)
Require Export GroupExponentiation.
Require Export FreshContexts.


(* Domains should be treated properly later*)


(* The symmetric-key public-key pairs are generated from two nonces *)
(* Key generation: to [nonce n1 ; nonce n2] ++ lx' ,
EGpkey assignes the pair of ggen [n1] and ggen [n1] ^^ Ring [n2]
 *)
Definition EGpkey (lx : list ppt) := pair   [ggen [Nth 0 lx]  ; (ggen [Nth 0 lx]) ^^ (Ring [Nth 1 lx])].

(*EGskey assignes the pair of ggen [n1] and  Ring [n2] *)
Definition EGskey (lx : list ppt) := pair   [ggen [Nth 0 lx]  ;  Ring [Nth 1 lx] ].

(*r random input *)
Definition r  := Ring.


(*EGenc works for [plaintext ; pk ; random input ] ++ lx'
note that pk is a pair of the form (g , g^R) the output of the encryption is a pair also
(g^(random input) , m ** (second component of pk)^(random input))
  *)
Definition EGenc (lx : list ppt) :=  pair [ (proj1 [Nth 1 lx ]) ^^  (Nth 2 lx) ;
                                          (Nth 0 lx) **  (proj2 [(Nth 1 lx) ]) ^^ (Nth 2 lx)].


Definition EGdec (lx : list ppt) :=  proj2 [ Nth 0 lx ] **   ginv [(proj1 [Nth 0 lx]) ^^ (proj2 [Nth 1 lx])].



Lemma EGdecenc :
  forall n1 n2 n m,
EGdec [EGenc [ m ; EGpkey [n1; n2] ; r [n] ] ;  EGskey [n1; n2]  ] # m.
Proof. intros. unfold EGdec. unfold EGenc. unfold EGpkey. unfold EGskey.
unfold Nth. simpl.  repeat rewrite proj2pair.
repeat rewrite proj1pair.
unfold r. rewrite expcomm. rewrite <- gmass.
rewrite gminvr. apply gmidentr.  Qed.






Fixpoint ggm  (lx : list ppt) (g: ppt) : list ppt :=
match lx with
| [ ] => [ ]
| a :: m => (a ** g) :: (ggm m (a ** g))
end.


Notation "g '***' r" := (ggm  g  r )
                         (at level 40, left associativity).





Lemma Nth_len_le : forall x a nl, Nonce a -> Nth x nl = a -> x < length nl.
Proof.
 intros x a.
  unfold Nth.
  induction x; intros.
  - destruct nl.
    + simpl in H0. inversion H. rewrite <- H1 in H0.
      assert (nonce x <> default). apply noncesNotdefault. rewrite H0 in H2. contradiction.
    + simpl in H0. simpl. lia.
  - destruct nl.
    + simpl in H0. inversion H. rewrite <- H1 in H0.
      assert (nonce x0 <> default). apply noncesNotdefault. rewrite H0 in H2. contradiction.
    + simpl in H0. simpl. apply Lt.lt_n_S.
      apply IHx; auto.
Qed.

Proposition ListFreshIndl :
      (*"<<<"*)  forall nl lx ly,  ListFresh nl lx ->  ListFresh nl ly  -> (*">>>"*)
              lx ~ ly  -> (nl ++ lx) ~ (nl ++ ly).
Proof.
  intros nl. induction nl.
  - intros. simpl. auto.
  - intros. assert (H2 := H). apply ListFreshNonceList in H2. inversion H2.
    assert (H7:= H5) . assert (H8:= H6) .
    apply  (NonceVsNonceList' a nl H7) in H8. destruct H8.
    + rewrite <- app_comm_cons.  rewrite <- app_comm_cons. inversion H8.
Check @cind_funcapp. apply IHnl in H1.
      apply (@cind_funcapp (fun lx => (Nth x lx) :: lx) (nl ++ lx) (nl ++ ly)) in H1.
      unfold Nth in H1. rewrite app_nth1 in H1. unfold Nth in H9. rewrite H9 in H1.
      rewrite app_nth1 in H1.  rewrite H9 in H1. auto.
      apply (Nth_len_le x a nl); auto.
      apply (Nth_len_le x a nl); auto.
      ProveContext. inversion H. auto. inversion H0. auto.
    + rewrite <- app_comm_cons.  rewrite <- app_comm_cons. apply FreshInd.
      * apply frlConc. auto. inversion H. auto.
      * apply frlConc. auto. inversion H0. auto.
      * apply frlConc. auto. inversion H. auto.
      * apply frlConc. auto. inversion H0. auto.
      * apply IHnl.
        ** inversion H.  auto.
        ** inversion H0.  auto.
        ** auto.
Qed.


Proposition ListFreshIndr :
      (*"<<<"*)  forall nc lx ly,  ListFresh nc lx ->  ListFresh nc ly ->  (*">>>"*)
              lx ~ ly -> (lx ++ nc) ~ (ly ++ nc).
Proof.
  intros.
  assert ((nc ++ lx) ~ (nc ++ ly)).
  apply ListFreshIndl; auto.
  apply (@cind_funcapp (fun l: list ppt => (skipn (length nc) l) ++ (firstn (length nc) l))) in H2.
  repeat (rewrite fstn in H2). repeat (rewrite skpn in H2).
  auto. ProveContext. Qed.



(*
Proposition ListFreshIndr :
      (*"<<<"*)  forall nc1 nc2 lx ly,  ListFresh nc1 lx ->  ListFresh nc1 ly -> ListFresh nc2 lx ->  ListFresh nc2  ly -> (*">>>"*)
              lx ~ ly -> nc1 ~ nc2 -> (lx ++ nc1) ~ (ly ++ nc2).
Proof.
  intros.
  assert ((nc1 ++ lx) ~ (nc1 ++ ly)).
  apply ListFreshIndl; auto.
  (*here we need an advanced form of FreshInd*)
  pose (f := fun l: list ppt => (skipn (length nc1) l) ++ (firstn (length nc1) l)).
  assert (Context Adversarial List f); unfold f; ProveContext.
  pose (LEM (length nc1 = length nc2)).
  destruct o.
  - apply (@cind_funcapp f (nc1 ++ lx) (nc2 ++ ly) H6) in H5.
    unfold f in H5.
    rewrite fstn in H5. rewrite skpn in H5.
    rewrite H7 in H5.
    rewrite fstn in H5. rewrite skpn in H5.
    auto.
  - apply cind_len in H7.
    apply H7 in H4. contradiction.
Qed.
*)


Lemma NthOnApp : forall n tl tl' , Nth ((length tl) + n) (tl ++ tl') = Nth n tl'.
Proof. intros. induction tl. simpl. auto. simpl. unfold Nth. simpl.
unfold Nth in IHtl. rewrite IHtl. auto.
Qed.


Theorem EGCPAsmall :  (DDH ggen Ring)  ->
 forall nl (n1 n2 n   : nat)  (u u' : list ppt -> ppt), n<> n1 -> n<>n2 -> n1 <> n2 ->
ListFreshTermc List [nonce n ; nonce n1 ; nonce n2]  u  -> ListFreshTermc List [nonce n ; nonce n1 ; nonce n2]  u' ->
ListFresh nl [nonce n ; nonce n1 ; nonce n2] ->
     nl ++ [EGpkey [nonce n1; nonce n2 ]; EGenc [u [EGpkey [nonce n1; nonce n2]]  ; EGpkey [nonce n1; nonce n2]; r [nonce n]]]
   ~
      nl ++ [EGpkey [nonce n1; nonce n2] ;  EGenc [u'[EGpkey [nonce n1; nonce n2]]  ; EGpkey [nonce n1; nonce n2]; r [nonce n]]].
Proof. intros DDH. intros. unfold EGenc. unfold EGpkey. unfold r. unfold Nth. simpl. apply intID.   apply ListFreshNonceList in H4 as NL.
repeat (rewrite proj2pair; rewrite proj1pair). apply rewID.
      assert (exists f nlu, ContextTerm Adversarial List f /\
      NonceList nlu /\ ListFresh [nonce n ; nonce n1 ; nonce n2 ] nlu /\ u = (fun x => f (nlu ++  x))) as NLU.
      { apply (ListFreshTermcfromNonceList List [nonce n ; nonce n1 ; nonce n2] u) in H2.
        inversion H2.
        - inversion H5. exists x. exists x0. destruct H6. destruct H7.  destruct H8.  repeat split.
          + ProveContext.
          + apply H7.
          + auto.
          + auto.
        - unfold not. intros. inversion H5. }
      inversion NLU as [f [nlu [NLU1 [NLU2 [NLU3 NLU4]]]]].
      assert (exists f' nlu', ContextTerm Adversarial List f' /\
      NonceList nlu' /\ ListFresh [nonce n ; nonce n1 ; nonce n2 ] nlu' /\ u' = (fun x => f' (nlu' ++  x))) as NLU'.
      { apply (ListFreshTermcfromNonceList List [nonce n ; nonce n1 ; nonce n2] u') in H3.
        inversion H3.
        - inversion H5. exists x. exists x0. destruct H6. destruct H7.  destruct H8.  repeat split.
          + ProveContext.
         + apply H7.
          + auto.
          + auto.
        - unfold not. intros. inversion H5. }
      inversion NLU' as [f' [nlu' [NLU'1 [NLU'2 [NLU'3 NLU'4]]]]].
      assert (exists n3 , Fresh (nonce n3) ([nonce n; nonce n1 ; nonce n2] ++ nl ++ nlu ++ nlu')).
      { apply (FreshExists ([nonce n; nonce n1 ; nonce n2] ++ nl ++ nlu ++ nlu')). ProveNonceList. }
      inversion H5 as [n3 N3].
      assert (n3 <> n).  inversion N3. apply invfrtNN in H9. auto.
      assert (n3 <> n1). inversion N3. inversion H11. apply invfrtNN in H15. auto.
      assert (n3 <> n2). inversion N3. inversion H12. inversion H17. apply invfrtNN in H21. auto.
      assert  (
              [ggen [nonce n1] ; ggen [nonce n1] ^^ Ring [nonce n2] ; ggen [nonce n1] ^^ Ring [nonce n] ;
                    ggen [nonce n1] ^^ Ring [nonce n2] ^^ Ring [nonce n]]
            ~ [ggen [nonce n1] ; ggen [nonce n1] ^^ Ring [nonce n2] ; ggen [nonce n1] ^^ Ring [nonce n] ;
                    ggen [nonce n1] ^^ Ring [nonce n3] ] ) .
      { intros. apply DDH; auto. }
      assert ((nl ++ nlu ++ [ggen [nonce n1] ; ggen [nonce n1] ^^ Ring [nonce n2] ; ggen [nonce n1] ^^ Ring [nonce n] ;
                   ggen [nonce n1] ^^ Ring [nonce n2] ^^ Ring [nonce n]] )
            ~ (nl ++ nlu ++ [ggen [nonce n1] ; ggen [nonce n1] ^^ Ring [nonce n2] ; ggen [nonce n1] ^^ Ring [nonce n] ;
                    ggen [nonce n1] ^^ Ring [nonce n3] ] )) .
      { repeat rewrite <- app_ass. apply ListFreshIndl.
        - apply invlfrlConc in H4. destruct H4.  apply invlfrlConc in H10. destruct H10.
          apply invlfrlConc in H11; destruct H11.
          inversion NLU3. inversion H17. inversion H22. ProveListFresh.
        - apply invlfrlConc in H4. destruct H4.  apply invlfrlConc in H10. destruct H10.
          apply invlfrlConc in H11; destruct H11.
          inversion NLU3. inversion H17. inversion H22. ProveListFresh.
          + apply invfrlConc in N3. destruct N3.
            apply invfrlConc in H29. destruct H29. apply invfrlConc in H30. destruct H30. auto.
          + apply invfrlConc in N3. destruct N3.
            apply invfrlConc in H29. destruct H29. apply invfrlConc in H30. destruct H30. auto.
        - auto. }
       assert  (
              (nl ++ [ggen [nonce n1] ; ggen [nonce n1] ^^ Ring [nonce n2] ; ggen [nonce n1] ^^ Ring [nonce n] ;
                   u [pair [ggen [nonce n1]; ggen [nonce n1] ^^ Ring [nonce n2]]]  ** ggen [nonce n1] ^^ Ring [nonce n2] ^^ Ring [nonce n]] )
            ~ (nl ++ [ggen [nonce n1] ; ggen [nonce n1] ^^ Ring [nonce n2] ; ggen [nonce n1] ^^ Ring [nonce n] ;
                   u [pair [ggen [nonce n1]; ggen [nonce n1] ^^ Ring [nonce n2]]]  ** ggen [nonce n1] ^^ Ring [nonce n3] ] )) as TR1.
      { apply (@cind_funcapp (fun x => ((firstn ((length nl))  x) ++ ( firstn 3 (skipn (length nlu)  (skipn (length nl) x) ) )
        ++ [ f  ((firstn (length nlu) (skipn (length nl) x) )++  [pair  (firstn 2 (skipn (length nlu)  (skipn (length nl) x) )) ] )
        **  (Nth 3 (skipn (length nlu)  (skipn (length nl) x) )) ]))) in H10.
        - repeat rewrite skpn in H10. repeat rewrite fstn in H10. simpl in H10. unfold Nth in H10. simpl in H10.
          assert (forall x , u x = f (nlu ++x)).
          { intros. rewrite NLU4. auto. }
          rewrite <- H11 in H10. auto.
        - ProveContext. }
       assert ((nl ++ nlu' ++ [ggen [nonce n1] ; ggen [nonce n1] ^^ Ring [nonce n2] ; ggen [nonce n1] ^^ Ring [nonce n] ;
                   ggen [nonce n1] ^^ Ring [nonce n2] ^^ Ring [nonce n]] )
            ~ (nl ++ nlu' ++ [ggen [nonce n1] ; ggen [nonce n1] ^^ Ring [nonce n2] ; ggen [nonce n1] ^^ Ring [nonce n] ;
                    ggen [nonce n1] ^^ Ring [nonce n3] ] )) .
      { repeat rewrite <- app_ass. apply ListFreshIndl.
        - apply invlfrlConc in H4. destruct H4.   apply invlfrlConc in H11. destruct H11. apply invlfrlConc in H12; destruct H12.
          inversion NLU'3. inversion H18. inversion H23. ProveListFresh.
        - apply invlfrlConc in H4. destruct H4.  apply invlfrlConc in H11. destruct H11. apply invlfrlConc in H12; destruct H12.
          inversion NLU'3. inversion H18. inversion H23. ProveListFresh.
          + apply invfrlConc in N3. destruct N3.
            apply invfrlConc in H30. destruct H30. apply invfrlConc in H31. destruct H31. auto.
          + apply invfrlConc in N3. destruct N3.
            apply invfrlConc in H30. destruct H30. apply invfrlConc in H31. destruct H31. auto.
         - auto. }
      assert  (
              (nl ++ [ggen [nonce n1] ; ggen [nonce n1] ^^ Ring [nonce n2] ; ggen [nonce n1] ^^ Ring [nonce n] ;
                   u' [pair [ggen [nonce n1]; ggen [nonce n1] ^^ Ring [nonce n2]]] ** ggen [nonce n1] ^^ Ring [nonce n2] ^^ Ring [nonce n]] )
            ~ (nl ++ [ggen [nonce n1] ; ggen [nonce n1] ^^ Ring [nonce n2] ; ggen [nonce n1] ^^ Ring [nonce n] ;
                   u' [pair [ggen [nonce n1]; ggen [nonce n1] ^^ Ring [nonce n2]]]   ** ggen [nonce n1] ^^ Ring [nonce n3] ] )) as TR2.
apply (@cind_funcapp (fun x => ((firstn ((length nl))  x) ++ ( firstn 3 (skipn (length nlu')  (skipn (length nl) x) ) )
++ [ f'  ((firstn (length nlu') (skipn (length nl) x) )++  [pair  (firstn 2 (skipn (length nlu')  (skipn (length nl) x) )) ] )   **  (Nth 3 (skipn (length nlu')  (skipn (length nl) x) )) ]))) in H11.
ProveContext.  repeat rewrite skpn in H11. repeat rewrite fstn in H11. simpl in H11. unfold Nth in H11. simpl in H11.
assert (forall x , u' x = f' (nlu' ++x)). intros. rewrite NLU'4. auto. rewrite <- H12 in H11. auto. ProveContext.
   assert  (
              (nl ++ [ggen [nonce n1] ; ggen [nonce n1] ^^ Ring [nonce n2] ; ggen [nonce n1] ^^ Ring [nonce n] ;
                     ggen [nonce n1] ^^ Ring [nonce n3] ] )
            ~ (nl ++ [ggen [nonce n1] ; ggen [nonce n1] ^^ Ring [nonce n2] ; ggen [nonce n1] ^^ Ring [nonce n] ;
                   u [pair [ggen [nonce n1]; ggen [nonce n1] ^^ Ring [nonce n2]]]   ** ggen [nonce n1] ^^ Ring [nonce n3] ] )) as TR4.
Check ggenRingUniform.
assert (nonce n1 :: ggen [nonce n1]^^ Ring [nonce n3]:: nonce n2 :: nonce n :: nl ~
       nonce n1 :: u [pair [ggen [nonce n1]; ggen [nonce n1] ^^ Ring [nonce n2]]] ** ggen [nonce n1] ^^ Ring [nonce n3] :: nonce n2 :: nonce n :: nl).
apply ggenRingUniform. ProveNonceList. rewrite NLU4. ProveFresh.
Check frlConc. apply invfrlConc in N3 ; inversion_clear N3 ; apply invfrlConc in H13 ; inversion_clear H13 ; apply invfrlConc in H15 ; inversion_clear H15. auto.
 apply invfrlConc in N3 ; inversion_clear N3 ; apply invfrlConc in H13 ; inversion_clear H13 ; apply invfrlConc in H15 ; inversion_clear H15.
ProveFresh.
apply (@cind_funcapp (fun x => (TL (TL ( TL ( TL x))) ++ [ ggen [HD x] ; ggen [HD x]^^(Ring [Nth 2 x]) ; ggen [HD x]^^(Ring [Nth 3 x]) ; Nth 1 x]))) in H12.
repeat unfold TL in H12. repeat unfold HD in H12. repeat unfold Nth in H12.  simpl in H12. auto. ProveContext.


  assert  (
              (nl ++ [ggen [nonce n1] ; ggen [nonce n1] ^^ Ring [nonce n2] ; ggen [nonce n1] ^^ Ring [nonce n] ;
                     ggen [nonce n1] ^^ Ring [nonce n3] ] )
            ~ (nl ++ [ggen [nonce n1] ; ggen [nonce n1] ^^ Ring [nonce n2] ; ggen [nonce n1] ^^ Ring [nonce n] ;
                   u' [pair [ggen [nonce n1]; ggen [nonce n1] ^^ Ring [nonce n2]]]   ** ggen [nonce n1] ^^ Ring [nonce n3] ] )) as TR'4.

assert (nonce n1 :: ggen [nonce n1]^^ Ring [nonce n3]:: nonce n2 :: nonce n :: nl ~
       nonce n1 :: u' [pair [ggen [nonce n1]; ggen [nonce n1] ^^ Ring [nonce n2]]] ** ggen [nonce n1] ^^ Ring [nonce n3] :: nonce n2 :: nonce n :: nl).
apply ggenRingUniform. ProveNonceList. rewrite NLU'4. ProveFresh.
Check frlConc. apply invfrlConc in N3 ; inversion_clear N3 ; apply invfrlConc in H13 ; inversion_clear H13 ; apply invfrlConc in H15 ; inversion_clear H15. auto.
 apply invfrlConc in N3 ; inversion_clear N3 ; apply invfrlConc in H13 ; inversion_clear H13 ; apply invfrlConc in H15 ; inversion_clear H15.
ProveFresh.
apply (@cind_funcapp (fun x => (TL (TL ( TL ( TL x))) ++ [ ggen [HD x] ; ggen [HD x]^^(Ring [Nth 2 x]) ; ggen [HD x]^^(Ring [Nth 3 x]) ; Nth 1 x]))) in H12.
repeat unfold TL in H12. repeat unfold HD in H12. repeat unfold Nth in H12.  simpl in H12. auto. ProveContext.

assert  (
              (nl ++ [ggen [nonce n1] ; ggen [nonce n1] ^^ Ring [nonce n2] ; ggen [nonce n1] ^^ Ring [nonce n] ;
                   u [pair [ggen [nonce n1]; ggen [nonce n1] ^^ Ring [nonce n2]]]  ** ggen [nonce n1] ^^ Ring [nonce n3] ] )
            ~ (nl ++ [ggen [nonce n1] ; ggen [nonce n1] ^^ Ring [nonce n2] ; ggen [nonce n1] ^^ Ring [nonce n] ;
                   u' [pair [ggen [nonce n1]; ggen [nonce n1] ^^ Ring [nonce n2]]]   ** ggen [nonce n1] ^^ Ring [nonce n3] ] )) as TR3.
rewrite <- TR4. rewrite <- TR'4. reflexivity.
rewrite TR3 in TR1. rewrite <- TR2 in TR1.
apply (@cind_funcapp (fun lx => (firstn (length nl) lx ) ++ [ pair [Nth ((length nl)+0) lx  ; Nth ((length nl)+1) lx  ] ;
 pair [Nth ((length nl)+2 ) lx  ; Nth ((length nl)+3) lx  ]     ] )) in TR1.
simpl in TR1. rewrite fstn in TR1. repeat rewrite NthOnApp in TR1. unfold Nth in TR1. simpl in TR1. rewrite fstn in TR1. simpl in TR1. auto.
ProveContext.
Qed.




Proposition EGCPA :
(DDH ggen Ring)  ->
forall  (n1 n2 n   : nat) (t: list ppt -> list ppt) (u u' : list ppt -> ppt), n<> n1 -> n<>n2 -> n1 <> n2 ->
ListFreshTermc List [nonce n ; nonce n1 ; nonce n2]  u  -> ListFreshTermc List [nonce n ; nonce n1 ; nonce n2]  u' ->
ListFreshc List [nonce n ; nonce n1 ; nonce n2] t ->
    t [ EGpkey [nonce n1; nonce n2] ; EGenc [u [EGpkey [nonce n1; nonce n2]]  ; EGpkey [nonce n1; nonce n2]; r [nonce n]]]
   ~
    t [ EGpkey [nonce n1; nonce n2] ; EGenc [u'[EGpkey [nonce n1; nonce n2]] ; EGpkey [nonce n1; nonce n2]; r [nonce n]]].
Proof. intros. apply ListFreshcfromNonceList in H5. inversion H5 as [f [nl [T1 [T2 [T3 T4]]]]]. rewrite T4. simpl.
apply cind_funcapp. auto. apply  EGCPAsmall; auto. apply frtllsymm. auto. auto. unfold not. intros. inversion H6.
Qed.
