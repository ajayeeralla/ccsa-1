(************************************************************************)
(* Copyright (c) 2021, Gergei Bana, Qianli Zhang                        *)   
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

Require Export Coq.Lists.List.
Import ListNotations.
Require Import Coq.micromega.Lia.   
Require Export prop1Equalityn.


Axiom Tau1Tri :
      forall {x1 x2 x3: ppt} ,
        (Tau1 [Triple [x1; x2; x3]]) = x1.

Axiom Tau2Tri :
      forall {x1 x2 x3: ppt} ,
        (Tau2 [Triple [x1; x2; x3]]) = x2.

Axiom Tau3Tri :
      forall {x1 x2 x3: ppt} ,
        (Tau3 [Triple [x1; x2; x3]]) = x3.





(*how the RA connects the "B1_0 x1" with pseudonym?*)
Parameter Pseudonyms : Symbols Deterministic (narg 3).
Notation Pseudonym := (FuncInt Deterministic (narg 3) Pseudonyms).

(**)
Notation np0  := (nonce 16).
Notation np1  := (nonce 17).
Notation np2  := (nonce 18).

Notation rRA0   := (Rand [nonce 10]).
Notation rRA1   := (Rand [nonce 19]).
Notation rRA2   := (Rand [nonce 20]).



(*the return value of adv2 should be a triple vector, then we can use τ?*)
Notation adv2 c1 c2:= (adv (2) [c1; c2]).
Notation Dec_RA c := (Dec [c; skRA]).
Notation Dec_RAif x d := (If (EQ [d ;x]) Then Error Else (Dec [d; skRA])).


(*why we have to add τ after adv2? when we use if-then-else, we need to compare "τ1 (adv2 (B1_0 x1) (B1_1 x2))" with "B1_0 x1"*)
Definition d k x1 x2 := (Dec_RA (tau k [adv2 (B1 I0 x1) (B1 I1 x2)])).


(* 
Wrong1: Definition np k x1 x2  := If ((π1 (τ1 (d k x1 x2))) ≟ pk0) Then np0 Else (If ((π1 (τ1 (d k x1 x2))) ≟ pk1) Then np1 Else np2) .
Wrong2: Definition np k x1 x2  := If (d k x1 x2 ≟ ＜ id0, hbd0, ｛ hbd0 ｝_ ssk0 ˆ rs0 ＞) Then np0
                    Else (If (d k x1 x2 ≟ ＜ id1, hbd1, ｛ hbd1 ｝_ ssk1 ˆ rs1 ＞) Then np1 Else np2). *)
Definition np k x1 x2  := If ((tau k [adv2 (B1 I0 x1) (B1 I1 x2)]) ≟ (❴＜ id0, X x1, ｛ X x1 ｝_ ssk0 ˆ rs0 ＞❵_ pkRA ＾ r0 )) Then np0
                    Else (If ((tau k [adv2 (B1 I0 x1) (B1 I1 x2)]) ≟ (❴＜ id1, X x2, ｛ X x2 ｝_ ssk1 ˆ rs1 ＞❵_ pkRA ＾ r1 )) Then np1 Else np2).



(*we use the public key, public verification key and a randomness to generate the pseudonyms*)
Definition P k x1 x2 := (Pseudonym [π1 (τ1 (d k x1 x2)); π2 (τ1 (d k x1 x2)); (np k x1 x2)]).



(*Definition rRA k x1 x2  := If ((π1 (τ1 (d k x1 x2))) ≟ pk0) Then rRA0 Else (If ((π1 (τ1 (d k x1 x2))) ≟ pk1) Then rRA1 Else rRA2) . *)
Definition rRA k x1 x2  := If ((tau k [adv2 (B1 I0 x1) (B1 I1 x2)]) ≟ (❴＜ id0, X x1, ｛ X x1 ｝_ ssk0 ˆ rs0 ＞❵_ pkRA ＾ r0 )) Then rRA0
                              Else (If ((tau k [adv2 (B1 I0 x1) (B1 I1 x2)]) ≟ (❴＜ id1, X x2, ｛ X x2 ｝_ ssk1 ˆ rs1 ＞❵_ pkRA ＾ r1 )) Then rRA1 Else rRA2).




(*Here randomness should be different, while this doesn't affect the final result*)
(*And here the randomness rRA and rsRA should be different in different encryption and signature*)
Definition R1 k x1 x2 := (If Ver [τ2 (d k x1 x2); τ3 (d k x1 x2);  π2 (τ1 (d k x1 x2))] Then ｛❴＜P k x1 x2, τ2 (d k x1 x2)＞❵_(π1 (τ1 (d k x1 x2))) ＾ (rRA k x1 x2)｝_sskRA ˆ rsRA  Else Error).


(*
Lemma prop2_lemma1:
  forall k1 k2 x1 x2,
    tau k1 [d k2 x1 x2] = If EQ [(tau k2 [adv2 (B1 I0 x1) (B1 I1 x2)]) ; (B1 I0 x1)]
                             Then (tau k1 [Dec_RA (B1 I0 x1)])
                             Else (If EQ [(tau k2 [adv2 (B1 I0 x1) (B1 I1 x2)]) ; (B1 I1 x2)]
                                      Then (tau k1 [Dec_RA (B1 I1 x2)])
                                      Else (tau k1 [d k2 x1 x2])
                                  ).
Admitted.  *)

Lemma prop2_lemma1_help: forall x u u' n n' n'',
       (Dec [x;  Skey [n]])
     = 
       (If (x ≟ ❴u ❵_(Pkey [n])＾(Rand [n' ])) Then u 
  Else (If (x ≟ ❴u'❵_(Pkey [n])＾(Rand [n''])) Then u'
  Else (Dec [x;  Skey [n]]))).
Proof.
  intros.
  rewrite <- (@If_same (x ≟ ❴u❵_(Pkey [n])＾(Rand [n'])) (Dec [x;  Skey [n]])) at 1.
  rewrite (Eq_branch _ _ (fun x => Dec [x; Skey [n]])).
  rewrite decenc.
  rewrite <- (@If_same (x ≟ ❴u'❵_(Pkey [n])＾(Rand [n''])) (Dec [x;  Skey [n]])) at 1.
  rewrite (Eq_branch _ _ (fun x => Dec [x; Skey [n]])).
  rewrite decenc.
  reflexivity. 
  all : ProveContext. Qed.


Lemma prop2_lemma1: forall x {f u u' n n' n''}, ContextTerm General Term f -> 
       (f (Dec [x;  Skey [n]]))
     = 
       (If (x ≟ ❴u ❵_(Pkey [n])＾(Rand [n' ])) Then f u 
  Else (If (x ≟ ❴u'❵_(Pkey [n])＾(Rand [n''])) Then f u'
  Else (f (Dec [x;  Skey [n]])))).
Proof.
  intros.
  rewrite (@prop2_lemma1_help x u u' n n' n'') at 1.
  repeat rewrite (@If_morph f).
  reflexivity.
  all: auto. Qed.



Lemma prop2_lemma2_help : forall k x1 x2,
    (fun y => If Ver [τ2 y; τ3 y; π2 (τ1 y)] Then ｛❴＜Pseudonym [π1 (τ1 y); π2 (τ1 y); (np k x1 x2)], τ2 y ＞❵_(π1 (τ1 y)) ＾ (rRA k x1 x2)｝_sskRA ˆ rsRA Else Error)
    (Dec [(tau k [adv2 (B1 I0 x1) (B1 I1 x2)]);  Skey [nonce 8]])
   =
         If ((tau k [adv2 (B1 I0 x1) (B1 I1 x2)]) ≟ (B1 I0 x1)) Then ｛ ❴ ＜ Pseudonym [pk0; vk0; np0], X x1 ＞ ❵_ pk0 ＾ rRA0 ｝_ sskRA ˆ rsRA
   Else (If ((tau k [adv2 (B1 I0 x1) (B1 I1 x2)]) ≟ (B1 I1 x2)) Then ｛ ❴ ＜ Pseudonym [pk1; vk1; np1], X x2 ＞ ❵_ pk1 ＾ rRA1 ｝_ sskRA ˆ rsRA
   Else (fun y => If Ver [τ2 y; τ3 y; π2 (τ1 y)] Then ｛❴＜Pseudonym [π1 (τ1 y); π2 (τ1 y); np2], τ2 y ＞❵_(π1 (τ1 y)) ＾ rRA2 ｝_sskRA ˆ rsRA Else Error)
        (d k x1 x2)).
Proof.
  intros.
  
  rewrite (@prop2_lemma1 (tau k [adv2 (B1 I0 x1) (B1 I1 x2)]) _ (＜id0, X x1,｛X x1｝_ssk0 ˆ rs0＞) (＜id1, X x2,｛X x2｝_ssk1 ˆ rs1＞) (nonce 8) (nonce 2) (nonce 6)) at 1.
  repeat (rewrite Tau1Tri; rewrite Tau2Tri; rewrite Tau3Tri; rewrite proj1pair; rewrite proj2pair; rewrite correctness; rewrite If_true). 
  unfold np, rRA in *; simpl. Admitted.
 
(*  rewrite (@If_eval (fun b1 => ｛ ❴ ＜ Pseudonym [_; _; If b1 Then np0 Else _ ], _ ＞ ❵_ pk0 ＾ (If b1 Then rRA0 Else _) ｝_ sskRA ˆ rsRA )
                    (fun b1 => If _ Then ｛ ❴ ＜ Pseudonym [_; _; If b1 Then np0 Else _ ], _ ＞ ❵_ _ ＾ If b1 Then rRA0 Else _  ｝_ sskRA ˆ rsRA
                       Else If _ Then ｛ ❴ ＜ Pseudonym [_; _; If b1 Then np0 Else _ ], _ ＞ ❵_ _ ＾ If b1 Then rRA0 Else _ ｝_ sskRA ˆ rsRA Else Error  ) 
                    ((tau k [adv2 (B1 I0 x1) (B1 I1 x2)]) ≟ ❴ ＜ id0, X x1, ｛ X x1 ｝_ ssk0 ˆ rs0 ＞ ❵_ pkRA ＾ r0)).
  repeat (rewrite If_true; rewrite If_false).
  rewrite (@If_eval (fun b2 => ｛ ❴ ＜ Pseudonym [pk1; vk1; If b2 Then np1 Else _ ], _ ＞ ❵_ pk1 ＾ If b2 Then rRA1 Else _ ｝_ sskRA ˆ rsRA )
                    (fun b2 => If Ver [_ ; _ ; _]
                            Then ｛ ❴ ＜ Pseudonym [_ ; _ ; If b2 Then np1 Else _ ], _ ＞ ❵_ _ ＾ If b2 Then rRA1 Else rRA2 ｝_ sskRA ˆ rsRA Else Error ) 
                    (tau k [adv2 (B1 I0 x1) (B1 I1 x2)] ≟ ❴ ＜ id1, X x2, ｛ X x2 ｝_ ssk1 ˆ rs1 ＞ ❵_ pkRA ＾ r1)).
  repeat (rewrite If_true; rewrite If_false).

  reflexivity.
  all : ProveContext. Admitted.*)



(*Lemma prop2_lemma2:
  forall k x1 x2,
    R1 k x1 x2 = If EQ [(tau k [adv2 (B1 I0 x1) (B1 I1 x2)]) ; (B1 I0 x1)]
                    Then｛❴＜Pseudonym [pk0; vk0  ; np0], x1 ＞❵_pk0 ＾ rRA0｝_sskRA ˆ rsRA
                    Else (If EQ [(tau k [adv2 (B1 I0 x1) (B1 I1 x2)]) ; (B1 I1 x2)]
                             Then｛❴＜Pseudonym [pk1; vk1  ; np1], x2 ＞❵_pk1 ＾ rRA1 ｝_sskRA ˆ rsRA
                             Else (If Ver [τ2 (d k x1 x2); τ3 (d k x1 x2);  π2 (τ1 (d k x1 x2))]
                                      Then ｛❴＜P k x1 x2, τ2 (d k x1 x2)＞❵_(π1 (τ1 (d k x1 x2))) ＾ rRA2｝_sskRA ˆ rsRA
                                      Else Error
                                  )
                         ).
Admitted. *)

Lemma prop2_lemma2:
  forall k x1 x2,
    R1 k x1 x2 = If ((tau k [adv2 (B1 I0 x1) (B1 I1 x2)]) ≟ (B1 I0 x1)) Then｛❴＜Pseudonym [pk0; vk0 ; np0], X x1 ＞❵_pk0 ＾ rRA0 ｝_sskRA ˆ rsRA
           Else (If ((tau k [adv2 (B1 I0 x1) (B1 I1 x2)]) ≟ (B1 I1 x2)) Then｛❴＜Pseudonym [pk1; vk1 ; np1], X x2 ＞❵_pk1 ＾ rRA1 ｝_sskRA ˆ rsRA
           Else (If Ver [τ2 (d k x1 x2); τ3 (d k x1 x2);  π2 (τ1 (d k x1 x2))]
                 Then ｛❴＜Pseudonym [π1 (τ1 (d k x1 x2)); π2 (τ1 (d k x1 x2)); np2], τ2 (d k x1 x2)＞❵_(π1 (τ1 (d k x1 x2))) ＾ rRA2｝_sskRA ˆ rsRA Else Error )).
Proof.
  intros.
  rewrite <- prop2_lemma2_help.
  reflexivity. Qed.




 Definition Cont k x y u v w :=
    If tau k [adv2 ( x ) ( y )] ≟ x
       Then ｛ u  ｝_ sskRA ˆ rsRA
       Else (If tau k  [adv2 ( x ) ( y )] ≟ y
                Then ｛ v ｝_ sskRA ˆ rsRA
                Else (If Ver [τ2 ( w );
                             τ3 ( w );
                             π2 τ1 ( w )
                             ]
                         Then ｛ ❴ ＜ Pseudonym [π1 τ1 ( w );
                                                π2 τ1 ( w );
                                                If (π1 τ1 ( w )) ≟ pk0
                                                   Then np0
                                                   Else (If (π1 τ1 ( w )) ≟ pk1
                                                            Then np1
                                                            Else np2
                                                        )
                                                ],
                                   τ2 ( w )
                                      ＞
                                 ❵_ π1 τ1 ( w )
                                  ＾ rRA2
                              ｝_ sskRA ˆ rsRA
                         Else Error
                     )
            ).


(* Use  Lemma 2 to show this:
 
Lemma prop2_lemma3:
  forall k x1 x2 x ,  (x = ❴ ＜ aid I0, x1, ｛ x1 ｝_ ssk0 ˆ rs0 ＞ ❵_ pkRA ＾ r0)  \/ (x = ❴ ＜ aid I1, x2, ｛ x2 ｝_ ssk1 ˆ rs1 ＞ ❵_ pkRA ＾ r1) -> 
    R1 k x1 x2 = Cont k
                      ( ❴ ＜ aid I0, x1, ｛ x1 ｝_ ssk0 ˆ rs0 ＞ ❵_ pkRA ＾ r0 )
                      ( ❴ ＜ aid I1, x2, ｛ x2 ｝_ ssk1 ˆ rs1 ＞ ❵_ pkRA ＾ r1 )
                      ( ❴＜Pseudonym [pk0; vk0  ; np0], x1 ＞❵_pk0 ＾ rRA0 )
                      ( ❴＜Pseudonym [pk1; vk1  ; np1], x2 ＞❵_pk1 ＾ rRA1)
                      ( Dec_RAif ( x ) (tau k [adv2 (❴ ＜ aid I0, x1, ｛ x1 ｝_ ssk0 ˆ rs0 ＞ ❵_ pkRA ＾ r0 ) (❴ ＜ aid I1, x2, ｛ x2 ｝_ ssk1 ˆ rs1 ＞ ❵_ pkRA ＾ r1 ) ] ) ) .

Admitted.  *)


 
Goal cca_2 ->  [R1 K1 X1 X2] ~ [R1 K1 X2 X1].
  intros cca2.
  repeat rewrite prop2_lemma2.
  
(* Claim1: t2[x1 ,h(b1)] is (npkRA, r0, h(b0), z(h(b0)) −- CCA2 compliant *)
  pose (cca2 [nonce 8] [nonce 2] (*npkRA := nonce 8, r0 := nonce 2*)
       (fun x => [If tau K1 [adv2 x (B1 I1 X2)] ≟ x        Then ｛ ❴ ＜ Pseudonym [pk0; vk0; np0], X X1 ＞ ❵_ pk0 ＾ rRA0 ｝_ sskRA ˆ rsRA
          Else If tau K1 [adv2 x (B1 I1 X2)] ≟ B1 I1 X2 Then ｛ ❴ ＜ Pseudonym [pk1; vk1; np1], X X2 ＞ ❵_ pk1 ＾ rRA1 ｝_ sskRA ˆ rsRA
        Else If Ver [τ2 (d K1 X1 X2); τ3 (d K1 X1 X2); π2 τ1 (d K1 X1 X2)]
             Then ｛ ❴ ＜ Pseudonym [π1 τ1 (d K1 X1 X2); π2 τ1 (d K1 X1 X2); np2], τ2 (d K1 X1 X2) ＞ ❵_ π1 τ1 (d K1 X1 X2) ＾ rRA2 ｝_ sskRA ˆ rsRA Else Error])
       (＜id0, hbd0,｛hbd0｝_ssk0 ˆ rs0＞) (＜id0, hbd1,｛hbd1｝_ssk0 ˆ rs0＞)) as claim1.
    repeat rewrite EqLen in claim1.
    repeat rewrite If_true in claim1. 
  rewrite claim1.
  clear claim1.
  simpl.
  
(* Claim1': t2[x1 ,h(b1)] is (npk0, rRA0, h(b0), h(b1)) −- CCA2 compliant *)  
  pose (cca2 [nonce 0] [nonce 10] (*npk0 := nonce 0, rRA0 := nonce 10*)
       (fun x => [If τ1 (adv2 (B1 I0 X2) (B1 I1 X2)) ≟ (B1 I0 X2) Then ｛ x  ｝_ sskRA ˆ rsRA
          Else If τ1 (adv2 (B1 I0 X2) (B1 I1 X2)) ≟ (B1 I1 X2) Then ｛ ❴ ＜ Pseudonym [pk1; vk1; np1], hbd I1 ＞ ❵_ pk1 ＾ rRA1 ｝_ sskRA ˆ rsRA
          Else If Ver [τ2 (d K1 X1 X2); τ3 (d K1 X1 X2); π2 τ1 (d K1 X1 X2)]
               Then ｛ ❴ ＜ Pseudonym [π1 τ1 (d K1 X1 X2); π2 τ1 (d K1 X1 X2); np2], τ2 (d K1 X1 X2) ＞ ❵_ π1 τ1 (d K1 X1 X2) ＾ rRA2 ｝_ sskRA ˆ rsRA Else Error])
       (＜ Pseudonym [pk0; vk0; np0], hbd I0 ＞) (＜ Pseudonym [pk0; vk0; np0], hbd I1 ＞)) as claim1'.
    repeat rewrite EqLen in claim1'.
    repeat rewrite If_true in claim1'. 
  rewrite claim1'.
  clear claim1'.

(* Claim2: t2[h(b1) ,x2] is (npkRA, r1, h(b0), h(b1)) −- CCA2 compliant *)
  pose (cca2 [nonce 8] [nonce 6] (*npkRA := nonce 8, r0 := nonce 6*)
       (fun x => [If τ1 (adv2 (B1 I0 X2) x) ≟ B1 I0 X2 Then ｛ ❴ ＜ Pseudonym [pk0; vk0; np0], hbd I1 ＞ ❵_ pk0 ＾ rRA0 ｝_ sskRA ˆ rsRA
          Else If τ1 (adv2 (B1 I0 X2) x) ≟ x        Then ｛ ❴ ＜ Pseudonym [pk1; vk1; np1], hbd I0 ＞ ❵_ pk1 ＾ rRA1 ｝_ sskRA ˆ rsRA
        Else If Ver [τ2 (d K1 X2 X1); τ3 (d K1 X2 X1); π2 τ1 (d K1 X2 X1)]
             Then ｛ ❴ ＜ Pseudonym [π1 τ1 (d K1 X2 X1); π2 τ1 (d K1 X2 X1); np2], τ2 (d K1 X2 X1) ＞ ❵_ π1 τ1 (d K1 X2 X1) ＾ rRA2 ｝_ sskRA ˆ rsRA Else Error])
       (＜id1, hbd0,｛hbd0｝_ssk1 ˆ rs1＞) (＜id1, hbd1,｛hbd1｝_ssk1 ˆ rs1＞)) as claim2.
    repeat rewrite EqLen in claim2.
    repeat rewrite If_true in claim2. 
  rewrite claim2.
  clear claim2.
  
  
(* Claim2': t2[h(b1), x2] is (npk1, rRA1, h(b0), h(b1)) −- CCA2 compliant *)  
  pose (cca2 [nonce 4] [nonce 19] (*npk1 := nonce 4, rRA1 := nonce 19*)
       (fun x => [If τ1 (adv2 (B1 I0 X2) (B1 I1 X2)) ≟ (B1 I0 X2) Then ｛ ❴ ＜ Pseudonym [pk0; vk0; np0], hbd I1 ＞ ❵_ pk0 ＾ rRA0 ｝_ sskRA ˆ rsRA
          Else If τ1 (adv2 (B1 I0 X2) (B1 I1 X2)) ≟ (B1 I1 X2) Then ｛ x  ｝_ sskRA ˆ rsRA
          Else If Ver [τ2 (d K1 X2 X1); τ3 (d K1 X2 X1); π2 τ1 (d K1 X2 X1)]
               Then ｛ ❴ ＜ Pseudonym [π1 τ1 (d K1 X2 X1); π2 τ1 (d K1 X2 X1); np2], τ2 (d K1 X2 X1) ＞ ❵_ π1 τ1 (d K1 X2 X1) ＾ rRA2 ｝_ sskRA ˆ rsRA Else Error] )
       (＜ Pseudonym [pk1; vk1; np1], hbd I0 ＞) (＜ Pseudonym [pk1; vk1; np1], hbd I1 ＞)) as claim2'.
    repeat rewrite EqLen in claim2'.
    repeat rewrite If_true in claim2'. 
  rewrite claim2'.
  clear claim2'.

Abort.


  


(* Gergei guess we do not need IfBranch any more *)
Proposition prop2small : cca_2nolength
  -> [ bd I0 ; bd I1 ; B1 I0 X1 ; B1 I1 X1 ; R1 K1 X1 X2 ]
   ~
    [ bd I0 ; bd I1 ; B1 I0 X1 ; B1 I1 X1 ; R1 K1 X2 X1 ].
Proof.
  intros. unfold cca_2nolength in H.

  
 (* Use  Lemma 3 here *)
 
Admitted.



Proposition prop2 :
  cca_2
  -> [ bd I0 ; bd I1 ; B1 I0 X1 ; B1 I1 X2 ; R1 K1 X1 X2 ; R1 K2 X1 X2 ; R1 K3 X1 X2 ]
   ~
    [ bd I0 ; bd I1 ; B1 I0 X2 ; B1 I1 X1 ; R1 K1 X2 X1 ; R1 K2 X2 X1 ; R1 K3 X2 X1 ].

Proof.
  intros. repeat rewrite prop2_lemma2.
Admitted. 








(* Only keep those that are needed: *)

Lemma If_morph2 :(*"<<<"*)  forall  {f}  , (*">>>"*)
              forall  {b x1 y1 x2 y2} {u} ,
      (*"<<<"*)ContextTerm General List f -> (*">>>"*)
    (f ((If b Then x1 Else y1) :: (If b Then x2 Else y2) :: u))
  = (If b Then (f (x1 :: x2 :: u) ) Else (f (y1 :: y2 :: u))). Admitted.


Lemma If_morph3 :(*"<<<"*)  forall  {f}  , (*">>>"*)
              forall  {b x1 y1 x2 y2 x3 y3} {u} ,
      (*"<<<"*)ContextTerm General List f -> (*">>>"*)
    (f ((If b Then x1 Else y1) :: (If b Then x2 Else y2) :: (If b Then x3 Else y3) :: u))
  = (If b Then (f (x1 :: x2 :: x3 :: u) ) Else (f (y1 :: y2 :: y3 :: u))). Admitted.


(**)
Lemma If_simpl : forall {b1 b2 b bla1 bla2 bla},
  (If (If b1 Then TRue Else (If b2 Then TRue Else b)) Then (If b1 Then bla1 Else (If b2 Then bla2 Else bla)) Else Error)
 =
  (If b1 Then bla1 Else If b2 Then bla2 Else If b Then bla Else Error).
  intros.
  repeat rewrite (@If_morph (fun x => If x Then _ Else Error)).
  repeat rewrite If_true.
  rewrite (@If_eval (fun b1  => If b1 Then bla1 Else If b2 Then bla2 Else bla )
                    (fun b1  => If b2 Then If b1 Then bla1 Else If b2 Then bla2 Else bla Else If b Then If b1 Then bla1 Else If b2 Then bla2 Else bla Else Error )).
  repeat rewrite If_true.
  repeat rewrite If_false. 
  rewrite (@If_eval (fun b2  => If b2 Then bla2 Else bla  )
                    (fun b2 =>  If b Then If b2 Then bla2 Else bla Else Error)).
  repeat rewrite If_true.
  repeat rewrite If_false.
  reflexivity.
  all : ProveContext.
Qed.
  
