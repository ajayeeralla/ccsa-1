(************************************************************************)
(* Copyright (c) 2021, Gergei Bana, Qianli Zhang                        *)   
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)
  
Require Export Coq.Lists.List.
Import ListNotations.
Require Import Coq.micromega.Lia.
Require Export prop2.


(* Φ : U+03A6 *)


Parameter TextExtracts : Symbols Deterministic (narg 1).
Notation Extracts := (FuncInt Deterministic (narg 1) TextExtracts).

Notation Extr m := (Extracts [m]). 

Axiom ExtText : forall m ssk rs, Extr (｛m｝_ssk ˆ rs) = m.

Axiom Ceq_ref: forall m,  ((m ≟ m) = TRue). 

Notation Dec_i i c := (Dec [c; (sk i)]).
Notation adv5 t2_vec:= (adv (5) t2_vec).



(* 
Definition t2_vec x1 x2 := [ bd I0 ; bd I1 ; B1 I0 (Ｈ x1) ; B1 I1 (Ｈ x2) ; R1 K1 (Ｈ x1) (Ｈ x2)].

Definition B2 i x1 x2 := (If Ver [Extract (adv5 (t2_vec x1 x2)); (adv5 (t2_vec x1 x2)); vkRA]
                             Then If ((Ｈ x1) ≟ π2 (Dec_i i (Extract (adv5 (t2_vec x1 x2))))) (*should be (X i) ≟  bla*)
                                     Then ＜ (  ❴ x1 ❵_pkS ＾ (r i)), (*Should be a different r i*)
                                             (｛❴ x1 ❵_pkS ＾ (r i)｝_(ssk i) ˆ (rs i)),
                                             ( π1 (Dec_i i (Extract (adv5 (t2_vec x1 x2))))) ＞
                                     Else Error                                                  
                             Else Error).

Definition t1 k x1 := (｛❴＜Pseudonym [pk0; vk0; np k], (Ｈ x1) ＞❵_pk0 ＾ (rRA k)｝_sskRA ˆ rsRA).
Definition t2 k x2 := (｛❴＜Pseudonym [pk1; vk1; np k], (Ｈ x2) ＞❵_pk1 ＾ (rRA k)｝_sskRA ˆ rsRA).

Definition C21 k x1 x2 := ((adv5 (t2_vec x1 x2)) ≟ (t1 k x1)).
Definition C22 k x1 x2 := ((adv5 (t2_vec x1 x2)) ≟ (t2 k x2)).

Lemma prop3_lemma3_small: forall x1 x2 k,
    B2 I0 x1 x2 =  If C21 k x1 x2 Then ＜ ❴ x1 ❵_ pkS ＾ r0, ｛ ❴ x1 ❵_ pkS ＾ r0 ｝_ ssk0 ˆ rs0, Pseudonym [pk0; vk0; np k] ＞
                   Else B2 I0 x1 x2.
Proof.
  intros.
  rewrite <- (@If_same (C21 k x1 x2) (B2 I0 x1 x2)) at 1.
  rewrite (Eq_branch (adv5 (t2_vec x1 x2)) (｛ ❴ ＜ Pseudonym [pk0; vk0; np k], (Ｈ x1) ＞ ❵_ pk0 ＾ rRA k ｝_ sskRA ˆ rsRA)
                   (fun x:ppt => If Ver [Extract x; x; vkRA]
                                 Then If Ｈ x1 ≟ (π2 Dec_i I0 (Extract x))
                                      Then ＜ ❴ x1 ❵_ pkS ＾ r I0, ｛ ❴ x1 ❵_ pkS ＾ r I0 ｝_ ssk I0 ˆ rs I0, π1 Dec_i I0 (Extract x) ＞
                                      Else Error
                                      Else Error)); simpl.
  repeat rewrite ExtText.
  rewrite correctness.
(* Since kIndex is not 'recursively' defined, just destruct k *)
  destruct k;
    simpl;
    rewrite decenc;
    rewrite proj2pair;
    rewrite proj1pair;
    rewrite Ceq_ref;
    repeat rewrite If_true;
    reflexivity.
  ProveContext. Qed.


Lemma prop3_lemma3: forall x1 x2,
    B2 I0 x1 x2 = (If C21 K1 x1 x2 Then ＜ ❴ x1 ❵_ pkS ＾ r0, ｛ ❴ x1 ❵_ pkS ＾ r0 ｝_ ssk0 ˆ rs0, Pseudonym [pk0; vk0; np1] ＞
             Else (If C21 K2 x1 x2 Then ＜ ❴ x1 ❵_ pkS ＾ r0, ｛ ❴ x1 ❵_ pkS ＾ r0 ｝_ ssk0 ˆ rs0, Pseudonym [pk0; vk0; np2] ＞
             Else (If C21 K3 x1 x2 Then ＜ ❴ x1 ❵_ pkS ＾ r0, ｛ ❴ x1 ❵_ pkS ＾ r0 ｝_ ssk0 ˆ rs0, Pseudonym [pk0; vk0; np3] ＞
                   Else (If C21 K1 x1 x2 Then Error
                   Else (If C21 K2 x1 x2 Then Error
                   Else (If C21 K3 x1 x2 Then Error Else (B2 I0 x1 x2))))))). Abort. 


Proposition prop3_small: cca_2 ->
  [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ; R1 K1 hbd0 hbd1; B2 I0 bd0 bd1]
 ~
  [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ; R1 K1 hbd1 hbd0; B2 I0 bd1 bd0]. Abort. *)






Notation Dec_RAphi i k x1 x2 := match i with I0 => Dec_RAif (B1 I0 x1) (tau k [adv2 (B1 I0 x1) (B1 I1 x2)])
                                           | I1 => Dec_RAif (B1 I1 x2) (tau k [adv2 (B1 I0 x1) (B1 I1 x2)]) end.

(* Attention:  R1 k x1 x2 ≠ Phi2 k x1 x2.  *)
Definition Phi2 i k x1 x2 :=
     (If C1 k x1 x2 Then｛❴＜Pseudonym [pk0; vk0  ; np11], x1 ＞❵_pk0 ＾ rRA11 ｝_sskRA ˆ rsRA (*use np11 and rRA11 here*)
Else (If C2 k x1 x2 Then｛❴＜Pseudonym [pk1; vk1  ; np12], x2 ＞❵_pk1 ＾ rRA12 ｝_sskRA ˆ rsRA (*use np12 and rRA12 here*)
Else (If Ver [τ2 (Dec_RAphi i k x1 x2);
              τ3 (Dec_RAphi i k x1 x2);
              π2 τ1 (Dec_RAphi i k x1 x2)]
      Then ｛ ❴ ＜ Pseudonym [π1 τ1 (Dec_RAphi i k x1 x2);
                             π2 τ1 (Dec_RAphi i k x1 x2); np1],
                τ2 (Dec_RAphi i k x1 x2) ＞
              ❵_ π1 τ1 (Dec_RAphi i k x1 x2) ＾ rRA1 ｝_ sskRA ˆ rsRA Else Error))).



Definition t2_vec i x1 x2 := [ bd I0 ; bd I1 ; B1 I0 (Ｈ x1) ; B1 I1 (Ｈ x2) ; Phi2 i K1 (Ｈ x1) (Ｈ x2)].

Definition B2 i x1 x2 := (If Ver [Extr (adv5 (t2_vec i x1 x2)); (adv5 (t2_vec i x1 x2)); vkRA]
                             Then If ((Ｈ x1) ≟ π2 (Dec_i i (Extr (adv5 (t2_vec i x1 x2))))) (*should be (X i) ≟  bla*)
                                     Then ＜ (  ❴ x1 ❵_pkS ＾ (r_3 i)), (*Should be a different r i*)
                                             (｛❴ x1 ❵_pkS ＾ (r_3 i)｝_(ssk i) ˆ (rs i)),
                                             ( π1 (Dec_i i (Extr(adv5 (t2_vec i x1 x2))))) ＞
                                     Else Error                                                  
                             Else Error).

Definition t1 k x1 := (｛❴＜Pseudonym [pk0; vk0; np k], (Ｈ x1) ＞❵_pk0 ＾ (rRA k)｝_sskRA ˆ rsRA).
Definition t2 k x2 := (｛❴＜Pseudonym [pk1; vk1; np k], (Ｈ x2) ＞❵_pk1 ＾ (rRA k)｝_sskRA ˆ rsRA).

Definition C21 i k x1 x2 := ((adv5 (t2_vec i x1 x2)) ≟ (t1 k x1)).
Definition C22 i k x1 x2 := ((adv5 (t2_vec i x1 x2)) ≟ (t2 k x2)).

Lemma prop3_lemma3_weak: forall k x1 x2,
    B2 I0 x1 x2 =  If C21 I0 k x1 x2 Then ＜ ❴ x1 ❵_ pkS ＾ (r_3 I0), ｛ ❴ x1 ❵_ pkS ＾ (r_3 I0) ｝_ ssk0 ˆ rs0, Pseudonym [pk0; vk0; np k] ＞
                   Else B2 I0 x1 x2.
Proof.
  intros.
  rewrite <- (@If_same (C21 I0 k x1 x2) (B2 I0 x1 x2)) at 1.
  rewrite (Eq_branch (adv5 (t2_vec I0 x1 x2)) (｛ ❴ ＜ Pseudonym [pk0; vk0; np k], (Ｈ x1) ＞ ❵_ pk0 ＾ rRA k ｝_ sskRA ˆ rsRA)
                   (fun x:ppt => If Ver [Extr x; x; vkRA]
                                 Then If Ｈ x1 ≟ (π2 Dec_i I0 (Extr x))
                                      Then ＜ ❴ x1 ❵_ pkS ＾ (r_3 I0), ｛ ❴ x1 ❵_ pkS ＾ (r_3 I0) ｝_ ssk I0 ˆ rs I0, π1 Dec_i I0 (Extr x) ＞
                                      Else Error
                                      Else Error)); simpl.
  repeat rewrite ExtText.
  rewrite correctness.
(* Since kIndex is not 'recursively' defined, just destruct k *)
  destruct k;
    simpl;
    rewrite decenc;
    rewrite proj2pair;
    rewrite proj1pair;
    rewrite Ceq_ref;
    repeat rewrite If_true;
    reflexivity.
  ProveContext. Qed.


Proposition prop3_small: cca_2 ->
  [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ; Phi2 I0 K1 hbd0 hbd1; B2 I0 bd0 bd1]
 ~
  [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ; Phi2 I1 K1 hbd1 hbd0; B2 I0 bd1 bd0].
Proof.
  intros cca2.
  unfold cca_2 in cca2.
  apply CCA2toCCA2L in cca2.

  rewrite prop3_lemma3_weak with (k := K1).
  (* Claim1:   t2[x1 ,h(b1)] is (npkRA, r0, h(b0), h(b1)) −- CCA2 compliant. npkRA := nonce 8, r0 := nonce 2 *)
  unfold B2 at 1. unfold C21. unfold t2_vec. (* shit *)

  
  (* Claim1':  t2[x1 ,h(b1)] is (npk0, rRA11, h(b0), h(b1)) −- CCA2 compliant. npk0 := nonce 0, rRA11 := nonce 101 *)

  (* Claim1'': t2[x1 ,h(b1)] is (npkS, r10, h(b0), h(b1)) −- CCA2 compliant. npkS := nonce 12, r10 := nonce 200 *)
  
  rewrite prop3_lemma3_weak with (k := K1).
  (* Claim2:   t2[h(b1) ,x2] is (npkRA, r1, h(b0), h(b1)) −- CCA2 compliant.  npkRA := nonce 8, r0 := nonce 6*)

  (* Claim2':  t2[h(b1), x2] is (npk1, rRA12, h(b0), h(b1)) −- CCA2 compliant. npk1 := nonce 4, rRA12 := nonce 191*)

  (* Claim2'': t2[h(b1) ,x2] is (npkS, r11, h(b0), h(b1)) −- CCA2 compliant. npkS := nonce 12, r11 := nonce 600 *)

(*If_eval*)

Abort.



  

  
(* This is a brother of prop2_small *)
Proposition prop2_weak: cca_2 ->
  [ bd I0 ; bd I1 ; B1 I0 hbd0 ; B1 I1 hbd1 ; Phi2 I0 K1 hbd0 hbd1]
 ~
  [ bd I0 ; bd I1 ; B1 I0 hbd1 ; B1 I1 hbd0 ; Phi2 I1 K1 hbd1 hbd0].
Proof.
  intros cca2. unfold cca_2 in cca2.
  apply CCA2toCCA2L in cca2.
  
(* Claim1: t2[x1 ,h(b1)] is (npkRA, r0, h(b0), h(b1)) −- CCA2 compliant. npkRA := nonce 8, r0 := nonce 2 *)
  pose (cca2 [nonce 8] [nonce 2]
             (fun x1 =>  [bd0; bd1; x1; B1 I1 hbd1;
  If τ1 (adv2 x1 (B1 I1 hbd1)) ≟ x1 Then ｛ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd0 ＞ ❵_ pk0 ＾ rRA11 ｝_ sskRA ˆ rsRA
  Else If τ1 (adv2 x1 (B1 I1 hbd1)) ≟ B1 I1 hbd1 Then ｛ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12 ｝_ sskRA ˆ rsRA
       Else If Ver
                 [τ2 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1)))); τ3 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1))));
                 π2 τ1 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1))))]
            Then ｛ ❴ ＜ Pseudonym
                         [π1 τ1 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1)))); π2 τ1 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1))));
                         np1], τ2 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1)))) ＞
                   ❵_ π1 τ1 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1)))) ＾ rRA1 ｝_ sskRA ˆ rsRA Else Error])
       (＜ aid I0, hbd0, ｛ hbd0 ｝_ ssk0 ˆ rs0 ＞) (＜ aid I0, hbd1, ｛ hbd1 ｝_ ssk0 ˆ rs0 ＞)) as claim1.
  rewrite claim1.
  clear claim1. 


(* Claim1': t2[x1 ,h(b1)] is (npk0, rRA11, h(b0), h(b1)) −- CCA2 compliant. npk0 := nonce 0, rRA11 := nonce 101 *)
  pose (cca2 [nonce 0] [nonce 101]
             (fun x1 => [bd I0; bd I1; B1 I0 hbd1; B1 I1 hbd1;
                      (If C1 K1 hbd1 hbd1 Then｛ x1 ｝_sskRA ˆ rsRA (*use np11 and rRA11 here*)
                 Else (If C2 K1 hbd1 hbd1 Then｛❴＜Pseudonym [pk1; vk1; np12], hbd1 ＞❵_pk1 ＾ rRA12 ｝_sskRA ˆ rsRA (*use np12 and rRA12 here*)
                 Else (If Ver [τ2 (Dec_RAphi I0 K1 hbd1 hbd1); τ3 (Dec_RAphi I0 K1 hbd1 hbd1); π2 τ1 (Dec_RAphi I0 K1 hbd1 hbd1)]
                       Then ｛ ❴ ＜ Pseudonym [π1 τ1 (Dec_RAphi I0 K1 hbd1 hbd1); π2 τ1 (Dec_RAphi I0 K1 hbd1 hbd1); np1],
                                   τ2 (Dec_RAphi I0 K1 hbd1 hbd1) ＞
                               ❵_ π1 τ1 (Dec_RAphi I0 K1 hbd1 hbd1) ＾ rRA1 ｝_ sskRA ˆ rsRA Else Error)))])
       (＜ Pseudonym [pk0; vk0; np11], hbd0 ＞) (＜ Pseudonym [pk0; vk0; np11], hbd1 ＞)) as claim1'.
  rewrite claim1'.
  clear claim1'.


  
(* Claim2: t2[h(b1) ,x2] is (npkRA, r1, h(b0), h(b1)) −- CCA2 compliant.  npkRA := nonce 8, r0 := nonce 6*)
   pose (cca2 [nonce 8] [nonce 6]
              (fun x2 =>  [bd I0; bd I1; B1 I0 hbd1; x2;
  If tau K1 [adv2 (B1 I0 hbd1) x2] ≟ B1 I0 hbd1 Then ｛ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11 ｝_ sskRA ˆ rsRA
  Else If tau K1 [adv2 (B1 I0 hbd1) x2] ≟ x2  Then ｛ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd0 ＞ ❵_ pk1 ＾ rRA12 ｝_ sskRA ˆ rsRA
       Else If Ver
                 [τ2 (Dec_RAif x2 (tau K1 [adv2 (B1 I0 hbd1) x2])); τ3 (Dec_RAif x2 (tau K1 [adv2 (B1 I0 hbd1) x2]));
                 π2 τ1 (Dec_RAif x2 (tau K1 [adv2 (B1 I0 hbd1) x2]))]
            Then ｛ ❴ ＜ Pseudonym
                         [π1 τ1 (Dec_RAif x2 (tau K1 [adv2 (B1 I0 hbd1) x2]));
                         π2 τ1 (Dec_RAif x2 (tau K1 [adv2 (B1 I0 hbd1) x2])); np1],
                     τ2 (Dec_RAif x2 (tau K1 [adv2 (B1 I0 hbd1) x2])) ＞
                   ❵_ π1 τ1 (Dec_RAif x2 (tau K1 [adv2 (B1 I0 hbd1) x2])) ＾ rRA1 ｝_ sskRA ˆ rsRA Else Error])
       (＜ aid I1, hbd0, ｛ hbd0 ｝_ ssk1 ˆ rs1 ＞) (＜ aid I1, hbd1, ｛ hbd1 ｝_ ssk1 ˆ rs1 ＞)) as claim2.
  rewrite claim2.
  clear claim2.


(* Claim2': t2[h(b1), x2] is (npk1, rRA12, h(b0), h(b1)) −- CCA2 compliant. npk1 := nonce 4, rRA12 := nonce 191*)
  pose (cca2 [nonce 4] [nonce 191]
             (fun x2 => [bd I0; bd I1; B1 I0 hbd1; B1 I1 hbd1;
                      (If C1 K1 hbd1 hbd1 Then｛ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11 ｝_sskRA ˆ rsRA
                 Else (If C2 K1 hbd1 hbd1 Then｛ x2  ｝_sskRA ˆ rsRA
                 Else (If Ver [τ2 (Dec_RAphi I1 K1 hbd1 hbd1); τ3 (Dec_RAphi I1 K1 hbd1 hbd1); π2 τ1 (Dec_RAphi I1 K1 hbd1 hbd1)]
                       Then ｛ ❴ ＜ Pseudonym [π1 τ1 (Dec_RAphi I1 K1 hbd1 hbd1); π2 τ1 (Dec_RAphi I1 K1 hbd1 hbd1); np1],
                                   τ2 (Dec_RAphi I1 K1 hbd1 hbd1) ＞
                               ❵_ π1 τ1 (Dec_RAphi I1 K1 hbd1 hbd1) ＾ rRA1 ｝_ sskRA ˆ rsRA Else Error)))])
       (＜ Pseudonym [pk1; vk1; np12], hbd0 ＞) (＜ Pseudonym [pk1; vk1; np12], hbd1 ＞)) as claim2'.
  rewrite claim2'.
  clear claim2'.


  
  rewrite (@If_eval (fun _ => _ )
                    (fun x => If _ Then _ 
            Else If Ver [τ2 (If x Then Error Else _); τ3 (If x Then Error Else _); π2 τ1 (If x Then Error Else _)]
            Then ｛ ❴ ＜ Pseudonym [π1 τ1 (If x Then Error Else _); π2 τ1 (If x Then Error Else _); np1], τ2 (If x Then Error Else _) ＞
                    ❵_ π1 τ1 (If x Then Error Else _) ＾ rRA1 ｝_ sskRA ˆ rsRA Else Error)
                    (C1 K1 hbd1 hbd1)).
  rewrite If_false.

  rewrite (@If_eval (fun _ => _ )
                    (fun x => If Ver [τ2 (If x Then Error Else _); τ3 (If x Then Error Else _); π2 τ1 (If x Then Error Else _)]
                     Then ｛ ❴ ＜ Pseudonym [π1 τ1 (If x Then Error Else _); π2 τ1 (If x Then Error Else _); np1], τ2 (If x Then Error Else _) ＞
                             ❵_ π1 τ1 (If x Then Error Else _) ＾ rRA1 ｝_ sskRA ˆ rsRA Else Error)
                    (C2 K1 hbd1 hbd1)).
  rewrite If_false.
  
  reflexivity.

Admitted.


  

(* CCA2After Experimence *)

Goal  CCA2AfterEnc Dec Pkey Skey Error [nonce (8)] [nonce (2)]
   (fun x1 : ppt =>
    [bd0; bd1; x1; B1 I1 hbd1;
    If τ1 (adv2 x1 (B1 I1 hbd1)) ≟ x1 Then ｛ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd0 ＞ ❵_ pk0 ＾ rRA11 ｝_ sskRA ˆ rsRA
    Else If τ1 (adv2 x1 (B1 I1 hbd1)) ≟ B1 I1 hbd1 Then ｛ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12 ｝_ sskRA ˆ rsRA
         Else If Ver [τ2 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1)))); τ3 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1)))); π2 τ1 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1))))]
              Then ｛ ❴ ＜ Pseudonym [π1 τ1 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1)))); π2 τ1 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1)))); np1],
                        τ2 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1)))) ＞ ❵_ π1 τ1 (Dec_RAif x1 (τ1 (adv2 x1 (B1 I1 hbd1)))) ＾ rRA1 ｝_ sskRA ˆ rsRA Else Error]).
  ProveCca2After. Qed.

Goal CCA2AfterEnc Dec Pkey Skey Error [nonce (0)] [nonce (101)]
   (fun x1 : ppt =>
    [bd I0; bd I1; B1 I0 hbd1; B1 I1 hbd1;
    If C1 K1 hbd1 hbd1 Then ｛ x1 ｝_ sskRA ˆ rsRA
    Else If C2 K1 hbd1 hbd1 Then ｛ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd1 ＞ ❵_ pk1 ＾ rRA12 ｝_ sskRA ˆ rsRA
         Else If Ver
                   [τ2 (Dec_RAif (B1 I0 hbd1) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd1)])); τ3 (Dec_RAif (B1 I0 hbd1) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd1)]));
                   π2 τ1 (Dec_RAif (B1 I0 hbd1) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd1)]))]
              Then ｛ ❴ ＜ Pseudonym
                           [π1 τ1 (Dec_RAif (B1 I0 hbd1) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd1)]));
                           π2 τ1 (Dec_RAif (B1 I0 hbd1) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd1)])); np1],
                       τ2 (Dec_RAif (B1 I0 hbd1) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd1)])) ＞
                      ❵_ π1 τ1 (Dec_RAif (B1 I0 hbd1) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd1)])) ＾ rRA1 ｝_ sskRA ˆ rsRA Else Error]).
  ProveCca2After. Qed.


Goal CCA2AfterEnc Dec Pkey Skey Error [nonce (8)] [nonce (6)]
   (fun x2 : ppt =>
    [bd I0; bd I1; B1 I0 hbd1; x2;
    If tau K1 [adv2 (B1 I0 hbd1) x2] ≟ B1 I0 hbd1 Then ｛ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11 ｝_ sskRA ˆ rsRA
    Else If tau K1 [adv2 (B1 I0 hbd1) x2] ≟ x2 Then ｛ ❴ ＜ Pseudonym [pk1; vk1; np12], hbd0 ＞ ❵_ pk1 ＾ rRA12 ｝_ sskRA ˆ rsRA
         Else If Ver
                   [τ2 (Dec_RAif x2 (tau K1 [adv2 (B1 I0 hbd1) x2])); τ3 (Dec_RAif x2 (tau K1 [adv2 (B1 I0 hbd1) x2]));
                   π2 τ1 (Dec_RAif x2 (tau K1 [adv2 (B1 I0 hbd1) x2]))]
              Then ｛ ❴ ＜ Pseudonym [π1 τ1 (Dec_RAif x2 (tau K1 [adv2 (B1 I0 hbd1) x2])); π2 τ1 (Dec_RAif x2 (tau K1 [adv2 (B1 I0 hbd1) x2])); np1],
                        τ2 (Dec_RAif x2 (tau K1 [adv2 (B1 I0 hbd1) x2])) ＞ ❵_ π1 τ1 (Dec_RAif x2 (tau K1 [adv2 (B1 I0 hbd1) x2])) ＾ rRA1 ｝_ sskRA ˆ rsRA Else Error]).
  ProveCca2After. Qed.

Goal CCA2AfterEnc Dec Pkey Skey Error [nonce (4)] [nonce (191)]
   (fun x2 : ppt =>
    [bd I0; bd I1; B1 I0 hbd1; B1 I1 hbd1;
    If C1 K1 hbd1 hbd1 Then ｛ ❴ ＜ Pseudonym [pk0; vk0; np11], hbd1 ＞ ❵_ pk0 ＾ rRA11 ｝_ sskRA ˆ rsRA
    Else If C2 K1 hbd1 hbd1 Then ｛ x2 ｝_ sskRA ˆ rsRA
         Else If Ver
                   [τ2 (Dec_RAif (B1 I1 hbd1) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd1)])); τ3 (Dec_RAif (B1 I1 hbd1) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd1)]));
                   π2 τ1 (Dec_RAif (B1 I1 hbd1) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd1)]))]
              Then ｛ ❴ ＜ Pseudonym
                           [π1 τ1 (Dec_RAif (B1 I1 hbd1) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd1)]));
                           π2 τ1 (Dec_RAif (B1 I1 hbd1) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd1)])); np1],
                       τ2 (Dec_RAif (B1 I1 hbd1) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd1)])) ＞
                      ❵_ π1 τ1 (Dec_RAif (B1 I1 hbd1) (tau K1 [adv2 (B1 I0 hbd1) (B1 I1 hbd1)])) ＾ rRA1 ｝_ sskRA ˆ rsRA Else Error]).
  ProveCca2After. Qed.
