(************************************************************************)
(* Copyright (c) 2021, Gergei Bana, Qianli Zhang                        *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

(*   Unicode used in this file:
 *   1.'｛' :     U+FF5B
 *   2.'｝' :     U+FF5D
 *   3.'ˆ'  :     U+02C6
 *   4.'Ｈ' :     U+03C0
 *   5.'⓪' :     U+24EA
     6. 'τ' :     U+03C4*)

Require Import Coq.Lists.List.
Require Import Coq.micromega.Lia.
Import ListNotations.
Require Export AuxiliaryTheoremEquality.



(**************************
 **   Nonce management   **
 **************************)

Notation pk0  := (Pkey [nonce 0]).
Notation sk0  := (Skey [nonce 0]).
Notation vk0  := (Pkey [nonce 1]).
Notation ssk0 := (Skey [nonce 1]).
Notation r0   := (Rand [nonce 2]).
Notation rs0  := (Rand [nonce 3]).

Notation pk1  := (Pkey [nonce 4]).
Notation sk1  := (Skey [nonce 4]).
Notation vk1  := (Pkey [nonce 5]).
Notation ssk1 := (Skey [nonce 5]).
Notation r1   := (Rand [nonce 6]).
Notation rs1  := (Rand [nonce 7]).


Notation pkRA  := (Pkey [nonce 8]).
Notation skRA  := (Skey [nonce 8]).
Notation vkRA  := (Pkey [nonce 9]).
Notation sskRA := (Skey [nonce 9]).

Notation rRA0   := (Rand [nonce 10]).
Notation rRA1   := (Rand [nonce 19]).
Notation rRA2   := (Rand [nonce 20]).

Notation rsRA  := (Rand [nonce 11]).

Notation pkS  := (Pkey [nonce 12]).
Notation skS  := (Skey [nonce 12]).
Notation vkS  := (Pkey [nonce 13]).
Notation sskS := (Skey [nonce 13]).
Notation rS   := (Rand [nonce 14]).
Notation rsS  := (Rand [nonce 15]).


(**********************************
 ** Fundamental Function Symbols **
 **********************************)

Parameter Signs : Symbols Deterministic (narg 3).
Notation "'Sign'" := (FuncInt Deterministic (narg 3) Signs).
Notation "'｛' m '｝_' sk 'ˆ' r " := (Sign [m; sk; r]) (at level 101, left associativity).

Parameter Vers : Symbols Deterministic (narg 3).
Notation "'Ver'" := (FuncInt Deterministic (narg 3) Vers).

Parameter Hashs : Symbols Deterministic (narg 1).
Notation "'Hash'" := (FuncInt Deterministic (narg 1) Hashs).
Notation "'Ｈ' m " := (Hash [m]) (at level 101, left associativity).

Parameter Zeros : Symbols Deterministic (narg 1).
Notation "'Zero'" := (FuncInt Deterministic (narg 1) Zeros).
Notation "'⓪' m " := (Hash [m]) (at level 101, left associativity).

Parameter Triples : Symbols Deterministic (narg 3).
Notation "'Triple'" := (FuncInt Deterministic (narg 3) Triples).

Parameter Taus1 : Symbols Deterministic (narg 1).
Notation "'Tau1'" := (FuncInt Deterministic (narg 1) Taus1).
Notation τ1 x := (Tau1 [x]).
Parameter Taus2 : Symbols Deterministic (narg 1).
Notation "'Tau2'" := (FuncInt Deterministic (narg 1) Taus2).
Notation τ2 x := (Tau2 [x]).
Parameter Taus3 : Symbols Deterministic (narg 1).
Notation "'Tau3'" := (FuncInt Deterministic (narg 1) Taus3).
Notation τ3 x := (Tau3 [x]).

Parameter Bidding0s : Symbols Deterministic (narg 0).
Notation bd0 := (ConstInt Deterministic Bidding0s).

Parameter Bidding1s : Symbols Deterministic (narg 0).
Notation bd1 := (ConstInt Deterministic Bidding1s).


(*******************
 **   Notations   **
 *******************)

Notation id0 := (＜pk0 , vk0＞).
Notation id1 := (＜pk1,  vk1＞).

Notation "'＜' c1 ',' c2 '＞'" := (Pair [c1; c2]) (at level 100, right associativity). (* U+FF1C and U+FF1E)*)
Notation "'＜' c1 ',' c2 ',' c3 '＞'" := (Triple [c1; c2; c3]) (at level 100, right associativity). (*Here is a overload of the ＜,＞ notation*)

Notation hbd0 := (Ｈ bd0).
Notation hbd1 := (Ｈ bd1).

Notation "'cca_2'" := (CCA2M Len Enc Dec Pkey Skey Rand Error 1 1).

Notation B1_0 := (fun x => ❴＜ id0,  x, ｛x｝_ssk0 ˆ rs0 ＞❵_pkRA ＾ r0).
Notation B1_1 := (fun x => ❴＜ id1,  x, ｛x｝_ssk1 ˆ rs1 ＞❵_pkRA ＾ r1).

(**********************
 ** Auxiliary Axioms **
 **********************)

Axiom correctness :  forall {m n r}, Ver [m; ｛m｝_ (Skey [n]) ˆ (Rand [r]);  Pkey [n]] = TRue.

(* Axiom EqHashLen: forall u u', (⓪ (Ｈ u)) = (⓪ (Ｈ u')).*)

(***********************
 **   Proposition 1   **
 ***********************)

Theorem prop1: cca_2 ->
  [bd0;  bd1;  ❴＜id0, hbd0,｛hbd0｝_ssk0 ˆ rs0＞❵_pkRA ＾ r0;  ❴＜id1, hbd1,｛hbd1｝_ssk1 ˆ rs1＞❵_pkRA ＾ r1]
 ~
  [bd0;  bd1;  ❴＜id0, hbd1,｛hbd1｝_ssk0 ˆ rs0＞❵_pkRA ＾ r0;  ❴＜id1, hbd0,｛hbd0｝_ssk1 ˆ rs1＞❵_pkRA ＾ r1].
Proof.
  intros cca2.
  (*apply cca2 in the first encryption place*)
  assert (forall h0 h1,
    [bd0; bd1; B1_0 h0; B1_1 h1]
   =
    [bd0; bd1; If |＜id0, h0,｛h0｝_ssk0 ˆ rs0＞| ≟ |＜id0, h1,｛h1｝_ssk0 ˆ rs0＞| Then B1_0 h0 Else Error; B1_1 h1]) as H0.
  intros; repeat rewrite EqLen in *; repeat rewrite If_true in *; reflexivity.
  rewrite H0.
  rewrite (cca2 [nonce 8] [nonce 2]  (fun x => [bd0; bd1; x; B1_1 hbd1]) (＜id0, hbd0,｛hbd0｝_ssk0 ˆ rs0＞) (＜id0, hbd1,｛hbd1｝_ssk0 ˆ rs0＞)).

(*apply cca2 in the second encryption place*)
  assert (forall h0 h1,
    [bd0; bd1; B1_0 h1; B1_1 h0]
   =
    [bd0; bd1; B1_0 h1; If |＜id1, h0,｛h0｝_ssk1 ˆ rs1＞| ≟ |＜id1, h1,｛h1｝_ssk1 ˆ rs1＞| Then B1_1 h0 Else Error]) as H1.
  intros; repeat rewrite EqLen in *; repeat rewrite If_true in *; reflexivity.
  rewrite H1.
  rewrite (cca2 [nonce 8] [nonce 6]  (fun x => [bd0; bd1; B1_0 hbd1; x]) (＜id1, hbd0,｛hbd0｝_ssk1 ˆ rs1＞) (＜id1, hbd1,｛hbd1｝_ssk1 ˆ rs1＞)).

(*Prove context*)
  repeat rewrite EqLen in *; repeat rewrite If_true in *; reflexivity.
  all : ProveCCA2.
  ProveListFresh; try (lia; constructor); repeat constructor. (* TODO: Refine ProveListFresh*)
  Admitted. 
