
(************************************************************************)
(* Copyright (c) 2020, Gergei Bana                                      *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Setoid.
Require Import Coq.micromega.Lia.
Import ListNotations.

Require Export FreshnessEquality.



Definition Pairs : Symbols Deterministic (narg 2) :=
  symbols Deterministic (narg 2) [1].


Notation "'Pair'" := (FuncInt Deterministic (narg 2) Pairs).

Notation "'＜' c1 ',' c2 '＞'" := (Pair [c1; c2]) (at level 100, right associativity). (* U+FF1C and U+FF1E)*)




Proposition ctpair :
       forall {hag tol tc1 tc2},
         ContextTerm hag tol tc1
         -> ContextTerm hag tol tc2
         -> ContextTerm hag tol (fun x => (Pair ([ (tc1 x) ; (tc2 x)]) )).
 Proof. ProveContext. Qed.


 Definition Proj1s : Symbols Deterministic (narg 1) :=
   symbols Deterministic (narg 1) [0].



Notation "'Proj1'" := (FuncInt Deterministic (narg 1) Proj1s).

Notation "'π1' c1" := (Proj1 [c1]) (at level 180, left associativity). (*π is U+03c0 *)


Axiom proj1pair :
      forall {x1 x2 : ppt} ,
        Proj1 [Pair [x1 ; x2]] = x1.


 Definition Proj2s : Symbols Deterministic (narg 1) :=
   symbols Deterministic (narg 1) [1].


Notation "'Proj2'" := (FuncInt Deterministic (narg 1) Proj2s).

Notation "'π2' c1" := (Proj2 [c1]) (at level 180, left associativity).




Axiom  proj2pair :
      forall {x1 x2 : ppt} ,
        Proj2 [Pair [x1 ; x2]] = x2.


Lemma bla15 : ContextTerm General Term (fun x => Proj2 [(adv 5) [(Pair [EQ [(nonce 2) ; x] ; x])]]).
Proof. ProveContext. Qed.
