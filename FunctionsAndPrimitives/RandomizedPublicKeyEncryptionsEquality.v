
(************************************************************************)
(* Copyright (c) 2021, Gergei Bana and Qianli Zhang                     *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Ensembles.
Require Export Setoid.
Require Import Coq.micromega.Lia.
Require Import CpdtTactics.
Import ListNotations.

Require Export AxiomImplicationsEquality.
Require Export NonceImplicationsEquality.



(* Encryption scheme using key generation and random seed from 1 nonce only *)

(*Key generation*)
Parameter Pkeys : Symbols Deterministic (narg 1) .
Notation "'Pkey'" := (FuncInt Deterministic (narg 1) Pkeys).


Parameter Skeys x: Symbols Deterministic (narg 1).
Notation "'Skey'" := (FuncInt Deterministic (narg 1) Skeys).


(*Random input*)
Parameter Rands : Symbols Deterministic (narg 1).
Notation "'Rand'" := (FuncInt Deterministic (narg 1) Rands).



(*Encryption*)
Parameter Encs : Symbols Deterministic (narg 3).
Notation "'Enc'" := (FuncInt Deterministic (narg 3) Encs).
Notation "'❴' c1 '❵_' c2 '＾' c3 " := (Enc [c1; c2; c3]) (at level 100, left associativity).  (*'❴' is U+23A1 U+23A4, '＾' is U+02C6*)

(*Decryption*)
Parameter Decs : Symbols Deterministic (narg 2).
Notation "'Dec'" := (FuncInt Deterministic (narg 2) Decs).


(*Decrypting the encryption gives plaintext*)
Axiom (*metaaxiom*) decenc :
      forall {m : ppt} {n n'},
        Dec [❴ m ❵_(Pkey [n]) ＾ (Rand [n']);  (Skey [n])] = m.



(*Test*)
Goal forall n n' n'' n''',
      n <> n'
      -> n <> n''
       -> n <> n'''
      -> FreshTerm (nonce n)  (Dec [Enc [nonce n'; Pkey [nonce n''] ; Rand [nonce n''']]; nonce n']).
  intros. ProveFresh. Qed.






(*******************************************************************************)
(*******************************************************************************)
(**************************** IND-CCA2 SECURITY ********************************)
(*******************************************************************************)
(*******************************************************************************)


Section CCA2Game.
Variable (L enc dec pkey skey rand : list ppt -> ppt)  (error : ppt) (nl nl': list ppt).
(* Nonce lists used by the Challenger for key generation and encrption.  *)


(* Challenger - Attacker interacion before the encryption by Challenger
I suspect that we cannot avoid mutual induction *)

Inductive CCA2BeforeEncTerm : ppt -> Prop :=
| CCA2Pkey :                  (* Public key is put out by Challenger *)
    CCA2BeforeEncTerm    (pkey nl)
| CCA2AttNonceTerm :    (* Attacker can generate nonces other than "n" "n'" *)
    forall n,
      Fresh (nonce n) nl
      -> Fresh (nonce n)  nl'
      -> CCA2BeforeEncTerm  (nonce n)
| CCA2AttContListTerm :        (* Attacker can apply any of his contexts *)
    forall tl f ,
      PPT  Adversarial f
      -> CCA2BeforeEnc tl
      -> CCA2BeforeEncTerm (f tl)
| CCA2DecOrac1 :             (* Challenger decrypts *)
    forall u ,
      CCA2BeforeEncTerm u
      -> CCA2BeforeEncTerm  (dec [ u  ; skey nl] )
with CCA2BeforeEnc : list ppt -> Prop :=
| CCA2BeforeTermList :        (* Lists of CCA2 terms are CCA2 lists *)
    forall u ul ,
      CCA2BeforeEncTerm u
      -> CCA2BeforeEnc ul
      -> CCA2BeforeEnc (u :: ul)
| CCA2BeforeTermNil :  CCA2BeforeEnc []. (* [] should be in the Before context*)

(* Challenger - Attacker interacion after the encryption by Challenger
Encryption of the Challenger is read into the variable "x" *)

(*
forall f g, cca2after f -> cca2after g -> cca2after (fun x => g ( f x))
forall f g, context f -> context g -> context (fun x => g ( f x)) *)

Inductive CCA2AfterEncTerm : (ppt -> ppt) -> Prop :=
| CCA2AttContId :
       CCA2AfterEncTerm (fun x => x)
| CCA2PkeyCont :                  (* Public key is put out by Challenger *)
    CCA2AfterEncTerm    (fun _ => (pkey nl))
| CCA2AttNonceTermCont :    (* Attacker can generate nonces other than "n" "n'" *)
    forall n,
      Fresh (nonce n) nl
      -> Fresh (nonce n)  nl'
      -> CCA2AfterEncTerm  (fun _ => (nonce n))
| CCA2DecOrac2 :             (* Challenger decrypts as long as it is not the output of the encryption challenge, which goes in "x" *)
    forall tl ,
      CCA2AfterEncTerm tl
      -> CCA2AfterEncTerm  (fun x => (If EQ [tl x ; x ] Then error Else (dec [ tl x ; skey nl] ) ) )
| CCA2AttBeforeToAfter : forall u , CCA2BeforeEncTerm u -> CCA2AfterEncTerm (fun x => u)  (* Attacker after gets the information from before *)
| CCA2AttContListTermFun :        (* Attacker can apply any of his functions *)
    forall  f tl,
      (PPT Adversarial f)
      -> CCA2AfterEnc tl
      -> CCA2AfterEncTerm (fun x => (f (tl x)))
with CCA2AfterEnc : (ppt -> list ppt) -> Prop :=
| CCA2AfterTermList :        (* Lists of CCA2 terms are CCA2 lists *)
    forall t tl ,
      CCA2AfterEncTerm t
      -> CCA2AfterEnc tl
      -> CCA2AfterEnc (fun x => (t x):: (tl x))
| CCA2AfterTermNil :  CCA2AfterEnc (fun _ => []). (* [] should be in the After context*)

End CCA2Game.


Definition CCA2 (L enc dec pkey skey rand : list ppt -> ppt) (error : ppt)  (kl rl : nat) : Prop :=
  forall  (nl nl': list ppt) (t:  ppt -> list ppt) (u u' : ppt),
     length nl = kl -> length nl' = kl ->  NonceList nl -> NonceList nl' -> ListFresh nl nl'
    -> CCA2BeforeEncTerm dec pkey skey nl nl'  u
    -> CCA2BeforeEncTerm dec pkey skey nl nl'  u'
    -> CCA2AfterEnc dec pkey skey error  nl nl' t
    ->
       t (If EQ [L [u] ; L [u']] Then  enc [u  ; pkey nl; rand nl'] Else error)
     ~
       t (If EQ [L [u] ; L [u']] Then  enc [u' ; pkey nl; rand nl'] Else error).


Definition CCA2L (L enc dec pkey skey rand : list ppt -> ppt) (error : ppt)  (kl rl : nat) : Prop :=
  forall  (nl nl': list ppt) (t:  ppt -> list ppt) (u u' : ppt),
     length nl = kl -> length nl' = kl ->  NonceList nl -> NonceList nl' -> ListFresh nl nl'
    -> CCA2BeforeEncTerm dec pkey skey nl nl'  u
    -> CCA2BeforeEncTerm dec pkey skey nl nl'  u'
    -> CCA2AfterEnc dec pkey skey error nl nl' t
    -> L [u] = L [u']
    -> t ( enc [u  ; pkey nl; rand nl'] )
         ~
         t ( enc [u' ; pkey nl; rand nl'] ).


Proposition CCA2toCCA2L :
  forall (L enc dec pkey skey rand : list ppt -> ppt) (error : ppt)  (kl rl : nat),
    CCA2 L enc dec pkey skey rand error  kl rl  -> CCA2L  L enc dec pkey skey rand error  kl rl.
Proof.
  unfold CCA2.
unfold CCA2L.
intros.
apply (H nl nl' t u u') in  H0.
apply ceq_eq in H8.
rewrite H8 in H0.
repeat rewrite If_true in H0.
all : assumption.
Qed.

Section CCA2GameM.
Variable (L enc dec pkey skey rand : list ppt -> ppt) (error : ppt)  (nl nl': list ppt).

Inductive CCA2AfterEncTermM : (ppt -> ppt) -> Prop :=
| CCA2AttContIdM :
       CCA2AfterEncTermM (fun x => x)
| CCA2PkeyContM :                  (* Public key is put out by Challenger *)
    CCA2AfterEncTermM    (fun _ => (pkey nl))
| CCA2AttNonceTermContM :    (* Attacker can generate nonces other than "n" "n'" *)
    forall n,
      Fresh (nonce n) nl
      -> Fresh (nonce n)  nl'
      -> CCA2AfterEncTermM  (fun _ => (nonce n))
| CCA2DecOrac2M :
   forall t t',
      CCA2AfterEncTermM  t
    -> CCA2AfterEncTermM  t'
    -> CCA2AfterEncTermM   (fun x => (If EQ [t x ;x] Then (t' x) Else (dec [ t x ; skey nl] ) ) )
| CCA2AttBeforeToAfterM : forall u , CCA2BeforeEncTerm  dec pkey skey nl nl' u -> CCA2AfterEncTermM (fun x => u)  (* Attacker after gets the information from before *)
| CCA2AttContListTermFunM :        (* Attacker can apply any of his functions *)
    forall  f tl,
      (PPT Adversarial f)
      -> CCA2AfterEncM tl
      -> CCA2AfterEncTermM (fun x => (f (tl x)))
with CCA2AfterEncM : (ppt -> list ppt) -> Prop :=
| CCA2AfterTermListM :        (* Lists of CCA2 terms are CCA2 lists *)
    forall t tl ,
      CCA2AfterEncTermM t
      -> CCA2AfterEncM tl
      -> CCA2AfterEncM (fun x => (t x):: (tl x))
| CCA2AfterTermNilM :  CCA2AfterEncM (fun _ => []). (* [] should be in the After context*)

End CCA2GameM.




Definition CCA2M (L enc dec pkey skey rand : list ppt -> ppt) (error : ppt)  (kl rl : nat) : Prop :=
  forall  (nl nl': list ppt) (t:  ppt -> list ppt) (u u' : ppt),
     length nl = kl -> length nl' = kl ->  NonceList nl -> NonceList nl' -> ListFresh nl nl'
    -> CCA2BeforeEncTerm dec pkey skey nl nl'  u
    -> CCA2BeforeEncTerm dec pkey skey nl nl'  u'
    -> CCA2AfterEncM dec pkey skey  nl nl' t
    ->
       t (If EQ [L [u] ; L [u']] Then  enc [u  ; pkey nl; rand nl'] Else error)
     ~
     t (If EQ [L [u] ; L [u']] Then  enc [u' ; pkey nl; rand nl'] Else error).



Definition CCA2ML (L enc dec pkey skey rand : list ppt -> ppt) (error : ppt)  (kl rl : nat) : Prop :=
  forall  (nl nl': list ppt) (t:  ppt -> list ppt) (u u' : ppt),
     length nl = kl -> length nl' = kl ->  NonceList nl -> NonceList nl' -> ListFresh nl nl'
    -> CCA2BeforeEncTerm dec pkey skey nl nl'  u
    -> CCA2BeforeEncTerm dec pkey skey nl nl'  u'
    -> CCA2AfterEncM dec pkey skey  nl nl' t
    -> L [u] = L [u']
    -> t ( enc [u  ; pkey nl; rand nl'] )
         ~
         t ( enc [u' ; pkey nl; rand nl'] ).

Proposition CCA2MtoCCA2ML :
  forall (L enc dec pkey skey rand : list ppt -> ppt) (error : ppt)  (kl rl : nat),
    CCA2M L enc dec pkey skey rand error  kl rl  -> CCA2ML  L enc dec pkey skey rand error  kl rl.
Proof.
  unfold CCA2M.
unfold CCA2ML.
intros.
apply (H nl nl' t u u') in  H0.
apply ceq_eq in H8.
rewrite H8 in H0.
repeat rewrite If_true in H0.
all : assumption.
Qed.

Definition CCA2Mnolength (enc dec pkey skey rand : list ppt -> ppt) (error : ppt)  (kl rl : nat) : Prop :=
  forall  (nl nl': list ppt) (t:  ppt -> list ppt) (u u' : ppt),
     length nl = kl -> length nl' = kl ->  NonceList nl -> NonceList nl' -> ListFresh nl nl'
    -> CCA2BeforeEncTerm dec pkey skey nl nl'  u
    -> CCA2BeforeEncTerm dec pkey skey nl nl'  u'
    -> CCA2AfterEncM dec pkey skey  nl nl' t
    ->
       t ( enc [u  ; pkey nl; rand nl'] )
     ~
       t ( enc [u' ; pkey nl; rand nl'] ).



Proposition CCA2CCA2M :
  forall  (L enc dec pkey skey rand : list ppt -> ppt) (kl rl : nat) (error : ppt)  ,
    CCA2  L enc dec pkey skey rand error kl rl  <->  CCA2M  L enc dec pkey skey rand error kl rl.
(* need mutual induction on CCA2AfterEnc *)
Admitted.



Ltac ProveCca2Before :=
  repeat (intros; auto;
          match goal with
          | [ |- CCA2BeforeEncTerm  _ ?Pk _ ?X _ (?Pk ?X) ]
            => apply CCA2Pkey
          | [ |- CCA2BeforeEncTerm _ _ _  _ _ (nonce _) ]
            => apply CCA2AttNonceTerm; ProveFresh; try lia
          | [ |- CCA2BeforeEncTerm ?D _ ?Sk  ?X _ (?D [ _ ; ?Sk ?X]) ]
            => apply CCA2DecOrac1
          | [ |- CCA2BeforeEncTerm  _ _ _ _ _ (?X) ]
            => first [ apply CCA2AttContListTerm; ProvePPT | apply CCA2AttContListTerm with (f := fun _ => X) (tl := []); ProvePPT ]
          | [ |- CCA2BeforeEnc  _ _ _  _ _ (cons _ _) ]
            => try apply CCA2BeforeTermList
          | [ |- CCA2BeforeEnc _ _ _  _ _ nil ]
            => try apply CCA2BeforeTermNil
  end).

Print CCA2AfterEncTerm.
Ltac ProveCca2After :=
  repeat  (intros; simpl; auto; (*add simpl here*)
           match goal with
           | [ |- CCA2AfterEncTerm _ _ _ _  _ _ (fun x => x) ]
             => apply CCA2AttContId
           | [ |- CCA2AfterEnc  _ _ _ _  _ _ (fun _ => cons _ _) ]
             => try apply CCA2AfterTermList
           | [ |- CCA2AfterEncTerm  _ ?Pk _ _ ?X _ (fun _ => ?Pk ?X)  ]
             => apply CCA2PkeyCont
           | [ |- CCA2AfterEncTerm _ _ _ _ _ _ (fun _ => nonce ?N ) ]
             => apply CCA2AttNonceTermCont; ProveFresh; try lia
           | [ |- CCA2AfterEncTerm ?D _ ?Sk ?Er  ?Nl _ (fun x
                                                        => If _ Then ?Er Else ?D [ _ ; ?Sk ?Nl]) ] => apply CCA2DecOrac2; ProvePPT
           | [ |- CCA2AfterEncTermM _ _ _ _ _ (fun _ => ConstInt ?f ?g ) ]
             => apply (CCA2AttContListTermFunM  _ _ _ _ _ _ (fun x =>  ConstInt ?f ?g) (fun x => [x])); ProvePPT
           | [ |- CCA2AfterEncTerm _ _ _ _ _ _ (fun _ => (?f _) ) ]
             => first [apply CCA2AttContListTermFun; ProvePPT | apply CCA2AttBeforeToAfter; try ProveCca2Before]
           | [ |- CCA2AfterEnc _ _ _ _ _ _ (fun _ => nil) ]
             => try apply CCA2AfterTermNil
  end).

Ltac ProveCCA2 :=
  repeat  (intros; auto;
           match goal with
           | [ |- NonceList _ ]
             => ProveNonceList
           | [ |- ListFresh _ _ ]
             => ProveListFresh; constructor
           | [ |- CCA2BeforeEncTerm _ _ _ _ _ _ ]
             => ProveCca2Before
           | [ |- CCA2BeforeEnc _ _ _ _ _ _ ]
             => ProveCca2Before
           | [ |- CCA2AfterEnc _ _ _ _ _ _ _]
             => ProveCca2After
           | [ |- CCA2AfterEncTerm _ _ _ _ _ _ _]
             => ProveCca2After
           end).




Ltac ProveCca2AfterM :=
  repeat  (intros; auto;
           match goal with
           | [ |- CCA2AfterEncTermM _ _ _   _ _ (fun x => x) ]
             => apply CCA2AttContIdM
           | [ |- CCA2AfterEncM  _ _ _   _ _ (fun _ => cons _ _) ]
             => try apply CCA2AfterTermListM
           | [ |- CCA2AfterEncTermM  _ ?Pk _  ?X _ (fun _ => ?Pk ?X)  ]
             => apply CCA2PkeyContM
           | [ |- CCA2AfterEncTermM _ _ _  _ _ (fun _ => nonce ?N ) ]
             => apply CCA2AttNonceTermContM; ProveFresh; try lia
           | [ |- CCA2AfterEncTermM ?D _ ?Sk  ?Nl _ (fun x
                                                     => If _ Then _ Else ?D [ _ ; ?Sk ?Nl]) ] => apply CCA2DecOrac2M; ProvePPT
           | [ |- CCA2AfterEncTermM _ _ _ _ _ (fun _ => ConstInt ?f ?g ) ]
             => apply (CCA2AttContListTermFunM  _ _ _ _ _ (fun x =>  ConstInt ?f ?g) (fun x => [x])); ProvePPT
           | [ |- CCA2AfterEncTermM _ _ _  _ _ (fun _ => _ ) ]
             => first [apply CCA2AttContListTermFunM; ProvePPT | apply CCA2AttBeforeToAfterM; try ProveCca2Before]
           | [ |- CCA2AfterEncM _ _ _  _ _ (fun _ => nil) ]
             => try apply CCA2AfterTermNilM
           end).

Ltac ProveCCA2M :=
  repeat  (intros; auto;
           match goal with
           | [ |- NonceList _ ]
             => ProveNonceList
           | [ |- ListFresh _ _ ]
             => ProveListFresh; constructor
           | [ |- CCA2BeforeEncTerm _ _ _ _ _ _ ]
             => ProveCca2Before
           | [ |- CCA2BeforeEnc _ _ _ _ _ _ ]
             => ProveCca2Before
           | [ |- CCA2AfterEncM _ _ _   _ _ _]
             => ProveCca2AfterM
           | [ |- CCA2AfterEncTermM _ _ _   _ _ _]
             => ProveCca2AfterM
           end).





(*
Lemma CCA2DecOrac2Var :
   forall t t'  nl nl'  dec pkey skey er,
      CCA2AfterEncTerm  nl nl' dec pkey skey er t
    -> CCA2AfterEncTerm nl nl'  dec pkey skey er  t'
    -> CCA2AfterEncTerm  nl nl' dec pkey skey er  (fun x => (If t x ≟ x Then (t' x) Else (dec [ t x ; skey nl] ) ) ).
Proof.
  intros.
  assert ((fun x => If t x ≟ x Then t' x Else (dec [ t x ; skey nl]))
        = (fun x => If t x ≟ x Then t' x Else (If t x ≟ x Then er Else (dec [ t x ; skey nl])))).
  apply functional_extensionality.
  intros. simpl.  admit.
        rewrite H1.  apply CCA2AttContListTermFun; ProvePPT. apply CCA2AfterTermList. ProveCca2After; auto.  apply CCA2AfterTermList.  auto.  apply CCA2AfterTermList.  apply CCA2DecOrac2; auto. apply CCA2AfterTermNil.
 Admitted.




(*IND-CCA2 property length omitted for now*)
Definition CCA2  (L enc dec pkey skey rand: list ppt -> ppt) (er : ppt) : Prop :=
  forall (nl nl' : list ppt) (t:  ppt -> list ppt) (u u' : ppt),
    NonceList nl -> NonceList nl' -> ListFresh nl nl'
    -> CCA2BeforeEncTerm nl nl' dec pkey skey u
    -> CCA2BeforeEncTerm nl nl' dec pkey skey u'
    -> CCA2AfterEnc nl nl'  dec pkey skey er t
    ->
       t (If EQ [L [u] ; L [u']] Then  enc [u  ; pkey nl; rand nl'] Else er)
     ~
       t (If EQ [L [u] ; L [u']] Then  enc [u' ; pkey nl; rand nl'] Else er).

*)


(*******************************************************************************)
(*******************************************************************************)
(**************************** IND-CPA SECURITY ********************************)
(*******************************************************************************)
(*******************************************************************************)






Section CPAGame.
Variable  (L enc dec pkey skey rand : list ppt -> ppt) (error : ppt) (nl nl': list ppt).
(* Nonce lists "nl" and "nl'"used by the Challenger for key generation and encrption. *)




(* Challenger - Attacker interacion before the encryption by Challenger
I suspect that we cannot avoid mutual induction *)

Inductive CPABeforeEncTerm : ppt -> Prop :=
| CPAPkey :                  (* Public key is put out by Challenger *)
    CPABeforeEncTerm    (pkey nl)
| CPAAttNonceTerm :    (* Attacker can generate nonces other than "n" "n'" *)
    forall n,
      Fresh (nonce n) nl
      -> Fresh (nonce n) nl'
      -> CPABeforeEncTerm  (nonce n)
| CPAAttContListTerm :        (* Attacker can apply any of his contexts *)
    forall tl f ,
      PPT Adversarial f
      -> CPABeforeEnc tl
      -> CPABeforeEncTerm (f tl)
with CPABeforeEnc : list ppt -> Prop :=
| CPABeforeTermList :        (* Lists of CPA terms are CPA lists *)
    forall t tl ,
      CPABeforeEncTerm t
      -> CPABeforeEnc tl
      -> CPABeforeEnc (t :: tl).

Scheme CPABeforeEnc_mut := Minimality for CPABeforeEnc Sort Prop
  with CPABeforeEncTerm_mut := Minimality for CPABeforeEncTerm Sort Prop.


(* Challenger - Attacker interacion after the encryption by Challenger
Encryption of the Challenger is read into the variable "x" *)

Inductive CPAAfterEncTerm : (ppt -> ppt) -> Prop :=
| CPAAttContId :
       CPAAfterEncTerm (fun x => x)
| CPAPkeyCont :                  (* Public key is put out by Challenger *)
    CPAAfterEncTerm    (fun x => (pkey nl))
| CPAAttNonceTermCont :    (* Attacker can generate nonces other than "n" "n'" *)
    forall n,
      Fresh (nonce n) nl
      -> Fresh (nonce n) nl'
      -> CPAAfterEncTerm  (fun x => (nonce n))
| CPAAttContListTermFun :        (* Attacker can apply any of his contexts *)
    forall tl f ,
      PPT Adversarial f
      -> CPAAfterEnc tl
      -> CPAAfterEncTerm (fun x => (f (tl x)))
with CPAAfterEnc : (ppt -> list ppt) -> Prop :=
| CPAAfterTermList :        (* Lists of CPA terms are CPA lists *)
    forall t tl ,
      CPAAfterEncTerm t
      -> CPAAfterEnc tl
      -> CPAAfterEnc (fun x => (t x):: (tl x)).
 (* Here we do not need after Attacker gets the information from before because there is nothing
before Attacker can do that the after attacker cannot. *)


Scheme CPAAfterEnc_mut := Minimality for CPAAfterEnc Sort Prop
  with CPAAfterEncTerm_mut := Minimality for CPAAfterEncTerm Sort Prop.



End CPAGame.

 (*IND-CCA2 property length omitted for now*)
Definition CPANoLength  (enc pkey rand: list ppt -> ppt) (kl rl : nat) : Prop :=
  forall (nl nl' : list ppt) (t:  ppt -> list ppt) (u u' : ppt),
    length nl = kl -> length nl' = kl -> NonceList nl -> NonceList nl' -> ListFresh nl nl'
    -> CPABeforeEncTerm  pkey nl nl' u
    -> CPABeforeEncTerm  pkey nl nl' u'
    -> CPAAfterEnc   pkey nl nl' t
    ->
    t ( enc [u  ; pkey nl; rand nl'])
   ~
    t ( enc [u' ; pkey nl; rand nl']).


(*IND-CCA2 property length omitted for now*)
Definition CPA  (L enc pkey rand: list ppt -> ppt) (er : ppt) (kl rl : nat): Prop :=
  forall (nl nl' : list ppt) (t:  ppt -> list ppt) (u u' : ppt),
    length nl = kl -> length nl' = kl -> NonceList nl -> NonceList nl' -> ListFresh nl nl'
    -> CPABeforeEncTerm  pkey nl nl' u
    -> CPABeforeEncTerm  pkey nl nl' u'
    -> CPAAfterEnc   pkey nl nl' t
    ->
    t (If EQ [L [u] ; L [u']] Then  enc [u  ; pkey nl; rand nl'] Else er)
   ~
    t (If EQ [L [u] ; L [u']] Then  enc [u' ; pkey nl; rand nl'] Else er).




(*******************************************************************************)
(**************************** IND-CPA  Lemmas   ********************************)
(*******************************************************************************)






(* *************************************************************************** *)
(* The following are the Challenger role, but I ended up not using these *)
(*
Inductive LR : Type :=
| Left
| Right.
Definition LRproj (lr :  LR ) : list ppt -> ppt :=
match lr with
| Left => Nth 0
|Right => Nth 1
end.
Inductive CCA2DecOracContextTerm1 : (ppt -> ppt) -> Prop :=
| CCA2ChDec1 :
    CCA2DecOracContextTerm1 (fun x => (dec [x ; skey nl] )) .
Inductive CCA2DecOracContextTerm2 (y: ppt) :  (ppt -> ppt) -> Prop :=
| CCA2ChDec2 :
    CCA2DecOracContextTerm2 y (fun x => (If EQ [x;y] Then default Else dec [x ; skey nl] )) .
Inductive CCA2LeftRightEncTerm : (LR -> list ppt -> ppt) -> Prop :=
| CCA2LREnc  :
    CCA2LeftRightEncTerm (fun lr => (fun x => (If EQ [L [Nth 1 x] ; L [Nth 2 x] ] Then enc [LRproj lr x ; pkey nl ; rand nl' ] Else error ) ) ).
*)
(* *************************************************************************** *)
