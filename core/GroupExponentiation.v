
(************************************************************************)
(* Copyright (c) 2020, Gergei Bana                                      *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Setoid.
Require Import Coq.micromega.Lia.
Import ListNotations.

Require Export NonceImplications.



(* Domains should be treated properly later*)

(**************************************************************************************)
(**************************************************************************************)
(********************************* Commutative Groups *********************************)
(**************************************************************************************)
(**************************************************************************************)



(* Group multiplication *)

Parameter gms : Symbols.

Notation "'gm'" := (FunctionSymbol Deterministic gms).


Axiom (*metaaxiom*) gm2arg :
      forall {x1 x2 : ppt} {lx : list ppt} ,
        (gm nil) = default
        /\ gm [x1] = default
        /\ (gm ([x1; x2]++lx)) = (gm [x1; x2]).


Notation "g '**' r" := (gm [ g ; r ])
                         (at level 39, left associativity).


(* Commutativity *)

Axiom  gmcomm :
      forall {g g' : ppt} ,
        g ** g' # g' ** g.


(* Associativity *)
Axiom  gmass :
      forall {g g' g'': ppt} ,
        g ** (g' ** g'') # (g ** g') ** g''.




(* Identity element *)
Parameter gids : Symbols.

Notation "'gid'" := (ConstantSymbol Deterministic gids).


(* Multiplication with identity *)
Axiom  gmidentr :
      forall {g : ppt} ,
        g ** gid # g.

Axiom  gmidentl :
      forall {g : ppt} ,
        gid ** g # g.


(* Inverse *)
Parameter ginvs : Symbols.

Notation "'ginv'" := (FunctionSymbol Deterministic ginvs).



(* Multiplication with inverse *)
 Axiom  gminvr :
      forall {g : ppt} ,
        g ** (ginv [g]) # gid.

Axiom  gminvl :
      forall {g : ppt} ,
       (ginv [g]) ** g # gid.


(**************************************************************************************)
(**************************************************************************************)
(********************************* Commutative Rings **********************************)
(**************************************************************************************)
(**************************************************************************************)




(* Ring multiplication *)

Parameter rms : Symbols.

Notation "'rm'" := (FunctionSymbol Deterministic rms).


Axiom (*metaaxiom*) rm2arg :
      forall {x1 x2 : ppt} {lx : list ppt} ,
        (rm nil) = default
        /\ rm [x1] = default
        /\ (rm ([x1; x2]++lx)) = (rm [x1; x2]).


Notation "a '^*^' b" := (rm [ a ; b ])
                         (at level 36, left associativity).


(* Commutativity *)

Axiom  rmcomm :
      forall {a b : ppt} ,
        a ^*^ b # b ^*^ a.


(* Associativity *)
Axiom  rmass :
      forall {a b c: ppt} ,
        a ^*^ (b ^*^ c) # (a ^*^ b) ^*^ c.


(* Identity element *)
Parameter rids : Symbols.

Notation "'rid'" := (ConstantSymbol Deterministic rids).


(* Multiplication with identity *)
Axiom  rmidentr :
      forall {a : ppt} ,
        a ^*^ rid # a .

Axiom  rmidentl :
      forall {a : ppt} ,
        rid ^*^ a # a.



(* Ring addition *)

Parameter ras : Symbols.

Notation "'ra'" := (FunctionSymbol Deterministic ras).


Axiom (*metaaxiom*) ra2arg :
      forall {x1 x2 : ppt} {lx : list ppt} ,
        (ra nil) = default
        /\ ra [x1] = default
        /\ (ra ([x1; x2]++lx)) = (ra [x1; x2]).


Notation "a '^+^' b" := (ra [ a ; b ])
                         (at level 37, left associativity).


(* Commutativity *)

Axiom  racomm :
      forall {a b : ppt} ,
        a ^+^ b # b ^+^ a.


(* Associativity *)
Axiom  raass :
      forall {a b c: ppt} ,
        a ^+^ (b ^+^ c) # (a ^+^ b) ^+^ c.



(* Zero element *)
Parameter rzeros : Symbols.

Notation "'rzero'" := (ConstantSymbol Deterministic rzeros).


(* Addition with zero *)
Axiom  razeror :
      forall {a : ppt} ,
        a ^+^ rzero # a .

Axiom  razerol :
      forall {a : ppt} ,
        rzero ^*^ a # a.

(* Inverse *)
Parameter rnegs : Symbols.

Notation "'rneg'" := (FunctionSymbol Deterministic rnegs).



(* Multiplication with inverse *)
 Axiom  rainvl :
      forall {a : ppt} ,
        (rneg [a]) ** a # rzero.


Axiom  rainvr :
      forall {a : ppt} ,
        a ^+^ (rneg [a]) # rzero.



(* Distributivity *)
 Axiom  rdists :
      forall {a b c : ppt} ,
        a ^*^ (b ^+^ c) # (a ^*^ b) ^+^ (a ^*^ c).





(**************************************************************************************)
(**************************************************************************************)
(********************************** Exponentiation ************************************)
(**************************************************************************************)
(**************************************************************************************)


Parameter exps : Symbols.

Notation "'exp'" := (FunctionSymbol Deterministic exps).


Axiom (*metaaxiom*) exp2arg :
      forall {x1 x2 : ppt} {lx : list ppt} ,
        (exp nil) = default
        /\ exp [x1] = default
        /\ (exp ([x1; x2]++lx)) = (exp [x1; x2]).


Notation "g '^^' r" := (exp [ g ; r ])
                         (at level 38, left associativity).



Axiom  expa :
      forall {g a b : ppt} ,
        (g ^^ a)  **  (g ^^ b )   #    g ^^ (b ^+^ a).


Axiom  expm :
      forall {g a b : ppt} ,
        (g ^^ a) ^^ b    #    g ^^ (b ^+^ a).



Axiom  expi :
      forall {g a : ppt} ,
        ginv [g ^^ a]    #    g ^^ (rneg [a]).




Lemma bla35: forall x y  u w , x#u -> y#w -> [exp [x;y]; x] ## [exp [u;w]; x].
Proof. intros.  rewrite H. rewrite H0.  apply ceql_ref. Qed.

Proposition   expcomm :
      forall {g a b : ppt} ,
        g ^^ a ^^ b  # g ^^ b ^^ a.
Proof. intros. rewrite (@expm g a b). rewrite  (@expm g b a).
rewrite racomm. reflexivity. Qed.

Proposition   expid :
      forall {g a b c: ppt} ,
        g ^^ rzero  # gid.
Proof. intros. rewrite <- (@rainvr rzero).  rewrite <- expa.
 rewrite <- (@expi g rzero). rewrite  gmcomm.  apply gminvr.
 Qed.



(*Axiom  expcomm :
      forall {g a b : ppt} ,
        (g ^^ a ^^ b) = (g ^^ b ^^ a).*)





(**************************************************************************************)
(**************************************************************************************)
(**************************** Randomized Exponentiation *******************************)
(**************************************************************************************)
(**************************************************************************************)



Parameter ggens : Symbols.


Notation "'ggen'" := (FunctionSymbol Deterministic ggens).

Definition g (n : nat) : ppt := ggen [nonce n].


Axiom (*metaaxiom*) generator1arg :
      forall {x : ppt} {lx : list ppt} ,
        (ggen nil) = default
        /\ (ggen ([x]++lx)) = (ggen [x]).





Axiom ggenUniform :
      forall {x : ppt} {n : nat} ,
        FreshTerm (nonce n) x
        ->  [ggen [nonce n]] ~ [x ** (ggen [nonce n])].







Parameter Rings : Symbols.


Notation "'Ring'" := (FunctionSymbol Deterministic Rings).

Definition R (n : nat) : ppt := Ring [nonce n].

Axiom (*metaaxiom*) ring1arg :
      forall {x : ppt} {lx : list ppt} ,
        (Ring nil) = default
        /\ (Ring ([x]++lx)) = (Ring [x]).





Axiom RingUniform :
      forall {x : ppt} {n : nat} ,
        FreshTerm (nonce n) x
        ->  [Ring [nonce n]] ~ [x ^+^ (Ring [nonce n])].


Axiom ggenRingUniform :
      forall {x : ppt} {n n': nat} nl , NonceList nl ->
        FreshTerm (nonce n) x -> Fresh (nonce n)  ((nonce n') :: nl )
        ->  nonce n':: ggen [nonce n']^^ Ring [nonce n] :: nl  ~ nonce n':: x ** (ggen [nonce n'])^^ Ring [nonce n] :: nl.





Goal forall n n' n'',
      n <> n'
      -> n <> n''
      -> FreshTerm (nonce n)  ((g n') ^^ (R n'')).
  intros. ProveFresh. Qed.


Definition DDH (gg rr: list ppt -> ppt) : Prop :=
forall (n n1 n2 n3 : nat) , n <> n1 -> n <> n2 -> n <> n3 -> n1 <> n2 -> n1 <> n3 -> n2 <> n3 ->
        [(gg [nonce n]) ; (gg [nonce n])^^(rr [nonce n1]) ; (gg [nonce n])^^(rr [nonce n2]) ; (gg [nonce n])^^(rr [nonce n1])^^(rr [nonce n2])]
       ~
        [(gg [nonce n]) ; (gg [nonce n])^^(rr [nonce n1]) ; (gg [nonce n])^^(rr [nonce n2]) ; (gg [nonce n])^^(rr [nonce n3])].


(*Axiom DDH :  forall {n n1 n2 n3 : nat} , n <> n1 -> n <> n2 -> n <> n3 -> n1 <> n2 -> n1 <> n3 -> n2 <> n3 ->
        [(g n) ; (g n)^^(R n1) ; (g n)^^(R n2) ; (g n)^^(R n1)^^(R n2)]
       ~
        [(g n) ; (g n)^^(R n1) ; (g n)^^(R n2) ; (g n)^^(R n3)].*)
