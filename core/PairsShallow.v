
(************************************************************************)
(* Copyright (c) 2020, Gergei Bana                                      *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Setoid.
Require Import Coq.micromega.Lia.
Import ListNotations.

Require Export FreshnessShallow.



Parameter Pairs : Symbols Deterministic (narg 2).

Notation "'Pair'" := (Function Deterministic (narg 2) pairs).

Notation "'＜' c1 ',' c2 '＞'" := (Pair [c1; c2]) (at level 100, left associativity). (* U+FF1C and U+FF1E)*)




Proposition ctpair :
       forall {hag tol tc1 tc2},
         ContextTerm hag tol tc1
         -> ContextTerm hag tol tc2
         -> ContextTerm hag tol (fun x => (pair ([ (tc1 x) ; (tc2 x)]) )).
 Proof. ProveContext. Qed.


Parameter Proj1s : Symbols Deterministic (narg 1).

Notation "'Proj1'" := (Function Deterministic (narg 1) Proj1s).

Notation "'π1' c1" := (Proj1 [c1]) (at level 180, left associativity). (*π is U+03c0 *)


Axiom (*metaaxiom*) proj1pair :
      forall {x1 x2 : ppt} ,
        (Proj1 [Pair [x1 ; x2]]) # x1.


Parameter Proj2s : Symbols Deterministic (narg 1).

Notation "'Proj2'" := (Function Deterministic (narg 1) Proj2s).

Notation "'π2' c1" := (Proj2 [c1]) (at level 180, left associativity).




Axiom  proj2pair :
      forall {x1 x2 : ppt} ,
        (Proj2 [Pair [x1 ; x2]]) # x2.


Lemma bla15 : ContextTerm General Term (fun x => Proj2 [(adv 5) [(Pair [EQ [(nonce 2) ; x] ; x])]]).
Proof. ProveContext. Qed.
