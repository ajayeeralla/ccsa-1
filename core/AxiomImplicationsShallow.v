
(************************************************************************)
(* Copyright (c) 2020, Gergei Bana, Qianli Zhang                        *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)


Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Setoid.
Require Import FunInd.
Import ListNotations.
Require Export FunctionalExtensionality.

Require Export AxiomsShallow.


Proposition ModusTollens :
      (* "meta-level quantification" *) forall P Q : Prop,
      (* "premise1:" *)     (P -> Q) ->
      (* "premise2:" *)     (not Q) ->
      (* "conclusion:" *)   (not P).
Proof. intros P Q. intros. unfold not. intros. apply H in H1.
contradiction.  Qed.




Proposition PBC (*Proof By Contradiction*) :
      (* "meta-level quantification" *) forall P : Prop,
      (* "premise:" *)     ((not P) -> False) ->
      (* "conclusion:" *)    P.
Proof. intros. apply doubleneg_elim. unfold not.
assumption. Qed.

Proposition LEM  (*Law of Excluded Middle*):
      (* "meta-level quantification" *) forall P : Prop,
      (* "conclusion:" *)    P \/ (not P).
Proof. intros P.  apply PBC.
intros. assert (P \/ (not P)). apply or_intror. unfold not.
intros. apply H.
apply or_introl. assumption.
apply H. assumption.  Qed.


(**************************************************************************************)
(**************************************************************************************)
(******************************** CORE  AXIOM IMPLICATIONS ****************************)
(**************************************************************************************)
(**************************************************************************************)







(**************************************************************************************)
(***************************** Properties of Indistinguishability ************************)
(**************************************************************************************)


Proposition FuncApp :
     (*"<<<"*) forall {f : list ppt -> list ppt},   (*">>>"*)
     forall {lx ly} ,
     (*"<<<"*) (Context Adversarial List f ) ->  (*">>>"*)
     lx ~ ly -> lx++f(lx) ~ ly++f(ly).
Proof.
  intros.
  assert (forall (f : list ppt -> list ppt) ,
             (Context Adversarial List f )
           -> (Context Adversarial List  (fun lx': list ppt => lx' ++ (f lx')))).
  intros. ProveContext.
   auto.

  assert (lx ++(f lx) ~ ly ++(f ly)).
  apply (@cind_funcapp (fun lx' => lx' ++ (f lx')) lx ly).
  apply H1. auto. auto. auto.
Defined.



(* If f is constant, and lx, ly are lists, then we can apply cind_funcapp for the constant function:*)
Proposition cind_funcapp0 :
    forall x0 :  ppt, PPT Adversarial (fun lx => x0) ->
    forall lx ly , lx ~ ly -> lx ++ [x0] ~ ly ++ [x0].
Proof.
  intros.
  assert  (Context Adversarial List (fun lx => [x0])).
  ProveContext.
  apply (FuncApp H1). auto. Defined.



Proposition cind_restr :  forall {l1 l2 t1 t2},
  ((t1 :: l1) ~ (t2 :: l2)) -> (l1 ~ l2).
Proof.
  intros. assert (TL (t1 :: l1) ~ TL (t2 :: l2)).
  apply (@cind_funcapp TL). constructor. constructor. auto.
  simpl in H0. auto. Defined.

Proposition cind_len_rev :
     forall {lt1 lt2 : list ppt}, lt1 ~ lt2 -> length lt1 = length lt2.
Proof. intros. apply doubleneg_elim. unfold not at 1. intros. apply cind_len in H0.
auto. Qed.


(**************************************************************************************)
(*********************************** Properties of Equality ******************************)
(**************************************************************************************)



Proposition  ceq_symm :
    forall {x y} , (x # y) -> (y # x).
Proof.
  intros.
  assert (Context General Term (fun (x': ppt)  => [ (EQ [x'; x ]) ])).
    ProveContext.
  apply (ceq_sub H0 H). apply ceq_ref.
Defined.

Proposition  ceq_trans :
    forall {x y z}, (x # y) -> (y # z) -> (x # z).
Proof.
  intros.
  assert (Context General Term  (fun x' => [ (EQ [x'; z]) ])).
    ProveContext.
  apply (ceq_sub H1 (ceq_symm H) H0).
Defined.


Proposition  ceq_transymm :
    forall {x y z}, (y # x) -> (y # z) -> (x # z).
Proof.
  intros.
  apply (@ceq_trans x y z).
  apply ceq_symm. all: auto.
Defined.

Proposition  ceq_subeq :
    (*"<<<"*) forall {lt1 :  ppt ->  ppt} {lt2 : ppt -> ppt}, (*">>>"*)
    forall {x y : ppt},
    (*"<<<"*)(ContextTerm General Term lt1) ->  (ContextTerm General Term lt2) -> (*">>>"*)
     x # y -> ((lt1 x) # (lt2 x)) -> ((lt1 y) # (lt2 y)).
Proof.
  intros.
  apply (@ceq_sub (fun x' => ([(EQ [(lt1 x') ; (lt2 x')])])) [TRue] x y ).
    ProveContext.
  all : auto. Defined.


Proposition  ceq_funcapp :
  (*"<<<"*) forall {lt :  ppt -> ppt} , (*">>>"*)
  forall {x y : ppt},
  (*"<<<"*)(ContextTerm General Term lt) -> (*">>>"*)
     x # y ->  ((lt x) # (lt y)).
Proof.
  intros.
  apply (@ceq_subeq (fun x' => (lt x)) lt x y).
  ProveContext.
  all: auto. apply ceq_ref. Defined.


Proposition  ceq_cind :
forall {x y : ppt},
     x # y ->  [x] ~ [y].
Proof.
  intros.
  apply (@ceq_sub (fun x' => [x']) [y] y x).
  ProveContext.
  apply ceq_symm. auto.
  apply cind_ref. Defined.









(**************************************************************************************)
(*************************************** Morphisms ************************************)
(**************************************************************************************)


Inductive ceql :list ppt -> list ppt ->  Prop :=
| ceql_nil : ceql nil nil
| ceql_cons : forall a a' lx lx', a # a' -> ceql lx lx' -> ceql (a :: lx) (a' :: lx').

Fixpoint ceql' (lx : list ppt) (lx' : list ppt) :  Prop :=
  match lx with
  | nil  => match lx' with
           | nil => True
           | a' :: m' => False
           end
  | a :: m => match lx' with
            | nil => False
            | a' :: m' => a # a' /\ ceql' m m'
            end
  end.

Goal forall  l2 l1, ceql' l1 l2 <-> ceql l1 l2.
  split.
  intros.  generalize dependent l2.
  induction l1; destruct l2; intros.
  + constructor.
  + simpl in H; inversion H.
  + simpl in H; inversion H.
  + constructor.
    - simpl in H; inversion_clear H; auto.
    - simpl in H; inversion_clear H.
      apply (IHl1 l2) in H1. auto.

  + intros.
    induction H.
    simpl. trivial.
    simpl. auto.
Defined.

Functional Scheme ceql'_ind := Induction for ceql' Sort Prop.

Goal forall l1 l2, ceql' l1 l2 -> ceql l1 l2.
  intros.
  functional induction  (ceql' l1 l2) using ceql'_ind.
  + constructor.
  + inversion H.
  + inversion H.
  + inversion_clear H. constructor; auto.
Qed.


Notation "lx ## ly" := (ceql lx ly) (at level 70, no associativity): type_scope.
Notation "lx ##' ly" := (ceql' lx ly) (at level 70, no associativity): type_scope.

Lemma ceql_ref :
  forall {x} , x ## x.
  induction x.
  constructor.
  constructor; auto.
  apply ceq_ref. Defined.


Lemma ceql_ref' :
  forall {x} , x ##' x.
  intros.
  induction x. simpl. trivial.
  simpl. trivial. apply conj. apply ceq_ref. trivial. Defined.


Proposition  ceql_sub:
    (*"<<<"*) forall {ltc :  list ppt -> list ppt} {lt : list ppt}, (*">>>"*)
                forall {lx ly : list ppt},
    (*"<<<"*)  (Context General List ltc) ->   (*">>>"*)
    lx ## ly -> (ltc lx ~ lt) -> ((ltc ly) ~ lt).
  assert  (forall ( lx ly : list ppt), lx ## ly -> forall (ltc : list ppt -> list ppt)
                                               (lt : list ppt),
                Context General List ltc -> ltc lx ~ lt -> ltc ly ~ lt).
  intros lx ly H.
  induction H; intros.
  auto.
  apply (@ceq_sub (fun a' => (ltc (a' :: lx'))) lt a a'). ProveContext. auto.
  apply  (IHceql) with (ltc := (fun p => ltc (a :: p))). ProveContext. auto.
  intros. apply (H lx ly H1 ltc lt H0 H2).
Qed.



Proposition  ceql_sub' :
    (*"<<<"*) forall {ltc :  list ppt -> list ppt} {lt : list ppt}, (*">>>"*)
                forall {lx ly : list ppt},
    (*"<<<"*)  (Context General List ltc) ->   (*">>>"*)
     lx ##' ly -> ((ltc lx) ~ lt) -> ((ltc ly) ~ lt). (* Maybe we should define equality on lists *)
Proof.
  assert  (forall ( lx ly : list ppt), forall (ltc : list ppt -> list ppt) (lt : list ppt),
              Context General List ltc -> lx ##' ly -> ltc lx ~ lt -> ltc ly ~ lt). (*Agda style proof...*)
  intros lx.
  induction lx; intros.
  destruct ly. trivial.  simpl in H0.  contradiction.
  induction ly. simpl in H0. contradiction.
  simpl in H0. destruct H0.
  apply (@ceq_sub (fun b => (ltc (b :: ly))) lt a a0).  ProveContext.
  trivial. Check @IHlx. Check @ceq_sub.
  apply (@IHlx ly (fun lb => (ltc (a :: lb))) lt ). ProveContext.
  trivial. trivial.
  intros. apply (H lx ly). trivial. trivial. trivial. Qed.




Proposition length_ceql : forall x y , x ## y -> length x = length y.
Proof.
  intros.
  induction H. auto.
  simpl. rewrite IHceql. auto. Qed.


Proposition length_ceql' : forall x y , x ##' y -> length x = length y.
Proof.
  intros x. induction x. intros. induction y. simpl. reflexivity. simpl in H.
  contradiction.
  intros. induction y. simpl in H. contradiction. simpl in H. destruct H. simpl.
  assert (length x = length y).
  apply IHx. apply H0. rewrite H1. reflexivity. Qed.



Proposition  ceql_symm :
    forall {x y} , (x ## y) -> (y ## x).
Proof.
  intros.
  induction H.
  constructor.
  constructor. apply (ceq_symm H).
  auto.
Qed.


Proposition  ceql_symm' :
    forall {x y} , (x ##' y) -> (y ##' x).
Proof.
  intros x. induction x. induction y. trivial.  simpl. intros. contradiction. intros.
induction x. induction y. simpl in H. contradiction. simpl in H. destruct H. induction y.
simpl. apply conj. apply ceq_symm. trivial. trivial. simpl in IHy. contradiction.
induction y. simpl in H. contradiction. simpl. apply conj. simpl in H. destruct H. apply ceq_symm.  apply H.
apply IHx. simpl in H. destruct H. apply H0. Qed.



Proposition  ceql_trans :
    forall {x y z}, (x ## y) -> (y ## z) -> (x ## z).
Proof.
  intros. generalize dependent z.
  induction H; intros.
  + auto.
  + destruct z.
    - inversion H1.
    - inversion_clear H1. constructor.
      apply (ceq_trans H H2).
      apply (IHceql z H3). Defined.

Proposition  ceql_trans' :
    forall {x y z}, (x ##' y) -> (y ##' z) -> (x ##' z).
Proof.
  intros x. induction x. intros y. induction y. intros. induction z. trivial.
simpl in H. contradiction. intros.  simpl in H.  contradiction.
intros y. induction y. intros z. induction z. intros. simpl in H.
contradiction. intros. simpl in H. contradiction. intros. induction z.
simpl in H0. contradiction. simpl. apply conj. simpl in H. destruct H.
simpl in H0. destruct H0. apply (ceq_trans H H0). simpl in H. destruct H.
simpl in H0; destruct H0. apply (@IHx  y z). trivial. trivial. Qed.



Add Parametric Relation : ppt ceq
  reflexivity proved by @ceq_ref
  symmetry proved by @ceq_symm
  transitivity proved by @ceq_trans
  as ceq_rel.

Goal forall x y z, x # y -> y # z -> z # x.
  intros. rewrite <- H in H0. rewrite H0. reflexivity. Qed.

Add Parametric Relation : (list ppt) ceql
  reflexivity proved by @ceql_ref
  symmetry proved by @ceql_symm
  transitivity proved by @ceql_trans
    as ceql_rel.

Goal forall x y z, x ## y -> y ## z -> z ## x.
  intros. rewrite <- H in H0. rewrite H0. reflexivity. Qed.






Add Parametric Morphism hag arg f : (@Function hag arg f) with
  signature  ceql ==> ceq as FS_mor.
Proof. intros. unfold ceq.
apply (@ceql_sub (fun x1 => [EQ [Function hag arg f x; Function hag arg f x1]]) [TRue] x y).
destruct hag. all : ProveContext.
apply ceq_ref. Defined.

Goal  forall x y : list ppt, x ## y -> EQ x # EQ y.
  intros. rewrite H. reflexivity. Qed.

Goal  forall x y : list ppt, x ## y -> If_Then_Else_ x # If_Then_Else_ y.
  intros. rewrite H. reflexivity. Defined.

(*To do: HD, TL, ++*)

Lemma ListMorph : forall x y : ppt,
x # y ->
forall x0 y0 : list ppt,
x0 ## y0 -> x :: x0 ## y :: y0.
Proof. intros. constructor. auto. auto. Qed.

Add Parametric Morphism : (@cons ppt) with
  signature  ceq ==> ceql ==> ceql as cons_mor.
Proof. apply ListMorph. Defined.


Lemma bla: forall x  u  , x##u ->  EQ x # EQ u.
Proof. intros. rewrite H. reflexivity. Qed.

Lemma bla2: forall x y  u w , x#u -> y#w -> [x;y] ## [u;w].
Proof. intros. rewrite H. rewrite H0. auto. reflexivity. Qed.

Lemma bla3: forall x y  u w , x#u -> y#w -> [EQ [x;y]; x] ## [EQ [u;w]; x].
Proof. intros. rewrite H. rewrite H0. apply ceql_ref. Qed.



Add Parametric Relation : (list ppt) cind
  reflexivity proved by @cind_ref
  symmetry proved by @cind_sym
  transitivity proved by @cind_trans
  as cind_rel.

Proposition ceql_cind : forall x y : list ppt, x ## y -> x ~ y.
Proof.
  intros. apply (@ceql_sub id y y x).  ProveContext. apply ceql_symm. apply H.
unfold id. apply cind_ref.  Qed.


(* "This did not work for me (Gergei):"
Add Parametric Morphism : (@ ) with
  signature  ceql   ==> cind as conscind_mor.
Proof.  apply ceql_cind. Defined.*)



Add Parametric Morphism : (@id (list ppt)) with
  signature  ceql   ==> cind as conscind_mor.
Proof.  apply ceql_cind. Defined.



Add Parametric Morphism : (@app ppt) with
  signature  ceql   ==> ceql ==> ceql as appceql_mor.
Proof. intros. induction H. simpl. auto. rewrite <-  app_comm_cons.
rewrite <-  app_comm_cons. apply ceql_cons.
auto. auto. Defined.



(*Goal forall x y, x ## y -> x ~ y.
  intros. rewrite H.*)

Proposition intID :
forall {x y} , id x ~ id y -> x ~ y.
Proof. intros. unfold id in H. trivial. Qed.

Proposition rewID :
forall {x y} , x ~ y -> id x ~ id y.
Proof. intros. unfold id  . trivial. Qed.


Lemma bla4: forall x y  u w , x#u -> y#w -> [EQ [x;y]; x] ~ [EQ [u;w]; x].
Proof. intros. apply intID.  rewrite H. rewrite H0. apply cind_ref. Qed.
 (* can we do  without introducing id?*)


Goal  forall x y : list ppt, x ## y -> If_Then_Else_ [EQ x] # If_Then_Else_ [EQ y].
  intros. rewrite H. reflexivity. Qed.


(* To be done:
Introduce morphism for the other function symbols also for ~
*)


(*E: [nonce n1; nonce n2; g n; g n ^^ R n4; g n ^^ R n3;
      g n ^^ R n4 ^^ R n3; g n ^^ R n1; g n ^^ R n2;
      g n ^^ R n3 ^^ R n1; g n ^^ R n3 ^^ R n2] ~
      [nonce n1; nonce n2; g n; g n ^^ R n4; g n ^^ R n3;
      g n ^^ R n5; g n ^^ R n1; g n ^^ R n2; g n ^^ R n3 ^^ R n1;
      g n ^^ R n3 ^^ R n2]
  c := expcomm : (g n ^^ R n3 ^^ R n1) # (g n ^^ R n1 ^^ R n3)*)













(**************************************************************************************)
(******************************** Lemmas for If_Then_Else_ ****************************)
(**************************************************************************************)












(*****************
 **    If_tf    **
 *****************)

Lemma If_tf : forall b ,
    b # (If b Then TRue Else FAlse).
Proof.
  intros. rewrite <- (@If_eval id id b). symmetry. apply If_same. ProveContext. ProveContext.
Defined.


(*****************
 **  If_morph   **
 *****************)

Lemma If_morph :(*"<<<"*)  forall  {f}  , (*">>>"*)
              forall  {b x y}  ,
      (*"<<<"*)ContextTerm General Term f -> (*">>>"*)
    (f (If b Then x Else y))
                     # (If b Then (f x) Else (f y)).
Proof.
  intros.
  apply (ceq_transymm  (@If_same b (f (If b Then x Else y)))).
assert (ContextTerm General Term
    (fun b' : ppt => f (If b' Then x Else y))).
 ProveContext.
  rewrite (@If_eval (fun b'=> f (If b' Then x Else y)) (fun b' => f (If b' Then x Else y)) b H0 H0).
apply (@ceq_subeq (fun x' : ToL Term => If b
    Then (f  x')
    Else (f  (If FAlse
               Then x
               Else y)))  (fun x': ToL Term => (If b
      Then f x
      Else f y)) x (If TRue
               Then x
               Else y)). ProveContext. ProveContext.
 apply ceq_symm. apply If_true.
apply (@ceq_subeq (fun x': ToL Term => If b
    Then (f  x)
    Else (f  x'))  (fun x': ToL Term  => (If b
      Then f x
      Else f y)) y (If FAlse
               Then x
               Else y)). ProveContext. ProveContext.
 apply ceq_symm. apply If_false.
apply ceq_ref. Qed.



(*****************
 **  If_idemp   **
 *****************)

Lemma If_idemp : forall { b x1 y1 x2 y2 } ,
    (If b Then (If b Then x1 Else y1) Else (If b Then x2 Else y2))
  # (If b Then x1 Else y2).
Proof. intros. rewrite (@If_eval (fun b' => If b' Then x1 Else y1) (fun b' => If b' Then x2 Else y2) b).
rewrite  (@If_true x1 y1).  rewrite (@If_false x2 y2). reflexivity. ProveContext. ProveContext.
Qed.
