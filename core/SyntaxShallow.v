
(************************************************************************)
(* Copyright (c) 2021, Gergei Bana                                      *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(* Special thanks to Qianli and Ajay K. Eeralla                         *)
(************************************************************************)

Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Setoid.
Import ListNotations.
Require Export FunctionalExtensionality.
Require Import Coq.Bool.Bool.
Require Export Symbols.





(**************************************************************************************)
(**************************************************************************************)
(************************************ SHALLOW EMBEDDING ********************************)
(**************************************************************************************)
(**************************************************************************************)


(* Here we carry out the shallow embedding, that is, we represent the syntax and semantics of
our logic in Coq. *)


(**************************************************************************************)
(***************************************** DOMAIN *************************************)
(**************************************************************************************)


(* We introduce the type "ppt" representing PPT algorithms, our domain of interpretation.
Its interpretation [[ppt]] is the set of PPT
algorithms with input 1^\eta where
eta is the security parameter. ppt is the combination of the bool and message types of the paper*)

Parameter  ppt: Type.



(**************************************************************************************)
(************************** SHALLOW EMBEDDING OF FUNCTION SYMBOLS **************************)
(**************************************************************************************)


(* With the introduction of "ppt", Coq delivers for us the "list ppt" type, that is a representaton of
all finite lists of elements in "ppt". When we embed the above function symbols of "Symbols",
as function symbols of Coq, all of them are interpreted as of type "list ppt -> ppt".
This turns out to be less notation-heavy than
if we defined them on products. E.g. EQ is ppt x ppt -> ppt, but we represent it as list ppt -> ppt. However,
we will require that function symbols of arity "arg n" output default when the input list is shorter, and
drops the rest of the list when it is longer than "arg n".
Constants (nullary function symbols) are also embedded as elements in "ppt" and the obvious link is
established between the two embeddings *)


(* FOR THE COMPUTATIONAL SEMANTICS OF EACH FUNCTION SYMBOL AS PPT ALGORITHMS, IT MUST BE ENSURED THAT THEY
ALL USE DISJOINT SEGMENTS OF THE HONEST RANDOM TAPE *)




(* Here to each function symbol symbol in "Symbols", "Function" assigns a function symbol of
type "list ppt -> ppt"*)
Parameter Function : forall (hag : ComputationType) (arg : arity), Symbols hag arg -> list ppt -> ppt.

(* Here to each constant symbol symbol in "Symbols hag (narg 0)", "Constant" assigns a function symbol
of type "ppt"*)
Parameter Constant : forall (hag : ComputationType) , Symbols hag (narg 0) ->  ppt.

(* Constant function symbols are the same as nullary function symbols evaluated on any element in "list ppt"*)
Axiom Const0Func :
  forall hag  c , (fun lx : list ppt => Constant hag c) = Function hag (narg 0) c.

(* Function symbols output default if input length is less than arity *)
Axiom ArityMatchLess : forall hag n f lx,
      length lx  < n  ->  Function  hag (narg n) f lx  = (Constant Deterministic defaults).

(* Function symbols output only depends on the initial segment of the input list matching arity. *)
Axiom ArityMatchGreater : forall hag n f lx ly,
      length lx  <= n -> length ly = n -> Function hag (narg n) f (ly ++ lx) =  Function hag (narg n) f ly.


(* We introduce the following notations for the above specific function symbols *)

Notation "'default'" := (Constant Deterministic  defaults).
Notation "'TRue'" := (Constant Deterministic  TRues).
Notation "'FAlse'" := (Constant Deterministic  FAlses).
Notation "'EQ'" := (Function Deterministic (narg 2) EQs).
Notation "'If_Then_Else_'" := (Function Deterministic (narg 3) ITEs).
Notation "'If' c1 'Then' c2 'Else' c3" := (Function Deterministic (narg 3) ITEs [c1; c2; c3])
  (at level 55, right associativity).
Notation "'adv' n" := (Function Adversarial arbarg (advs n)) (at level 0).
Notation "'nonce' n" :=  (Constant Honest (nonces n)) (at level 0).

Notation " c1 '≟' c2" := (EQ [c1; c2]) (at level 101, left associativity).

(* The semantics [[EQ]] is the PPT algorithm that on
interpretations [[x]], [[y]] of x and y assigns the PPT algorithm [[E(x,y)]]
that outputs true when the outputs of its inputs [[x]] and [[y]] are the
same and outputs false otherwise. *)



(* The semantics of  [[If_Then_Else_]] on input
PPT algorithms [[b]], [[x]], [[y]], it assigns the PPT algorithm
 [[If_Then_Else_(b,x,y)]] that outputs the output of [[x]] when
  the output of [[b]] is true; outputs the output of [[y]] when
the output of [[b]] is false; outputs an error otherwise *)







(* We need to be able to distinguish those functions of type "list ppt -> ppt" that come from
our syntax, and any general function of type "list ppt -> ppt" because many of our axioms only work
for those coming from our syntax. For this we use the metapredicate (not in our original language) "PPT"*)


(* "PPT" creates the ensembles within type list ppt -> ppt corresponding
to Honest, Deterministic, Adversarial and General function symbols. For example for
"f : list ppt -> ppt", the proposition "(PPT Adversarial) f" states that
f is an adversarial computation coming from our syntax. Note that our CCSA logic is very limited
and we do not have a domain for "list ppt -> ppt". On the other hand, we
can use the rich language of Coq to reason about such maps. Hence
"PPT Adversarial" is a "metapredicate", it is
a predicate in Coq, but it is not a predicate of our logic *)
(*deterministic PPT is also honest and adversarial, and honest and adversarial are also general, so after
embedding these are not disjoint any more. *)

(* "PPT" maybe be removable by redifining "Context" *)







(* It probably makes no sense to make this inductive: *)


Inductive  PPT: ComputationType -> ((list ppt -> ppt) -> Prop) :=
| FunHAG :
      forall  hag arg f ,
        (PPT hag) (Function  hag arg f)
| DeterministicHonest :
      forall {lf : list ppt -> ppt} ,
        (PPT Deterministic) lf
         -> (PPT Honest) lf
| DeterministicAdversarial :
      forall {lf : list ppt -> ppt} ,
        (PPT Deterministic) lf
         -> (PPT Adversarial) lf
| HonestMixed :
      forall {lf : list ppt -> ppt} ,
        (PPT Honest) lf
        -> (PPT Mixed) lf
| AdversarialMixed :
      forall {lf : list ppt -> ppt} ,
        (PPT Adversarial) lf
         -> (PPT Mixed) lf
| MixedGeneral :
      forall {lf : list ppt -> ppt} ,
        (PPT Mixed) lf
         -> (PPT General) lf.


Axiom ConstantGeneral :
      forall {x0 :  ppt} ,
        (PPT General) (fun x => x0).








(**************************************************************************************)
(************************************** PREDICATE *************************************)
(**************************************************************************************)


(************************* Computational Indistinguishability *************************)



(* We have a single predicate symbol, computational indistinguishability *)
(* "x ~ y" means that the output distributions
of [[x]] and [[y]] are indistinguishable for all
PPT algorithms*)

Parameter cind : list ppt -> list ppt -> Prop.
Notation "x ~ y" := (cind x y) (at level 75, no associativity): type_scope.




(* The definitions of Syntax and Shallow Embedding end here *)


(**************************************************************************************)
(**************************************************************************************)
(**************************************************************************************)
(**************************************************************************************)
(**************************************************************************************)
(**************************************************************************************)
(**************************************************************************************)
(**************************************************************************************)
(**************************************************************************************)








(* Below there are some further notations and some propositions that follow from the above *)





(**************************************************************************************)
(**************************************************************************************)
(*********************************** ABBREVIATIONS ************************************)
(**************************************************************************************)
(**************************************************************************************)

(******************************* If Then Else on lists ********************************)



(*The following defines "IF b THEN x ELSE y " for x y lists.
Works on entry by entry:
E.g. IF b THEN [x1, x2] ELSE [y1, y2] =
[ If b Then x1 Else y1 , If b Then x2 Else y2 ] - Prove it
If x is shorter then y, then the rest of y simply does not appear in the result:
E.g. IF b THEN [x1, x2] ELSE [y1, y2, y3] =
[ If b Then x1 Else y1 , If b Then x2 Else y2 ] - Prove it
If x is longer than y the rest of x does not appear:
E.g. IF b THEN [x1, x2, x3] ELSE [y1, y2 ] =
[ If b Then x1 Else y1 , If b Then x2 Else y2 ] - Prove it
*)


(* "Better change this to inductive:" *)

Fixpoint IF_THEN_ELSE_ (b: ppt) (lx : list ppt) (lx' : list ppt) : list ppt :=
match lx with
| nil  => nil
| a :: m => match lx' with
            | nil => nil
            | a' :: m' => (If_Then_Else_ [b; a; a']) :: (IF_THEN_ELSE_ b m m')
            end
end.

Notation "'IF' c1 'THEN' c2 'ELSE' c3" := (IF_THEN_ELSE_ c1 c2 c3)
  (at level 200, right associativity).



(*********************** Overwhelming (Computational) Equality ************************)



(* Tis is also an abbreviation, but its semantics turns out to be that
[[x]] and [[y]] are equal except maybe for negligible probability *)

Definition ceq :  ppt -> ppt -> Prop := fun x y => [ (EQ [x ; y ]) ] ~ [ TRue ].
Notation "x # y" := (ceq x y) (at level 74, no associativity): type_scope.






(**************************************************************************************)
(**************************************************************************************)
(*************************** Auxilliary Functions for Lists ***************************)
(**************************************************************************************)
(**************************************************************************************)



(* 0th entry, head of a list. nil is mappted to default *)
Definition HD (lx : list ppt) : ppt :=  hd default lx.

(* Tail of a list, taking off the first enrty *)
Definition TL (lx : list ppt) : list ppt :=  tl lx.

(* Mapping on the nth element. nil is mappted to default *)
Definition Nth (n : nat) (lx : list ppt) := nth n lx default.


(* Zeroth element and Head are the same *)
Lemma ZerothisHD :
      Nth 0 = HD.
Proof. apply functional_extensionality. intros. induction x. cbn.
 reflexivity. cbn. reflexivity. Qed.

(* S(n)th element and the nth of the tail are the same *)
Lemma SnthisnthTL :
      forall {n} ,
        (fun lx => Nth (S n) lx) = (fun lx => Nth  n (TL lx)).
Proof. intros. apply functional_extensionality. intros. induction x.
cbn. induction n. reflexivity. reflexivity. cbn. reflexivity. Qed.


(* concetanating the head and the tail is the identity map on non-nil. *)
Lemma IDisHDTL :
      forall {lx} ,
        lx <> [] -> lx = (HD lx) :: (TL lx).
Proof. intros. induction lx. destruct H. reflexivity. cbn. reflexivity. Qed.








(*******************************************************************************)
(*******************************************************************************)
(********************* Properties of our PPT functions *************************)
(*******************************************************************************)
(*******************************************************************************)


Proposition Func0Const :
forall (hag : ComputationType) (c :  Symbols hag (narg 0)) (lx : list ppt), Constant hag c = Function hag (narg 0) c lx.
Proof. intros. rewrite <- (Const0Func hag c). auto.
Qed.


Proposition ConstHAG : forall hag hag' c , PPT hag (Function hag' (narg 0) c)
-> PPT hag (fun _ : list ppt => Constant hag' c).
Proof. intros. rewrite  (Const0Func hag' c). auto.
Qed.


Proposition nonceHonest :
      forall n , (PPT Honest) (fun lx : list ppt => nonce n).
Proof. intros. simpl (nonce n).  rewrite Const0Func. apply
FunHAG. Qed.


Proposition advAdversarial :
      forall n ,
        (PPT Adversarial) (adv n).
Proof. intros. simpl (adv n).   apply
FunHAG. Qed.



(*Additional function symbols*)
Parameter ERrors : Symbols Deterministic (narg 0).
Notation "'Error'" := (Constant Deterministic ERrors).

Parameter Lens : Symbols Deterministic (narg 1).
Notation "'Len'" := (Function Deterministic (narg 1) Lens).
