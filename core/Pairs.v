
(************************************************************************)
(* Copyright (c) 2020, Gergei Bana                                      *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Setoid.
Require Import Coq.micromega.Lia.
Import ListNotations.

Require Export Freshness.



Parameter pairs : Symbols.

Notation "'pair'" := (FunctionSymbol Deterministic pairs).



Axiom (*metaaxiom*) pair2arg :
      forall {x1 x2 : ppt} {lx : list ppt} ,
        (pair nil) = default
        /\ pair [x1] = default
        /\ (pair ([x1; x2]++lx)) = (pair [x1; x2]).


Proposition ctpair :
       forall {hag tol tc1 tc2},
         ContextTerm hag tol tc1
         -> ContextTerm hag tol tc2
         -> ContextTerm hag tol (fun x => (pair ([ (tc1 x) ; (tc2 x)]) )).
 Proof. intros. ProveContext. Qed.


Parameter proj1s : Symbols.

Notation "'proj1'" := (FunctionSymbol Deterministic proj1s).



Axiom (*metaaxiom*) proj11arg :
      forall {x : ppt} {lx : list ppt} ,
        (proj1 nil) = default
        /\ (proj1 ([x]++lx)) = (proj1 [x]).



Axiom (*metaaxiom*) proj1pair :
      forall {x1 x2 : ppt} ,
        (proj1 [pair [x1 ; x2]]) # x1.


Parameter proj2s : Symbols.

Notation "'proj2'" := (FunctionSymbol Deterministic proj2s).




Axiom (*metaaxiom*) proj21arg :
      forall {x : ppt} {lx : list ppt} ,
        (proj2 nil) = default
        /\ (proj2 ([x]++lx)) = (proj2 [x]).


Axiom  proj2pair :
      forall {x1 x2 : ppt} ,
        (proj2 [pair [x1 ; x2]]) # x2.


Lemma bla15 : ContextTerm General Term (fun x => proj2 [(adv 5) [(pair [EQ [(nonce 2) ; x] ; x])]]).
Proof. ProveContext. Qed.
