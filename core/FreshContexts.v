
(************************************************************************)
(* Copyright (c) 2020, Gergei Bana, Qianli Zhang                        *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)

Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.PeanoNat.
Require Export Ensembles.
Require Export Setoid.
Require Import Coq.micromega.Lia.
Import ListNotations.


Require Export NonceImplications.


(**************************************************************************************)
(******************************** Nonce Lists *****************************)
(**************************************************************************************)



(**************************************************************************************)
(******************************** Context Freshness *****************************)
(**************************************************************************************)
Proposition IdHD :
forall tol, (fun x  => Id tol Term x ) = (fun x  => HD (Id tol List x )).
Proof. intros. destruct tol.
  - unfold Id at 2. unfold HD. simpl. auto.
  - unfold Id. auto.
Qed.


Check Id.

Section Freshc.
Variable tol: TermOrList.
Inductive Freshc : ppt -> (ToL tol -> list ppt) -> Prop := (* metapredicate *)
| frcConc : forall nc (t : ToL tol ->  ppt) (tc :  ToL tol ->  list ppt) ,
    (FreshTermc  nc t)
    -> (Freshc nc tc)
    -> (Freshc nc (fun x => ((t x) :: (tc x))))
| frclConc : forall  nc (t : ToL tol ->  list ppt) (tc :  ToL tol ->  list ppt) ,
    (Freshc  nc t)
    -> (Freshc nc tc)
    -> (Freshc  nc (fun x => ((t x) ++ (tc x))))
| frcFAdv : forall  nc (tl: list ppt -> list ppt) (tc : ToL tol -> list ppt) ,
    (Context Adversarial List tl)
    -> Freshc  nc tc
    -> Freshc  nc (fun y => tl (tc y))
| frcId : forall  n , Freshc  (nonce n) (Id tol List)

with FreshTermc  :  ppt -> (ToL tol -> ppt) -> Prop :=
| frtcNN : forall (n m : nat) , n <> m
                        -> (FreshTermc (nonce n) (fun x => (nonce m)))
| frtcFAdv: forall nc (t: list ppt -> ppt) (tc : ToL tol ->  list ppt) ,
    (ContextTerm Adversarial List t)
    -> Freshc nc tc
    -> FreshTermc  nc (fun x =>  (t (tc x))).
(*| frtcCAdv: forall  n (t:  ToL tol -> ppt) ,
    (ContextTerm Adversarial tol t)
    -> FreshTermc  (nonce n) t.*)
End Freshc.


Proposition frcIdnc :
forall  nc tol , Nonce  nc -> Freshc tol nc (Id tol List).
Proof. intros. inversion H. rewrite <- H0. apply frcId. Qed.

Proposition frtcId :
forall  n tol , FreshTermc tol (nonce n) (Id tol Term).
Proof. intros. rewrite IdHD. apply frtcFAdv.
  - ProveContext.
  - apply frcId.
Qed.

Proposition frtcIdnc :
forall  nc tol , Nonce  nc -> FreshTermc tol nc (Id tol Term).
Proof. intros. rewrite IdHD. apply frtcFAdv.
  - ProveContext.
  - apply frcIdnc. auto.
Qed.


Proposition  frcnil :
forall  nc tol,  Nonce nc -> Freshc tol nc (fun x => nil).
Proof. intros. apply (frcFAdv tol nc (fun x => []) (Id tol List)).
  - ProveContext.
  - inversion H. rewrite <- H0. apply frcId.
Qed.


Proposition frtcCAdv: forall tol  n (t:  ToL tol -> ppt) ,
    (ContextTerm Adversarial tol t)
    -> FreshTermc tol (nonce n) t.
Proof. intros. destruct tol.
  - apply (frtcFAdv Term (nonce n) (fun x => t (HD x)) (Id Term List)).
    + ProveContext.
    + apply frcId.
  - apply frtcFAdv.
    + auto.
    + apply frcId.
Qed.





Proposition frtcCFresh : forall tol nc tl , Fresh nc tl -> Freshc tol nc (fun x => tl).
Proof.
intros. assert (Fresh nc tl); auto. apply FreshIsNonce in H0.
apply (Fresh_mut (fun nc tl => Freshc tol nc (fun _ : ToL tol => tl) ) (fun nc t => FreshTermc tol nc (fun _ : ToL tol => t) ) ) in H.
  - auto.
  - intros. apply frcnil. apply NonceNonce.
  - intros. apply frcConc. auto. auto.
  - intros. constructor. auto.
  - intros. constructor. auto. auto.
  - intros. apply frtcCAdv. destruct tol.
    + assert (Context Adversarial Term (fun x => [x])). ProveContext. apply (cltCont H1 H2) .
    + auto.
Qed.







Scheme Freshc_mut := Minimality for Freshc Sort Prop
  with FreshTermc_mut := Minimality for FreshTermc Sort Prop.

Inductive ListFreshc tol : list  ppt -> (ToL tol -> list ppt) -> Prop := (* metapredicate *)
| lfrctnil :  forall  lx, ListFreshc tol [ ] lx
| lfrctConc : forall  nc nl lx, Freshc tol  nc lx
                           -> ListFreshc tol nl lx
                           -> ListFreshc tol (nc::nl) lx.


Inductive ListFreshTermc tol : list  ppt ->  (ToL tol  -> ppt) -> Prop := (* metapredicate *)
| lfrtctnil :  forall lx, ListFreshTermc tol [ ] lx
| lfrtctConc : forall nc nl lx, FreshTermc tol nc lx
                           -> ListFreshTermc tol nl lx
                           -> ListFreshTermc tol (nc::nl) lx.



(* These axioms say that if (c nl1)  is fresh wrt nl, that is, (c nl1) is independent of
the nonces in nl, then nl1 can be changed to another list that does not contain nonces from nl.
This is computationally sound, but it is not a very nice axiom as it is quite complex.
Hopefully eventually we can replace it with something simpler that implies this. *)

Axiom FreshcRem :
  (*"<<<"*)  forall tol nl c nl1,
              Context Adversarial List c -> NonceList nl1 ->
                 ListFreshc tol nl (fun x => c ((nl1 ++ (Id tol List x)))) ->
exists nl2,( ListFresh nl nl2 /\  NonceList nl2 /\ (fun x => c (nl1 ++ (Id tol List x))) = (fun x => c (nl2 ++ (Id tol List x)))). (*">>>"*)


(*
Proposition FreshcTermRem :
  (*"<<<"*)  forall nl c nl1,
              Context Adversarial List c -> NonceList nl1 ->
                 ListFreshc Term nl (fun x => c (x :: nl1)) ->
exists nl2,( ListFresh nl nl2 /\  NonceList nl2 /\ (fun x => c (x :: nl1)) = (fun x => c (x :: nl2))). (*">>>"*)
Proof. Admitted. *)


Axiom  FreshTermcRem tol :
  (*"<<<"*)  forall tol  nl c nl1,
              ContextTerm Adversarial List c -> NonceList nl1 ->
                 ListFreshTermc tol nl (fun x => c (nl1 ++ (Id tol List x) )) ->
exists nl2,( ListFresh nl nl2 /\  NonceList nl2 /\ (fun x => c (nl1 ++ (Id tol List x))) = (fun x => c (nl2 ++ (Id tol List x)))). (*">>>"*)


(*
Proposition  FreshTermcTermRem :
  (*"<<<"*)  forall  nl c nl1,
              ContextTerm Adversarial List c -> NonceList nl1 ->
                 ListFreshTermc Term nl (fun x => c (x :: nl1)) ->
exists nl2,( ListFresh nl nl2 /\  NonceList nl2 /\ (fun x => c (x :: nl1)) = (fun x => c (x :: nl2))). (*">>>"*)
Proof. Admitted.*)


(* FreshRem, FreshTermRem, FreshTermcTermRem should be provable from FreshcTermRem, *)

(*
Proof. intros. assert ((fun x : ppt => c (x :: nl1)) = (fun x : ppt => HD [c (x :: nl1)] )  ).
unfold HD. simpl. auto.
assert (Context Adversarial List (fun x => [c x])). ProveContext.
assert (ListFreshcTerm nl
       (fun x : ppt => [c (x :: nl1)])). apply (frctConc  nc (fun x => c (x :: nl1)) (fun x => [])).
apply lfrctFAdv
apply (FreshcTermRem nl (fun x => [c x]) nl1 H3 H0) in H1.


nc (t :  ppt ->  ppt) (tc :  ppt ->  list ppt) *)


Lemma FreshcNonce : forall tol nc tl, Freshc tol nc tl  -> Nonce nc.
Proof. intros. induction H.
  - auto.
  - auto.
  - auto.
  - apply NonceNonce.
Qed.


Lemma FreshTermcNonce : forall tol nc t, FreshTermc tol nc t  -> Nonce nc.
Proof. intros. inversion H.
  - ProveNonceList.
  - apply FreshcNonce in H1. auto.
Qed.




Lemma ListFreshcNonceList :
forall tol nl xl, ListFreshc tol nl xl -> NonceList nl.
Proof. intros. induction H.
  - constructor.
  - apply FreshcNonce in H. apply nlN.
    + auto.
    + auto.
Qed.


Lemma ListFreshTermcNonceList :
forall tol nl xl, ListFreshTermc tol nl xl -> NonceList nl.
Proof. intros. induction H.
  - constructor.
  - apply FreshTermcNonce in H. apply nlN.
    + auto.
    + auto.
Qed.






(*
Lemma Fresh_tail : forall tol nc lx,
    (Freshc tol nc (fun _ => lx) -> Freshc tol nc (fun x => lx ++ (Id tol List x))).
Proof. Admitted.
(*  intros.  induction lx. simpl. apply FreshcNonce in H. inversion H.
rewrite <- H0. destruct tol0. unfold Id. apply frcConc. apply frtcId. apply frcnil.
apply NonceNonce. unfold Id. apply frcId.
assert ((fun x : ToL tol0 => (a :: lx) ++ Id tol0 List x) = (fun x : ToL tol0 => a :: (lx ++ Id tol0 List x))).
apply functional_extensionality. intros.
 rewrite <- app_comm_cons. auto. rewrite H0. apply frcConc.
inversion H.
 apply (frcFAdv tol0 nc (Id tol0 List)).
  - Check frtcCFresh. Admitted.*)
*)


Proposition FreshcfromNonceList : forall tol nc tl , Freshc tol nc tl <->
  exists f lx , Context Adversarial List f /\ NonceList lx /\
           Fresh nc lx /\ tl = (fun x => f (lx ++ (Id tol List x))).
Proof.
  split; intros.
  - apply (Freshc_mut tol0
                          (fun nc tl => exists f lx,
                            Context Adversarial List f /\
                            NonceList lx /\
                            Fresh nc lx /\
                            tl = (fun x => f (lx ++ (Id tol0 List x))))
                          (fun nc tl => exists f lx,
                            ContextTerm Adversarial List f /\
                            NonceList lx /\
                            Fresh nc lx /\
                            tl = (fun x => f (lx ++ (Id tol0 List x))))); intros.
    + inversion_clear H1 as [f [lx [H4 [H5 [H6 H7]]]]].
      inversion_clear H3 as [fc [lc [H8 [H9 [H10 H11]]]]].
      rewrite H7; rewrite H11; simpl.
      exists (fun l => (f ((firstn  (length lx) l) ++ (skipn ((length lx) + (length lc)) l)) ) :: (fc  (skipn (length lx) l))), (lx ++ lc).
      repeat split.
      * ProveContext.
      * ProveNonceList.
      * ProveFresh.
      * rewrite <- app_length. apply functional_extensionality. intros.
        rewrite (skpn (lx ++ lc) (Id tol0 List x)). rewrite <- app_assoc.
        rewrite skpn.  rewrite fstn. auto.
    + inversion_clear H1 as [f [lx [H4 [H5 [H6 H7]]]]].
      inversion_clear H3 as [fc [lc [H8 [H9 [H10 H11]]]]].
      rewrite H7; rewrite H11; simpl.
      exists (fun l => (f ((firstn  (length lx) l) ++ (skipn ((length lx) + (length lc)) l)) ) ++ (fc  (skipn (length lx) l))), (lx ++ lc).
      repeat split.
      * ProveContext.
      * ProveNonceList.
      * ProveFresh.
      * rewrite <- app_length. apply functional_extensionality. intros.
        rewrite (skpn (lx ++ lc) (Id tol0 List x)). rewrite <- app_assoc.
        rewrite skpn. rewrite fstn. auto.
    + inversion_clear H2 as [f [lx [H3 [H4 [H5 H6]]]]].
      rewrite H6; simpl.
      exists (fun l => (tl0 (f l))), lx.
      repeat split.
      ProveContext. auto. auto.
    + exists (fun l => l), [].
      repeat split.
      ProveContext. ProveNonceList. ProveFresh.
    + exists (fun l => HD l), ([nonce m]).
      repeat split.
      ProveContext. ProveNonceList. ProveFresh.
    + inversion_clear H2 as [f [lx [H3 [H4 [H5 H6]]]]].
      rewrite H6; simpl.
      exists (fun l => t (f l)), lx.
      repeat split. ProveContext. auto. auto.
    + auto.
  - inversion_clear H as [f [lx [H1 [H2 [H3 H4]]]]].
    rewrite H4. constructor.
    + auto.
    + apply (frtcCFresh tol0) in H3.
      apply frclConc.
      * auto.
      * apply (frcIdnc nc tol0). apply FreshcNonce in H3. auto.
Qed.



Proposition FreshTermcfromNonceList : forall tol nc tl , FreshTermc tol nc tl <->
  exists f lx , ContextTerm Adversarial List f /\ NonceList lx /\
           Fresh nc lx /\ tl = (fun x => f (lx ++ (Id tol List x))).
Proof.
  intros tol nc tl. split; intros.
  - assert (Freshc tol nc (fun x => [tl x])).
    constructor; auto; apply frcnil; apply (FreshTermcNonce tol nc tl H).
    apply FreshcfromNonceList in H0.
    inversion_clear H0 as [f [lx [H1 [H2 [H3 H4]]]]].
    exists (fun l => HD (f l)), lx.
    repeat split. constructor. auto. auto. auto.
    apply functional_extensionality.
    intros. (*it's interesting that we can't apply equal_f in H4*)
    assert (forall x : ToL tol, [tl x] = f (lx ++ Id tol List x)).
    apply equal_f; auto.
    specialize H0 with x.
    rewrite <- H0. simpl. auto.
  - inversion_clear H as [f [lx [H1 [H2 [H3 H4]]]]].
    rewrite H4.
    constructor. auto. apply frclConc.  apply frtcCFresh. auto.
    apply (frcIdnc nc tol). apply FreshIsNonce in H3.  auto.
Qed.



Proposition ListFreshcfromNonceList : forall tol nl tl ,
  nl <> [] -> ListFreshc tol nl tl
  -> exists f nl2 , Context Adversarial List f /\ NonceList nl2
              /\ ListFresh nl nl2 /\ tl = (fun x => f (nl2 ++ (Id tol List x))).
Proof.
intros tol nl tl. intros H. intros LF.
assert ( forall lx,  ListFreshc tol nl lx ->
exists (f : ToL List -> list ppt) (nl2 : list ppt),
Context Adversarial List f /\ NonceList nl2 /\
ListFresh nl nl2 /\ lx = (fun x => f (nl2 ++ (Id tol List x)))).
  { intros lx. intros H0. induction H0.
    - contradiction.
    - induction nl.
      + apply (FreshcfromNonceList tol nc lx) in H0. inversion H0. inversion H2.
        exists (fun lx => x  lx). exists x0. destruct H3. destruct H4. destruct H5.
        repeat split.
        * auto.
        * ProveContext.
        * apply lfrConc.
          ** auto.
          ** constructor.
        * unfold TL.  auto.
      + assert (a :: nl <> []).
         { unfold not. intros.  inversion H2. }
        apply IHListFreshc in H2.
        * inversion H2.  inversion H3.
          destruct H4. destruct H5. destruct H6.  apply (lfrctConc tol nc (a :: nl) lx H0) in H1.
          rewrite H7 in H1.
          assert (ListFreshc tol (nc :: a :: nl)
          (fun x1 => x (x0 ++ (Id tol List x1)))).
          { auto. }
          apply  (FreshcRem tol (nc :: a :: nl) x x0 H4 H5) in H1.
          inversion H1. destruct H9. exists (fun lx => x  lx). exists x1.
          destruct H10. rewrite H11 in H7. repeat split.
          ** ProveContext.
          ** auto.
          ** inversion LF. auto.
          ** auto.
        * inversion LF. auto.  }
auto.
Qed.


Lemma Termc2C: forall tol a tl, FreshTermc tol a tl
                 -> Freshc tol a (fun x : ToL tol => [tl x]).
  intros tol a tl H.
  constructor. auto.
  apply frcnil. apply FreshTermcNonce in H. auto.
Qed.


Lemma ListTermc2C : forall tol nl tl,  ListFreshTermc tol nl tl
                   -> ListFreshc tol nl (fun x : ToL tol => [tl x]).
  intros tol nl tl; intros.
  induction nl.
  - constructor.
  - inversion_clear H. apply IHnl in H1.
    constructor. apply Termc2C; auto. auto. Qed.



Proposition ListFreshTermcfromNonceList :
  forall tol nl tl ,  nl <> [] -> ListFreshTermc tol nl tl
                 -> exists f nl2 , ContextTerm Adversarial List f /\
                             NonceList nl2 /\ ListFresh nl nl2 /\
                             tl = (fun x => f (nl2 ++ (Id tol List x))).
Proof.
  intros tol nl tl; intros.
  assert (ListFreshc tol nl (fun x => [tl x])).
  apply ListTermc2C. auto.
  apply ListFreshcfromNonceList in H1.
  - inversion_clear H1 as [f [nl2 [H2 [H3 [H4 H5]]]]].
    exists (fun l => HD (f l)), nl2.
    repeat split. constructor. auto. auto. auto.

    apply functional_extensionality.
    intros.
    assert (forall x : ToL tol, [tl x] = f (nl2 ++ Id tol List x)).
    apply equal_f; auto.
    specialize H1 with x.
    rewrite <- H1. simpl. auto.
  - auto. Qed.



Proposition NonceRename :
 (*"<<<"*)   forall {f :  ppt -> list ppt},
    forall {nc1 nc2 } ,  ListFreshc Term [nc1 ; nc2] f  ->   (*">>>"*)
                    (f nc1) ~ (f nc2).
(*Qianli modified it from  "ListFreshcTerm [nc1 ; nc2] f" to  "ListFreshc Term [nc1 ; nc2] f"*)
Proof.
  intros.
  assert ([nc1; nc2] <> []). unfold not; intros. inversion H0.
  apply  (ListFreshcfromNonceList Term [nc1; nc2] f) in H0.
  inversion_clear H0 as [f0 [nl2 [H1 [H2 [H3 H4]]]]].
  rewrite H4.
  apply cind_funcapp; auto.
  unfold Id; simpl.
  inversion_clear H3; inversion_clear H5; clear H6.
  pose (f1 := fun l: list ppt => (skipn 1 l) ++ (firstn 1 l)).
  apply (@cind_funcapp f1 (nc1 :: nl2) (nc2 :: nl2)).
  unfold f1. constructor. ProveContext. ProveContext.
  apply FreshInd; auto. reflexivity.
  auto. Qed.



(**************************************************************************************)
(****************************** Below here might be useless *****************************)
(**************************************************************************************)



(**************************************************************************************)
(******************************** List Freshness' *****************************)
(**************************************************************************************)





(*



Inductive ListFresh' : list ppt -> list ppt -> Prop := (* metapredicate *)
| nilfrl' : forall t,  ListFresh' [ ] t
| lfrnil' : forall nl, NonceList nl -> ListFresh' nl [ ]
| lfrConc' : forall nl (t : ppt) (tc : list ppt) ,
    (ListFreshTerm' nl t)
    -> (ListFresh' nl tc)
    -> (ListFresh' nl (t :: tc))
with ListFreshTerm' : list ppt -> ppt -> Prop :=
| nilfrt' : forall t,  ListFreshTerm' [ ] t
| lfrtNN' :
forall nl nc t ,  FreshTerm nc t ->
(ListFreshTerm' nl t) ->
                         (ListFreshTerm' (nc :: nl) t)
| lfrtFAdv': forall nl (t: list ppt -> ppt) (tc : list ppt) ,
    (ContextTerm Adversarial List t)
    -> ListFresh' nl tc
    -> ListFreshTerm' nl (t tc)
| lfrtCAdv': forall nl (t: ppt) ,
    (ContextTerm Adversarial List (fun x => t))
    -> NonceList nl -> ListFreshTerm' nl t.


Proposition ListFresh'NonceList :
forall nl tl , ListFresh' nl tl -> NonceList nl.
Proof.  intros. induction H. constructor. auto. auto.
Qed.


Proposition ListFreshTerm'NonceList :
forall nl t , ListFreshTerm' nl t -> NonceList nl.
Proof.  intros. induction H. constructor.
apply nlN. apply FreshTermIsNonce in H.  auto. auto.
apply ListFresh'NonceList  in H0. auto. auto.
Qed.



Scheme ListFresh'_mut := Minimality for ListFresh' Sort Prop
  with ListFreshTerm'_mut := Minimality for ListFreshTerm' Sort Prop.


Check ListFreshTerm'_mut.




Proposition LF : forall nl tl, ListFresh' nl tl -> ListFresh nl tl.
Proof. intros.
apply (ListFresh'_mut (fun nl tl => ListFresh nl tl) (fun nl t => ListFreshTerm nl t)).
 constructor. intros.  apply lfrlnil. auto.
 intros.  apply lfrlConc. apply ListFreshTermNonceList in H1. auto. auto. auto.
intros. constructor. intros.
apply lfrtConc. auto. auto.
intros. apply lfrtFAdv.  apply ListFreshNonceList in H2. auto. auto. auto.
intros.   apply lfrtCAdv. auto. ProveContext.  auto.
Qed.



Proposition LFT : forall nl t, ListFreshTerm' nl t -> ListFreshTerm nl t.
Proof. intros.
apply (ListFreshTerm'_mut (fun nl tl => ListFresh nl tl) (fun nl t => ListFreshTerm nl t)).
 constructor. intros.  apply lfrlnil. auto.
 intros. apply lfrlConc. apply ListFreshTermNonceList in H1. auto. auto. auto.
intros. constructor. intros.
apply lfrtConc. auto. auto.
intros. apply lfrtFAdv. apply ListFreshNonceList in H2. auto. auto. auto.
intros.   apply lfrtCAdv. auto. ProveContext.  auto.
Qed.


Proposition blabla : forall nc tl , Fresh nc tl ->   ListFresh' [nc] tl.
Proof. intros.  apply (Fresh_mut (fun nc tl =>  ListFresh' [nc]  tl)  (fun nc t =>  ListFreshTerm' [nc]  t)) in H.
auto. intros. apply lfrnil'.  ProveNonceList.
intros.   apply lfrConc'.  auto. auto.
intros.  apply lfrtNN'. ProveFresh. constructor. intros.
apply lfrtFAdv'. auto. auto. intros. apply lfrtCAdv'. auto.
ProveNonceList.
Qed.


Proposition invlfrConc' : forall t  nl tl,  ListFresh'  nl (t :: tl) -> (ListFreshTerm' nl  t /\ ListFresh' nl tl).
Proof. intros. inversion H. constructor. constructor. constructor. constructor.
auto. auto. Qed.

Proposition lfrlConc' : forall nc nl tl , Fresh  nc tl ->  ListFresh' nl tl ->   ListFresh' (nc :: nl) tl.
Proof. intros nc nl tl.  intros H.  intros LF1.
apply (Fresh_mut (fun nc tl =>  ListFresh' nl tl-> ListFresh' (nc :: nl) tl) (fun nc t =>  ListFreshTerm' nl  t ->  ListFreshTerm' (nc :: nl) t)) in H .
intros. auto. intros. constructor. ProveNonceList. apply LF in H0.   apply ListFreshNonceList in H0. auto. intros. apply lfrConc'.
  apply H1.
   apply invlfrConc' in H4. destruct H4.  auto. apply H3.  apply invlfrConc' in H4.
 destruct H4.  auto. intros.  apply lfrtNN'. ProveFresh. auto. intros. apply lfrtNN'. ProveFresh. auto.
intros. apply lfrtNN'. ProveFresh. auto. auto.
Qed.


Proposition FLT: forall nl t , ListFreshTerm nl t -> ListFreshTerm' nl t.
Proof. intros. induction H.  constructor. apply lfrtNN'.  auto. auto.
Qed.

Proposition FL: forall   nl tl , ListFresh nl tl -> ListFresh' nl tl.
Proof. intros.  induction H.  constructor. induction lx. constructor.
apply FreshIsNonce in H. ProveNonceList.  apply ListFreshNonceList in H0.
auto. apply lfrConc'.  apply lfrtNN'. inversion H. auto.  apply invlfrlConc in H0.
destruct IHListFresh. constructor.  destruct H0. apply FLT. apply H0.
destruct H0. apply FLT. auto. apply IHlx.
inversion H. auto.  apply invlfrlConc in H0.
destruct H0. auto. inversion IHListFresh . constructor. inversion IHListFresh . constructor; auto. auto.
Qed.



Proposition invlfrlConc' : forall nc nl tl, ListFresh' (nc :: nl) tl -> ( Fresh nc  tl /\ ListFresh'  nl tl).
Proof. intros. apply LF in H. inversion H. apply FL in H4. auto. Qed.



*)
