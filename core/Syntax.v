
(************************************************************************)
(* Copyright (c) 2020, Gergei Bana                                      *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************) 

Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Setoid.
Import ListNotations.
Require Export FunctionalExtensionality.
Require Import Coq.Bool.Bool.






(**************************************************************************************)
(**************************************************************************************)
(*************************************** SYNTAX ***************************************)
(**************************************************************************************)
(**************************************************************************************)



(* Type "ppt" represents PPT algorithms, its interpretation [[ppt]] is the set of PPT 
algorithms with input 1^\eta where
eta is the security parameter. ppt is the combination of the bool and message types of the paper*)

Parameter (* type *) ppt: Type.    


(* In our semantics, PPT algorithms can use two random tapes. Honest and Adversarial. 
Nonces use honest random tape, Adversarial function symbols use the adversarial random 
tape. While we represent cryptographic primitives as deterministic PT algorithms. 
Random inputs to cryptographic primitives have to be explicitely represented using nonces.
"ComputationType" type is a set of labels to distinguish these PPT maps. 
So Honest includes script N in the paper, Deterministic includes script F and Adversarial includes script G.  
General allows using the honest random string too such as nonces. Functions in.*)
Inductive ComputationType (* set *) : Type :=
      Honest | Deterministic | Adversarial | General.             








(* "PPT" creates the ensembles within type list ppt -> ppt corresponding
to Honest, Deterministic, Adversarial and General ppt computations. For example for 
"f : list ppt -> ppt", the proposition "(PPT Adversarial) f" states that
f is an adversarial computation. Note that our CCSA logic is very limited 
and we do not have a domain for "list ppt -> ppt". On the other hand, we 
can use the rich language of Coq to reason about such maps. Hence
"PPT Adversarial" is a "metapredicate", it is 
a predicate in Coq, but it is not a predicate of our logic *)
                                                                   
Parameter (*metapredicate*) 
      PPT: ComputationType -> ((list ppt -> ppt) -> Prop).       


(*The following axioms are also not part of our logic, they are used to reason about 
function symbols. *)


(* Deterministic algorithms are in particular Honest *)
Axiom (*metaaxiom*) DeterministicHonest : 
      forall {lf : list ppt -> ppt} , 
        (PPT Deterministic) lf 
         -> (PPT Honest) lf.

(* Deterministic algorithms are in particular Adversarial *)
Axiom (*metaaxiom*) DeterministicAdversarial : 
      forall {lf : list ppt -> ppt} , 
        (PPT Deterministic) lf 
         -> (PPT Adversarial) lf.

(* Honest algorithms are in particular General *)
Axiom (*metaaxiom*) HonestGeneral : 
      forall {lf : list ppt -> ppt} , 
        (PPT Honest) lf 
        -> (PPT General) lf.

(* Adversarial algorithms are in particular General *)
Axiom (*metaaxiom*) AdversarialGeneral : 
      forall {lf : list ppt -> ppt} , 
        (PPT Adversarial) lf 
         -> (PPT General) lf.





(* Constant maps, that is, generating the same PPT algorithm 
irrispective of the input are in General *)
Axiom (*metaaxiom*) ConstantGeneral : 
      forall {x0 :  ppt} , 
        (PPT General) (fun x => x0).



(**************************************************************************************)
(******************************** CORE FUNCTION SYMBOLS *******************************)
(**************************************************************************************)


(************************************** Constants *************************************)



(* In the Core part, we have 3 zero-arity (constant) function symbols : default, TRue and FAlse in ppt.
default represents the empty ppt, when there is no output. TRue represents the ppt that on all inputs
outputs the Boolean true, and FAlse the ppt that on all inputs outputs the Boolean false. *)


Parameter default : ppt.       
Axiom (*metaaxiom*) defaultDeterministic : 
      (PPT Deterministic) (fun lx : list ppt => default).
(*Proposition defaultGeneral : 
      (PPT General) (fun lx : list ppt => default).
Proof. apply DeterministicGeneral. apply defaultDeterministic. Qed. *)



Parameter TRue FAlse : ppt.       
Axiom (*metaaxiom*) trueDeterministic : 
      (PPT Deterministic) (fun lx : list ppt => TRue).
(*Proposition trueGeneral : 
      (PPT General) (fun lx : list ppt => TRue).
Proof. apply DeterministicGeneral. apply trueDeterministic. Qed. *)
Axiom (*metaaxiom*) falseDeterministic : 
      (PPT Deterministic) (fun lx : list ppt => FAlse).
(*Proposition falseGeneral : 
      (PPT General) (fun lx : list ppt => FAlse).
Proof. apply DeterministicGeneral. apply falseDeterministic. Qed. *)



(* Non-zero arity function symbols *)
(* We define non-zero arity function symbols on lists which turns out to be less notation-heavy than
if we defined them on products. EQ is ppt x ppt -> ppt, but we represent it as list ppt -> ppt such that
on lists shorter than 2, EQ outputs default, while if the list is longer than 2, than the items after the 
second are ignored. EQ is is accessible for both honest and adversarial computations*)
(* The semantics [[EQ]] is the PPT algorithm that on 
interpretations [[x]], [[y]] of x and y assigns the PPT algorithm [[E(x,y)]] 
that outputs true when the outputs of its inputs [[x]] and [[y]] are the 
same and outputs false otherwise. *) 

Parameter EQ : list ppt -> ppt.    
Axiom (*metaaxiom*) EQDeterministic : 
      (PPT Deterministic) EQ.
Axiom (*metaaxiom*) EQ2arg : 
      forall {x1 x2 : ppt} {lx : list ppt} ,
        (EQ nil) = default 
        /\ EQ [x1] = default 
        /\ (EQ ([x1; x2]++lx)) = (EQ [x1; x2]). 
(*Proposition EQGeneral : 
      (PPT General) EQ.
Proof. apply DeterministicGeneral. apply EQDeterministic. Qed. *)



(* Similarly, If_Then_Else_ is ppt x ppt x ppt -> ppt, but we represent it as list ppt -> ppt such that
on lists shorter than 3, it outputs default, while if the list is longer than 3, than the items after the 
second are ignored. If_Then_Else_ is is accessible for both honest and adversarial computations*)
(* The semantics of  [[If_Then_Else_]] on input
PPT algorithms [[b]], [[x]], [[y]], it assigns the PPT algorithm 
 [[If_Then_Else_(b,x,y)]] that outputs the output of [[x]] when
  the output of [[b]] is true; outputs the output of [[y]] when 
the output of [[b]] is false; outputs an error otherwise *)


Parameter If_Then_Else_: list ppt-> ppt.  
Axiom (*metaaxiom*) ITEDeterministic : 
      (PPT Deterministic) If_Then_Else_.  
Axiom (*metaaxiom*) ITE3arg : 
      forall {x1 x2 x3 : ppt} {lx : list ppt} ,
        (If_Then_Else_ nil) = default 
        /\ If_Then_Else_ [x1] = default 
        /\ (If_Then_Else_ [x1 ; x2]) = default 
        /\ (If_Then_Else_ ([x1; x2; x3]++lx)) = (If_Then_Else_ [x1; x2; x3]). 
(*Proposition ITEGeneral : 
      (PPT General) If_Then_Else_.
Proof. apply DeterministicGeneral. apply ITEDeterministic. Qed. *)
 



Notation "'If' c1 'Then' c2 'Else' c3" := (If_Then_Else_ [c1; c2; c3])
  (at level 55, right associativity).






(**************************************************************************************)
(************************************** PREDICATE *************************************)
(**************************************************************************************)


(************************* Computational Indistinguishability *************************)



(* We have a single predicate symbol, computational indistinguishability *)
(* "x ~ y" means that the output distributions
of [[x]] and [[y]] are indistinguishable for all
PPT algorithms*)

Parameter cind : list ppt -> list ppt -> Prop.    
Notation "x ~ y" := (cind x y) (at level 75, no associativity): type_scope.   


(**************************************************************************************)
(*********************************** ABBREVIATIONS ************************************)
(**************************************************************************************)


(******************************* If Then Else on lists ********************************)



(*The following defines "IF b THEN x ELSE y " for x y lists. 
Works on entry by entry: 
E.g. IF b THEN [x1, x2] ELSE [y1, y2] = 
[ If b Then x1 Else y1 , If b Then x2 Else y2 ] - Prove it 
If x is shorter then y, then the rest of y simply does not appear in the result:
E.g. IF b THEN [x1, x2] ELSE [y1, y2, y3] = 
[ If b Then x1 Else y1 , If b Then x2 Else y2 ] - Prove it 
If x is longer than y the rest of x does not appear: 
E.g. IF b THEN [x1, x2, x3] ELSE [y1, y2 ] = 
[ If b Then x1 Else y1 , If b Then x2 Else y2 ] - Prove it 
*)



Fixpoint IF_THEN_ELSE_ (b: ppt) (lx : list ppt) (lx' : list ppt) : list ppt :=
match lx with 
| nil  => nil
| a :: m => match lx' with 
            | nil => nil
            | a' :: m' => (If_Then_Else_ [b; a; a']) :: (IF_THEN_ELSE_ b m m')
            end
end.                                                                            

Notation "'IF' c1 'THEN' c2 'ELSE' c3" := (IF_THEN_ELSE_ c1 c2 c3)
  (at level 200, right associativity).    



(*********************** Overwhelming (Computational) Equality ************************)



(* Tis is also an abbreviation, but its semantics turns out to be that
[[x]] and [[y]] are equal except maybe for negligible probability *)

Definition ceq :  ppt -> ppt -> Prop := fun x y => [ (EQ [x ; y ]) ] ~ [ TRue ].
Notation "x # y" := (ceq x y) (at level 74, no associativity): type_scope.





(**************************************************************************************)
(*************************** Auxilliary Functions for Lists ***************************)
(**************************************************************************************)




(* 0th entry, head of a list. nil is mappted to default *)
Definition HD (lx : list ppt) : ppt :=  hd default lx.

(* Tail of a list, taking off the first enrty *)
Definition TL (lx : list ppt) : list ppt :=  tl lx.

(* Mapping on the nth element. nil is mappted to default *)
Definition Nth (n : nat) (lx : list ppt) := nth n lx default.


(* Zeroth element and Head are the same *)
Lemma ZerothisHD : 
      Nth 0 = HD. 
Proof. apply functional_extensionality. intros. induction x. cbn.   
 reflexivity. cbn. reflexivity. Qed.

(* S(n)th element and the nth of the tail are the same *)
Lemma SnthisnthTL : 
      forall {n} , 
        (fun lx => Nth (S n) lx) = (fun lx => Nth  n (TL lx)).
Proof. intros. apply functional_extensionality. intros. induction x. 
cbn. induction n. reflexivity. reflexivity. cbn. reflexivity. Qed. 


(* concetanating the head and the tail is the identity map on non-nil. *)
Lemma IDisHDTL : 
      forall {lx} , 
        lx <> [] -> lx = (HD lx) :: (TL lx). 
Proof. intros. induction lx. destruct H. reflexivity. cbn. reflexivity. Qed.  

 



Parameter adv :  nat -> list ppt -> ppt. 
Axiom (*metaaxiom*) advAdversarial :  
      forall n ,
        (PPT Adversarial) (adv n).


(* Here we introduce built-in functions that can be used for new function symbols such as pairing etc.*)

Parameter Symbols : Type. 




Parameter FunctionSymbol :  ComputationType -> Symbols -> list ppt -> ppt. 
Axiom (*metaaxiom*) FunHAG :  
      forall s hag,
        (PPT hag) ((FunctionSymbol hag) s).



Parameter ConstantSymbol :  ComputationType -> Symbols -> ppt. 
Axiom (*metaaxiom*) ConHAG :  
      forall s hag,
        (PPT hag) (fun lx => (ConstantSymbol hag) s).




(**************************************************************************************)
(**************************************************************************************)
(*************************************** NONCES ***************************************)
(**************************************************************************************)
(**************************************************************************************)



(* Ltac checking Freshness to be done. *)


Parameter nonce : nat -> ppt.
Axiom (*metaaxiom*) nonceHonest : 
      forall n , (PPT Honest) (fun lx : list ppt => nonce n).

Axiom (*metaaxiom*) noncesDifferent : 
      forall n n' , nonce n = nonce n' -> n = n'.
Axiom (*metaaxiom*) noncesNotdefault : 
      forall n , nonce n <> default.

