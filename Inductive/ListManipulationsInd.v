

(************************************************************************)
(* Copyright (c) 2021, Gergei Bana, Qianli Zhang                        *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(************************************************************************)



Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Setoid.
Import ListNotations.
Require Export FunctionalExtensionality.
Require Import Coq.Bool.Bool.
Require Export TermsInd.




(**************************************************************************************)
(**************************************************************************************)
(*************************** Auxilliary Functions for Lists ***************************)
(**************************************************************************************)
(**************************************************************************************)



(* 0th entry, head of a list. nil is mappted to default *)
Definition HD (lx : list ppt) : ppt :=
  hd default lx.

(* Tail of a list, taking off the first enrty *)
Definition TL (lx : list ppt) : list ppt :=
  tl lx.

(* Mapping on the nth element. nil is mapped to default *)
Definition Nth (n : nat) (lx : list ppt) :=
  nth n lx default.



(* Mapping on a list of nth elements. nil is mapped to default *)
Definition ListNth (ln : list nat) (lx : list ppt) :=
  maplist nat ppt (fun n => nth n lx default) ln.





(* Zeroth element and Head are the same *)
Lemma ZerothisHD :
  Nth 0 = HD.
Proof.
  apply functional_extensionality.
  intros.
  induction x.
  - cbn.
    reflexivity.
  - cbn.
    reflexivity.
Qed.

(* S(n)th element and the nth of the tail are the same *)
Lemma SnthisnthTL :
  forall {n} ,
    (fun lx => Nth (S n) lx) = (fun lx => Nth  n (TL lx)).
Proof.
  intros.
  apply functional_extensionality.
  intros.
  induction x.
  - cbn.
    induction n.
    + reflexivity.
    + reflexivity.
  - cbn.
    reflexivity.
Qed.


(* concetanating the head and the tail is the identity map on non-nil. *)
Lemma IDisHDTL :
  forall {lx} ,
    lx <> []
    -> lx = (HD lx) :: (TL lx).
Proof.
  intros.
  induction lx.
  - destruct H.
    + reflexivity.
  - cbn.
    reflexivity.
Qed.




(**************************************************************************************)
(**************************************************************************************)
(********************************** List Manipulations ********************************)
(**************************************************************************************)
(**************************************************************************************)



(****************************** some auxiliary functions *****************************)


(* Id represents the identity function for ppt -> ppt and
  for list ppt -> list ppt. For ppt -> list ppt it assigns [x] to x, and
for list ppt -> ppt it assigns HD lx to lx*)

Definition Id (tol : TermOrList ) (tol' : TermOrList ) : (ToL tol) -> (ToL tol') :=
  match tol with
  | Term => match tol' with Term => (fun x : ppt => x) | List => (fun x : ppt => [x]) end
  | List => match tol' with Term => (fun lx : list ppt => HD lx) | List => (fun lx : list ppt => lx) end
  end.


Proposition IdHD :
  forall tol,
    (fun x  => Id tol Term x ) = (fun x  => HD (Id tol List x )).
Proof.
  intros.
  destruct tol.
  - unfold Id at 2.
    unfold HD; simpl; auto.
  - unfold Id; auto.
Qed.





(**************************************************************************************)
(********************************* Listmap Definition *********************************)
(**************************************************************************************)



Section Listmap. (* we should change the name of this *)


  Inductive Listmap (tol  : TermOrList) :  (ToL tol -> list ppt) -> Prop :=  (* metapredicate *)
  | lmId  :
      Listmap tol (Id tol List )
  | lmtl   :
      forall  {c} ,
        ListmapTerm tol c
        -> Listmap  tol (fun x => [c x])
  | lmTL   :
      forall  {c} ,
        Listmap tol c
        -> Listmap  tol (fun x => (TL (c x)))
  | lmfirstn   :
      forall  {c n} ,
        Listmap tol c
        -> Listmap  tol (fun x => (firstn n (c x)))
  | lmlength   :
      forall  {c n} ,
        Listmap tol c
        -> Listmap  tol (fun x => (skipn ((length (c x)) - n) (c x)))
  | lmskipn   :
      forall  {c n} ,
        Listmap tol c
        -> Listmap  tol (fun x => (skipn n (c x)))
  |lmApp  :
     forall  {c1 c2},
       Listmap tol c1
       -> Listmap tol c2
       -> Listmap tol  (fun x => (c1 x)++(c2 x))
  with ListmapTerm  (tol : TermOrList) : (ToL tol -> ppt) -> Prop :=
  | lmtId   :
      ListmapTerm tol (Id tol Term)
  | lmtHD  :
      forall {c} ,
        Listmap tol c
        -> ListmapTerm  tol (fun x => (HD (c x))).
  
End Listmap.


(**************************************************************************************)
(********************************* Mutual Induction *********************************)
(**************************************************************************************)


Scheme Listmap_mut := Minimality for Listmap Sort Prop
  with ListmapTerm_mut := Minimality for ListmapTerm Sort Prop.



(**************************************************************************************)
(************************************ Properties ***************************************)
(**************************************************************************************)




Proposition lmnil   :
  forall tol,
    Listmap tol (fun x : ToL tol => nil). 
Proof.
  intros.
  destruct tol.
  - simpl.
    assert  ((fun (x : ppt) =>  []) = (fun (x: ppt)  => firstn 0 [x])).
    { apply functional_extensionality.
      unfold firstn.
      intros.
      auto. } 
    rewrite H.
    apply lmfirstn.
    apply lmtl.
    apply lmtId.
  - assert  ((fun (x : list ppt) =>  []) = (fun (x: list ppt)  => firstn 0 x)).
    { apply functional_extensionality.
      unfold firstn.
      intros.
      auto. }
    simpl.
    rewrite H.
    apply lmfirstn. 
    apply lmId.
Qed.  


Proposition lmCons  :
  forall tol {ct c},
    ListmapTerm tol ct
    -> Listmap tol c
    -> Listmap tol (fun x => (ct x)::(c x)).
Proof.
  intros.
  assert ((fun x => (ct x)::(c x)) = (fun x => [ct x]++(c x))).
  { apply functional_extensionality.
    intros.
    rewrite <- (app_nil_l (c x )) at 1.
    apply app_comm_cons. }
  rewrite H1.
  apply lmApp.
  + apply lmtl.
    auto.
  + auto.
Qed.  
 

Proposition lmCont   :
  forall tol {c1 c2 } ,
    Listmap  List  c1
    -> Listmap tol c2
    -> Listmap tol (fun x   => (c1 (c2 x)) ).
Proof.
  intros.
  apply ( Listmap_mut   List (fun c =>   Listmap  tol (fun x : ToL tol => c (c2 x))) (fun c =>   ListmapTerm  tol (fun x : ToL tol => c (c2 x)))); try (intros; constructor;  auto).
  - unfold Id; auto. 
  - auto.
Qed.  
 

Proposition tlmCont   :
  forall   tol {tc1 c2},
    Listmap   Term tc1
    -> ListmapTerm   tol c2
    -> Listmap   tol (fun x => (tc1  (c2 x))).
Proof.
  intros.
  apply (Listmap_mut    Term (fun c =>  Listmap   tol (fun x : ToL tol => c (c2 x))) (fun c =>  ListmapTerm   tol (fun x : ToL tol => c (c2 x)))); try (intros; constructor;  auto).
  - unfold Id; auto. 
  - auto.
Qed.
  





(* [ ] makes a Listmap from ListmapTerm*)
Proposition lmTtoL :
  forall {  tol ct},
    ListmapTerm   tol ct
    -> Listmap   tol (fun x => [ (ct x)]).
Proof.
  intros.
  apply lmtl.
  auto.
Qed.


(* Various ways of combining two Listmaps *)
Proposition lmtlCont :
  forall {  tol c1 c2},
    Listmap   Term c1
    -> ListmapTerm   tol c2
    -> Listmap   tol (fun x => (c1  (c2 x))).
Proof.
  intros.
  apply tlmCont.
  - assumption.
  - assumption.
Qed.

Proposition lmllCont :
  forall {  tol c1 c2},
    Listmap   List c1
    -> Listmap   tol c2
    -> Listmap   tol (fun x => (c1  (c2 x))).
Proof.
  intros.
  apply lmCont.
  - assumption.
  - assumption.
Qed.

Proposition lmttCont :
  forall {  tol tc1 tc2},
    ListmapTerm   Term tc1
    -> ListmapTerm   tol tc2
    -> ListmapTerm   tol (fun x => (tc1  (tc2 x))).
Proof.
  intros.
  assert ((fun x : ToL tol => tc1 (tc2 x))= (fun x : ToL tol => HD [tc1 (tc2 x)])).
  { simpl.
    reflexivity. }
  rewrite H1.
  apply lmtHD.
  apply (@lmtlCont   tol (fun x => [tc1 x]) tc2).
  - apply lmTtoL.
    assumption.
  - assumption.
Qed.

Proposition lmltCont :
  forall {  tol tc1 tc2},
    ListmapTerm   List tc1
    -> Listmap   tol tc2
    -> ListmapTerm   tol (fun x => (tc1  (tc2 x))).
Proof.
  intros.
  assert ((fun x : ToL tol => tc1 (tc2 x))= (fun x : ToL tol => HD [tc1 (tc2 x)])).
  { simpl.
    reflexivity. }
  rewrite H1.
  apply lmtHD.
  apply (@lmllCont   tol (fun x => [tc1 x]) tc2).
  - apply lmTtoL.
    assumption.
  - assumption.
Qed.



(* Nth is a Listmap *)
Proposition lmNth :
  forall {  tol n c},
    Listmap   tol c
    -> ListmapTerm   tol (fun x => (Nth n (c x))).
Proof.
  intros tol n.
  induction n.
  - intros c.
    rewrite ZerothisHD.
    apply lmtHD.
  - intros.
    rewrite SnthisnthTL.
    apply (IHn (fun x => TL (c x))).
    apply lmTL.
    apply H.
Qed.



(* ListNth is a Listmap *)
Proposition lmListNth :
  forall {tol ln c},
    Listmap   tol c
    -> Listmap  tol (fun x => (ListNth ln (c x))).
Proof.
  intros.
  induction ln.
  - intros.
    unfold ListNth. simpl.
    apply lmnil.
  - unfold ListNth.
    unfold maplist. 
    apply lmCons.
    + apply lmNth.
      assumption.
    + apply IHln.
Qed.


Proposition NthListmap:
  forall {n},
    ListmapTerm List (Nth n).
Proof.
  intros.
  apply lmNth.
  apply lmId.
Qed.


Proposition ListNthListmap:
  forall {ln},
    Listmap List (ListNth ln).
Proof.
  intros.
  apply lmListNth.
  apply lmId.
Qed.


Proposition HDListmap:
  ListmapTerm  List HD.
Proof.
  intros.
  apply lmtHD.
  apply lmId.
Qed.



Proposition TLListmap:
  Listmap   List TL.
Proof.
  intros.
  apply lmTL.
  apply lmId.
Qed.

Proposition firstnListmap:
  forall {n},
    Listmap List (firstn n).
Proof.
  intros.
  apply lmfirstn.
  apply lmId.
Qed.

Proposition skipnListmap:
  forall {n},
    Listmap List (skipn n).
Proof.
  intros.
  apply lmskipn.
  apply lmId.
Qed.

Proposition lengthListmap:
  forall {n},
    Listmap   List (fun x => (skipn ((length x) - n) x)).
Proof.
  intros.
  apply lmlength.
  apply lmId.
Qed.





(**************************************************************************************)
(***************************** Automated Check of Listmap *****************************)
(**************************************************************************************)


(* Automated proof that something is a context. *)
Ltac ProveListmap :=
  repeat ( intros;
           match goal with
           |H : ?prop |- ?prop => apply H
           |   |- Listmap  List (fun lx  => lx) => apply lmId
           |   |- Listmap  List id => apply @lmId
           |   |- Listmap _ (fun x   => ?a :: ?m) => apply @lmCons
           |   |- Listmap _ (fun x   => ?a ++ ?m) => apply @lmCons
           |   |- Listmap List (cons ?a) => apply @lmCons
           |   |- Listmap _ (fun x => [ ]) => apply @lmnil
           |   |- Listmap _ TL => apply @TLListmap
           |   |- Listmap _ (fun x   => TL ?t) => apply @lmllCont
           |   |- Listmap _ (firstn ?n) => apply @firstnListmap
           |   |- Listmap _ (fun x   => firstn ?n ?t) => apply @lmllCont
           |   |- Listmap _ (skipn ((length ?t) - ?n)) => apply @lengthListmap
           |   |- Listmap _ (fun x   => skipn ((length ?t) - ?n) ?t) => apply @lengthListmap
           |   |- Listmap _ (skipn ?n) => apply @skipnListmap
           |   |- Listmap _ (fun x   => skipn ?n ?t) => apply @lmllCont
           |   |- ListmapTerm Term (fun x  => x) => apply @lmtId
           |   |- ListmapTerm Term id => apply @lmtId
   (*      |   |- ListmapTerm _ _ (fun x => TRue) => apply @TRueListmap
           |   |- ListmapTerm _ _ (fun x => FAlse) => apply @FAlseListmap
           |   |- ListmapTerm _ _ (fun x => default) => apply @defaultListmap*)
  (*       |   |- ListmapTerm _ _ (fun x => nonce ?n) => apply @nonceListmap *)
           |   |- ListmapTerm _ (Nth ?n) => apply @NthListmap
           |   |- Listmap _ (ListNth ?ln) => apply @ListNthListmap
           |   |- ListmapTerm _ HD => apply @HDListmap
  (*       |   |- ListmapTerm _ _ (adv _) => apply @advListmap  *)
   (*      |   |- ListmapTerm _ _ If_Then_Else_ => apply @ITEListmap  *)
           (*       |   |- ListmapTerm _ _ EQ => apply @EQListmap *)
           |   |- ListmapTerm _ (fun x   => HD ?t) => apply @lmltCont
           |   |- ListmapTerm _ (fun x  => Nth ?n ?t) => apply @lmltCont
           |   |- Listmap _ (fun x  => ListNth ?ln ?t) => apply @lmltCont
      (*   |   |- ListmapTerm _ _ (fun x   => adv ?n ?t) => apply @cltCont *)
   (*      |   |- ListmapTerm _ _ (fun x   => EQ ?t) => apply @cltCont *)
           (*      |   |- ListmapTerm _ _ (fun x   => If_Then_Else_ ?t) => apply @cltCont *)
           | H : Listmap List ?c  |- Listmap (fun lx => (?c ?t)) => apply (@lmllCont _ _)
           | H : Listmap List ?c  |- Listmap List (fun lx : list ppt => (?c ?t)) => apply (@lmllCont _ List)
           | H : Listmap List ?c  |- Listmap Term (fun x : ppt => (?c ?t)) => apply (@lmllCont _ Term)
           | H : Listmap Term ?c  |- Listmap _ (fun x : ToL _ => (?c ?t)) => apply (@lmtlCont _ _)
           | H : Listmap Term ?c  |- Listmap Term (fun x : ppt => (?c ?t)) => apply (@lmtlCont _ Term)
           | H : Listmap Term ?c  |- Listmap List (fun lx : list ppt => (?c ?t)) => apply (@lmtlCont _ List)
           | H : ListmapTerm List ?c  |- ListmapTerm _ (fun lx : ToL _ => (?c ?t)) => apply (@lmltCont _ _)
           | H : ListmapTerm List ?c  |- ListmapTerm List (fun lx : list ppt => (?c ?t)) => apply (@lmltCont _ List)
           | H : ListmapTerm List ?c  |- ListmapTerm Term (fun x : ppt => (?c ?t)) => apply (@lmltCont _ Term)
           | H : ListmapTerm Term ?c  |- ListmapTerm _ (fun x : ToL _ => (?c ?t)) => apply (@lmttCont _ _)
           | H : ListmapTerm Term ?c  |- ListmapTerm Term (fun x : ppt => (?c ?t)) => apply (@lmttCont _ Term)
           | H : ListmapTerm Term ?c  |- ListmapTerm List (fun lx : list ppt => (?c ?t)) => apply (@lmttCont _ List)
           end). (* Works quite well but needs to be extended. PPT _ _ goals should be skipped down. *)




(******************************** Testing the Automation ******************************)



Proposition bla11 :
  Listmap  List (fun x0 : ToL List => x0).
Proof.
  ProveListmap.
Qed.


Proposition  bla10:
  forall tc1 tc2 ,
    ListmapTerm Term tc1
    -> ListmapTerm Term tc2
    -> Listmap Term (fun x : ToL Term => [tc1 x; tc2 x]).
Proof.
  ProveListmap.
Qed.

Proposition bla :
  ListmapTerm Term (fun x : ToL Term => x).
Proof.
  ProveListmap.
Qed.

Proposition bla0 :
  ListmapTerm List (fun x : ToL List => HD x ).
Proof.
  ProveListmap.
Qed.






(********************************* EXAMPLE: Projection ********************************)

Proposition proj02 :
  Listmap   List (fun x => [HD x ; HD (TL (TL x))]).
Proof.
  ProveListmap.
Qed.



(****************** EXAMPLE: Permutation - using the nth, it's easier *****************)

Proposition perm021 :
  forall c ,
    Listmap List c
    -> Listmap List (fun x => [Nth 0 (c x) ; Nth 2 x ; Nth 1 x ]).
Proof.
  ProveListmap.
Qed.
