

(************************************************************************)
(* Copyright (c) 2021, Gergei Bana and Qianli Zhang                                    *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(* Special thanks to Qianli and Ajay K. Eeralla                         *)
(************************************************************************)

Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Setoid.
Import ListNotations.
Require Export FunctionalExtensionality.
Require Import Coq.Bool.Bool.
Require Import Coq.Init.Nat.
Require Export SyntaxInd.
Require Export CpdtTactics. 







(*******************************************************************************)
(************************* Orderding  ***************************)
(*******************************************************************************)

(* Reserved Notation "t0 << t1" (at level 80, no associativity).*)

(*Inductive hagle_help : ComputationType -> ComputationType -> Prop :=
| Det_Hon : Deterministic << Honest
| Det_Adv : Deterministic << Adversarial
| Hon_Mix : Honest << Mixed
| Adv_Mix : Adversarial << Mixed
| Mix_Gen : Mixed << General
where "t0 << t1" := (hagle_help t0 t1).*)


Reserved Notation "t0 <<< t1" (at level 80, no associativity).

(*le means less than or equal to*)
Inductive hagle : ComputationType -> ComputationType -> Prop :=
| Det_Hon : Deterministic <<< Honest
| Det_Adv : Deterministic <<< Adversarial
| Hon_Mix : Honest <<< Mixed
| Adv_Mix : Adversarial <<< Mixed
| Mix_Gen : Mixed <<< General
| hagle_ref : forall {t}, t <<< t
| hagle_trans : forall {t0 t1 t2}, t0 <<< t1 -> t1 <<< t2 -> t0 <<< t2
where "t0 <<< t1" := (hagle t0 t1).


Definition hagleb (t1 t2: ComputationType) : bool :=
  match t1 with
  | Deterministic => true
  | Honest => match t2 with
              | Deterministic => false
              | Adversarial => false
              | _ => true
              end
  | Adversarial => match t2 with
                   | Deterministic => false
                   | Honest => false
                   | _ => true
                   end
  | Mixed => match t2 with
             | Mixed => true
             | General => true
             | _ => false
             end
  | General => match t2 with
               | General => true
               | _ => false
               end
  end.

Lemma hagleble :
  forall t1 t2,
    hagleb t1 t2 = true
    <->
    t1 <<< t2.
Proof.   
  split.
  - intros. 
    destruct t1; destruct t2; unfold hagleb in *; try discriminate H; try constructor.
    apply (@hagle_trans Honest Mixed General); constructor.
    apply (@hagle_trans Deterministic Honest Mixed); constructor.
    apply (@hagle_trans Deterministic Honest General); try constructor.
    apply (@hagle_trans Honest Mixed General); constructor.
    apply (@hagle_trans Adversarial Mixed General); constructor.
  - intros.
    induction H; auto.
    destruct t; auto.
    destruct t0; destruct t1; destruct t2; auto.
Qed.


Lemma hagreflect :
  forall t1 t2,
    reflect (t1 <<< t2) (hagleb t1 t2).
Proof.
  intros t1 t2.
  apply iff_reflect.
  apply (iff_sym (hagleble t1 t2)).
Qed.
 

Ltac ProveHag :=
  match goal with
  | H1: ?t0 <<< ?t2, H2: ?t1 <<< ?t0 |- ?t1 <<< ?t2 => apply (@hagle_trans t1 t0 t2) 
  | |- ?t1 <<< ?t2 => assert (H100 := hagreflect t1 t2); inversion H100; auto
  end.


Goal Deterministic <<< General.
  ProveHag.
Qed.

Goal General <<< General.
  ProveHag.
Qed.



(************************* Least Upper Bound  ***************************)

Definition haglubpair (hag1 : ComputationType) (hag2 : ComputationType) : ComputationType :=
  match hag1 with
  | Deterministic =>
    match hag2 with
    | Deterministic => Deterministic
    | Honest => Honest
    | Adversarial => Adversarial 
    | Mixed => Mixed
    | General => General
    end
  | Honest =>
    match hag2 with 
    | Deterministic => Honest
    | Honest => Honest
    | Adversarial => Mixed 
    | Mixed => Mixed
    | General => General
    end
  | Adversarial =>
    match hag2 with 
    | Deterministic => Adversarial
    | Honest => Mixed
    | Adversarial => Adversarial 
    | Mixed => Mixed
    | General => General
    end
  | Mixed =>
    match hag2 with 
    | Deterministic => Mixed
    | Honest => Mixed
    | Adversarial => Mixed 
    | Mixed => Mixed
    | General => General
    end
  | General => 
    match hag2 with 
    | Deterministic => General
    | Honest => General
    | Adversarial => General 
    | Mixed => General
    | General => General
    end
  end. 


Lemma haglubpair_symm:
  forall hag1 hag2 ,
    haglubpair hag1 hag2 = haglubpair hag2 hag1. 
Proof.
  intros.
  destruct hag1; destruct hag2; unfold haglubpair; auto. 
Qed. 
  
Lemma haglubpair_gt :
  forall hag1 hag2,
    hag1 <<< haglubpair hag1 hag2 /\  hag2 <<< haglubpair hag1 hag2.
Proof.   
  intros.
  apply conj.
  destruct hag1; destruct hag2; unfold haglubpair; ProveHag. 
  destruct hag1; destruct hag2; unfold haglubpair; ProveHag. 
Qed.   

  
Lemma haglubpair_lub :
  forall hag1 hag2 hag,
    hag1 <<< hag
    -> hag2 <<< hag
    -> haglubpair hag1 hag2 <<< hag.
Proof.   
  intros.
  apply hagleble.
  apply hagleble in H.
  apply hagleble in H0.
  unfold hagleb in *.
  destruct hag1; destruct hag2; destruct hag; try discriminate H;  try discriminate H0; auto.  
Qed.

Fixpoint haglub (hagl : list ComputationType) : ComputationType :=
  match hagl with
  | [ ] => Deterministic
  | hag :: hagl' => haglubpair hag (haglub hagl')
  end.

Compute (haglub [Deterministic ; Honest ; Adversarial ]). 
Compute (haglub [Deterministic ; Honest ; General ]). 
Compute (haglub [Adversarial ; Honest  ]). 
Compute (haglub [Mixed ]). 

Lemma unfold_haglub :
  forall hag hagl',
    haglub  (hag :: hagl') =  haglubpair hag (haglub hagl').
Proof.
  auto.
Qed.



Definition hageqb (hag1 hag2 : ComputationType) : bool :=
  match hag1 with
  | Deterministic => match hag2 with Deterministic => true | _ => false end
  | Honest => match hag2 with Honest => true | _ => false end
  | Adversarial => match hag2 with Adversarial => true | _ => false end
  | Mixed => match hag2 with Mixed => true | _ => false end
  | General => match hag2 with General => true | _ => false end
  end.

Lemma hageqb_ref :
  forall hag,
    hageqb hag hag = true.
Proof.
  intros.
  destruct hag; auto.
Qed.  

Lemma hagleb_ref :
  forall hag,
    hagleb hag hag = true.
Proof.
  intros. destruct hag; auto.
Qed.  
