
(************************************************************************)
(* Copyright (c) 2021, Gergei Bana                                      *)
(*                                                                      *)
(* This work is licensed under the MIT license. The license is          *)
(* described in the file "LICENSE" available at the root of the source  *)
(* or at https://opensource.org/licenses/MIT                            *)
(* Special thanks to Qianli and Ajay K. Eeralla                         *)
(************************************************************************)

Require Export Coq.Lists.List.
Require Export Coq.Init.Logic.
Require Export Coq.Init.Peano.
Require Export Coq.Arith.EqNat.
Require Export Setoid.
Import ListNotations.
Require Export FunctionalExtensionality.
Require Import Coq.Init.Nat.
Require Import Coq.Structures.Equalities.

Require Export BasicsInd.


(* For now we set up our logic for shallow embedding *)


(**************************************************************************************)
(**************************************************************************************)
(********************* CCSA SYNTAX WITH SHALLOW EMBEDDING ****************************)
(**************************************************************************************)
(**************************************************************************************)





(**************************************************************************************)
(*************************** FUNCTION SYMBOLS IN GENERAL **************************)
(**************************************************************************************)





(* Our set of function symbols is "Symbols" this set is divided into 4 disjoint groups,
"Deterministic", "Honest", "Adversarial" and "General" symbols, which are further divied into
infinite number of subsets by "Arity". For each "n" natural number we have an arity "narg n"
where "narg" stands for number of arguments. Furthermore, we have a division "arbarg", which stands
for arbitrary arguments, meaning that we do no pre-specify the arity. *)


(* Each time a user wants to add a new function symbol, that should be done here specifying which
type it has. See below the additions of "defaults", "EQs" etc.  *)


Inductive ComputationType (* set *) : Set :=
  Honest | Deterministic | Adversarial | Mixed | General.
(* Maybe replace General with Mixed *)




Inductive arity : Set :=
|narg : nat -> arity
|arbarg.


Inductive Symbols (hag : ComputationType) (arg : arity) :  Set :=
| symbols  : nat -> nat -> Symbols hag arg.   (* "nat" twice just to be more flexible *)





(**************************************************************************************)
(*************************** PREDICATE SYMBOLS IN GENERAL **************************)
(**************************************************************************************)

(* Since we only have the indistinguishability predicate and we do not allow at this point
to add further predicate symbols, we do not need types for predicates similar to what we did for
function symbols. If that were to change, it should be done here. *)



(**************************************************************************************)
(*************************** SPECIFIC FUNCTION SYMBOLS **************************)
(**************************************************************************************)



(* Some specific function symbols *)

(* They could also be just parameters, at least so far we have not used that they are fixed  *)



(* Here the "s" at the end stands for symbol *)       

(* constants *)


Definition defaults : Symbols Deterministic (narg 0) :=
  symbols Deterministic (narg 0) 0 0.

Definition TRues : Symbols Deterministic (narg 0) :=
  symbols Deterministic (narg 0) 1 1.   

Definition FAlses : Symbols Deterministic (narg 0) :=
  symbols Deterministic (narg 0) 1 0.   


  (* Non-zero arity function symbols for equality and tests*)

  

Definition EQs : Symbols Deterministic (narg 2) :=
  symbols Deterministic (narg 2) 0 0.

Definition ITEs : Symbols Deterministic (narg 3) :=
  symbols Deterministic (narg 3) 0 0.





(*Functions symbols for the adversary. These symbols have no pre-specified number of
arguments*)


Definition  advs (n: nat) : Symbols Adversarial arbarg :=
  symbols Adversarial arbarg 0 n.



(*************************************** RANDOM BLOCKS ***************************************)


(* nonces stand for disjoint random blocks of the honest tape, each with equal length *)

Definition nonces (n: nat) : Symbols Honest (narg 0) :=
  symbols Honest (narg 0) 0 n.






(**************************************************************************************)
(***************************************** TERMS *************************************)
(**************************************************************************************)


(* We introduce the type "ppt" represents terms for PPT algorithms, our domain of interpretation.
Its interpretation [[ppt]] is the set of PPT
algorithms with input 1^\eta where
eta is the security parameter. ppt is the combination of the bool and message types of the paper
Here ppt is inductive, it is simply terms, not the interpretation. *)
 


Inductive ppt: Set :=
| Holes :  nat -> ppt
| Functions : forall (hag : ComputationType) arg , Symbols hag arg -> list ppt -> ppt.


Definition Constants (hag : ComputationType) (c : Symbols hag (narg 0)):  ppt :=
  Functions hag (narg 0) c [ ].




(* With the introduction of "ppt", Coq delivers for us the "list ppt" type, that is a representaton of
all finite lists of elements in "ppt". When we embed the above function symbols of "Symbols",
as function symbols of Coq, all of them are interpreted as of type "list ppt -> ppt".
This turns out to be less notation-heavy than
if we defined them on products. E.g. EQ is ppt x ppt -> ppt, but we represent it as list ppt -> ppt. However,
we will require that function symbols of arity "arg n" output default when the input list is shorter, and
drops the rest of the list when it is longer than "arg n".
Constantss (nullary function symbols) are also embedded as elements in "ppt" and the obvious link is
established between the two embeddings *)


(* FOR THE COMPUTATIONAL SEMANTICS OF EACH FUNCTION SYMBOL AS PPT ALGORITHMS, IT MUST BE ENSURED THAT THEY
ALL USE DISJOINT SEGMENTS OF THE HONEST RANDOM TAPE *)



(* We introduce the following notations for the above specific function symbols *)

Notation "'default'" := (Constants Deterministic  defaults).
Notation "'TRue'" := (Constants Deterministic  TRues).
Notation "'FAlse'" := (Constants Deterministic  FAlses).
Notation "'EQ'" := (Functions Deterministic (narg 2) EQs).
Notation "'If_Then_Else_'" := (Functions Deterministic (narg 3) ITEs).
Notation "'If' c1 'Then' c2 'Else' c3" := (Functions Deterministic (narg 3) ITEs [c1; c2; c3])
  (at level 55, right associativity).
Notation "'adv' n" := (Functions Adversarial arbarg (advs n)) (at level 0).
Notation "'nonce' n" :=  (Constants Honest (nonces n)) (at level 0).

(* The semantics [[EQ]] is the PPT algorithm that on
interpretations [[x]], [[y]] of x and y assigns the PPT algorithm [[E(x,y)]]
that outputs true when the outputs of its inputs [[x]] and [[y]] are the
same and outputs false otherwise. *)



(* The semantics of  [[If_Then_Else_]] on input
PPT algorithms [[b]], [[x]], [[y]], it assigns the PPT algorithm
 [[If_Then_Else_(b,x,y)]] that outputs the output of [[x]] when
  the output of [[b]] is true; outputs the output of [[y]] when
the output of [[b]] is false; outputs an error otherwise *)











(**************************************************************************************)
(************************************** PREDICATE *************************************)
(**************************************************************************************)


(************************* Computational Indistinguishability *************************)



(* We have a single predicate symbol, computational indistinguishability *)
(* "x ~ y" means that the output distributions
of [[x]] and [[y]] are indistinguishable for all
PPT algorithms*)

Parameter cind : list ppt -> list ppt -> Prop.
Notation "x ~ y" := (cind x y) (at level 75, no associativity): type_scope.




(**************************************************************************************)
(****************************** Syntactic Inerpretations *******************************)
(**************************************************************************************)



Definition Fint (hag : ComputationType) arg (f : Symbols hag arg) : list ppt -> ppt := 
 Functions hag arg f.






(* The definitions of Syntax  end here *)


(**************************************************************************************)
(**************************************************************************************)
(**************************************************************************************)
(**************************************************************************************)
(**************************************************************************************)
(**************************************************************************************)
(**************************************************************************************)
(**************************************************************************************)
(**************************************************************************************)








(* Below there are some further notations and some propositions that follow from the above *)





(**************************************************************************************)
(**************************************************************************************)
(*********************************** ABBREVIATIONS ************************************)
(**************************************************************************************)
(**************************************************************************************)

(******************************* If Then Else on lists ********************************)



(*The following defines "IF b THEN x ELSE y " for x y lists.
Works on entry by entry:
E.g. IF b THEN [x1, x2] ELSE [y1, y2] =
[ If b Then x1 Else y1 , If b Then x2 Else y2 ] - Prove it
If x is shorter then y, then the rest of y simply does not appear in the result:
E.g. IF b THEN [x1, x2] ELSE [y1, y2, y3] =
[ If b Then x1 Else y1 , If b Then x2 Else y2 ] - Prove it
If x is longer than y the rest of x does not appear:
E.g. IF b THEN [x1, x2, x3] ELSE [y1, y2 ] =
[ If b Then x1 Else y1 , If b Then x2 Else y2 ] - Prove it
*)


(* "Better change this to inductive:" *)

Fixpoint IF_THEN_ELSE_ (b: ppt) (lx : list ppt) (lx' : list ppt) : list ppt :=
match lx with
| nil  => nil
| a :: m => match lx' with
            | nil => nil
            | a' :: m' => (If_Then_Else_ [b; a; a']) :: (IF_THEN_ELSE_ b m m')
            end
end.

Notation "'IF' c1 'THEN' c2 'ELSE' c3" := (IF_THEN_ELSE_ c1 c2 c3)
  (at level 200, right associativity).



(*********************** Overwhelming (Computational) Equality ************************)



(* Tis is also an abbreviation, but its semantics turns out to be that
[[x]] and [[y]] are equal except maybe for negligible probability *)

Definition ceq :  ppt -> ppt -> Prop :=
  fun x y => [ (EQ [x ; y ]) ] ~ [ TRue ].

Notation "x # y" := (ceq x y) (at level 74, no associativity): type_scope.




(**************************************************************************************)
(************************************** Properties *************************************)
(**************************************************************************************)

(* Proposition ConstFun : forall  hag c, (fun _ : list ppt => Constants hag c) = Fint hag (narg 0) c.
Proof. intros. apply functional_extensionality.  intros. induction x.  unfold Constants.  unfold Fint. 
destruct hag; simpl; auto.
unfold Fint. destruct hag; simpl; auto.   
Qed. *)




